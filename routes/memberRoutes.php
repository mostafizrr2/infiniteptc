<?php 
use Illuminate\Support\Facades\Route;

$middleware = config('laralte.middleware');
array_unshift($middleware , 'web');

Route::group(
    [
        'prefix' => 'dashboard', 
        'middleware' => 'memberAuth', 
        'namespace' => '\App\Http\Controllers\Member'
    ], 
function(){

    // *********************
    // LaraLte defaults routes
    // *********************
    Route::get('/', 'MemberController@index')->name('member.index'); 

    Route::get('my-referred', 'NetworkController@referred')->name('member.referred');
    Route::get('my-geneology', 'NetworkController@geneology')->name('member.geneology');

    Route::get('membership', 'MembershipController@index')->name('member.membership');
    Route::post('membership', 'MembershipController@membershipPurchase')->name('membership.purchase');
    
    Route::get('withdraw', 'WithdrawController@index')->name('member.withdraw'); 
    Route::post('withdraw', 'WithdrawController@actionWithdraw')->name('action.withdraw'); 

    Route::get('refferal-earnings', 'RefferalEarningController@index')->name('refferal.earnings'); 

    // Your custom routes.......

    Route::get('deposit-funds', 'DepositController@index')->name('member.deposit');

    Route::get('deposit-funds/earning', function(){
        abort(404);
    })->name('deposit.earning');
    Route::post('deposit-funds/earning', 'DepositController@depositEarning')->name('deposit.earning');
    
    Route::post('deposit-funds', 'DepositController@depositeFromOutside')->name('deposit.outside');

    Route::get('transaction/{id}', 'DepositController@transactionView')->name('member.transaction');

    Route::get('histories', 'HistoryController@index')->name('member.history');

    Route::resource('advertise', 'AdvertiseController');

    Route::get('my-offers', 'OfferController@myOffers')->name('my.offers');
    Route::get('offer/{id}', 'OfferController@offerView')->name('offer.view');
    Route::post('offer/{id}', 'OfferController@adClicked')->name('ad.click');

    Route::resource('ads', 'AdsController');
    Route::get('purchase', 'PurchaseController@index')->name('member.purchase');
    Route::post('purchase', 'PurchaseController@purchaseAdpack')->name('adpack.purchase');

    Route::resource('wallet', 'WalletController');
    Route::post('wallet/default', 'WalletController@makeDefault')->name('wallet.default');

    //Raffles
    Route::get('raffle/{id}', 'MyRaffleController@index')->name('raffle.details');
    Route::post('raffle/{id}', 'MyRaffleController@rafflePurchase')->name('raffle.purchase');
    Route::get('my-raffles', 'MyRaffleController@myRaffles')->name('my.raffles');

    //Admin profile routes
    Route::get('profile', 'ProfileController@index') ->name('user.profile');
    
    Route::get('profile/edit', 'ProfileController@profileEdit')->name('profile.edit');
    Route::post('profile/edit', 'ProfileController@updateEdit')->name('profile.update');

    Route::get('profile/change-password', 'ProfileController@changePassword')->name('profile.password');
    Route::post('profile/change-password', 'ProfileController@updatePassword')->name('memberpassword.update');
   
    Route::get('FAQ', 'ProfileController@faq')->name('member.faq');

    Route::get('logout', 'ProfileController@logout')->name('member.logout');

    Route::post('deposit-complete', 'DepositController@completeDeposited')->name('deposit.complete');

    //Fund transfer
    Route::get('transfer-funds', 'TransferController@index')->name('transfer.funds');
    Route::post('transfer-funds', 'TransferController@postTranfer')->name('post.transfer');

    Route::get('allChatRooms','ChatRoomController@index')->name('member.chatrooms');
    Route::get('chat/{id}','ChatRoomController@myChatRoom')->name('member.room');

});