<?php
use Illuminate\Support\Facades\Route;

Route::group(
    [
        'prefix' => 'adminDashboard',
        'middleware' => 'auth',
        'namespace' => '\App\Http\Controllers\Admin'
    ],

function(){

    // *********************
    // LaraLte defaults routes
    // *********************
    Route::get('/', 'AdminController@index')->name('admin.index');


    // Your custom routes.......

    //All Members
    Route::get('/add-member', 'MemberController@addMember')->name('admin.addMember');
    Route::post('/add-member', 'MemberController@addMemberAction')->name('add.member');

    Route::get('/all-members', 'MemberController@allMembers')->name('admin.allMembers');
    Route::get('/paid-members', 'MemberController@paidMembers')->name('admin.paidMembers');
    Route::get('/free-members', 'MemberController@freeMembers')->name('admin.freeMembers');
    Route::get('/blocked-members', 'MemberController@blockedMembers')->name('admin.blockedMembers');

    Route::get('/member/{username}/block', 'MemberController@memberBlock')->name('admin.memberBlock');
    Route::get('/member/{username}/unblock', 'MemberController@memberUnBlock')->name('admin.memberUnBlock');
    Route::get('/member/{id}/edit', 'MemberController@memberEdit')->name('admin.member.edit');
    Route::post('/member/{id}/edit', 'MemberController@memberUpdate')->name('admin.member.update');
    Route::get('/member/{username}', 'MemberController@memberView')->name('admin.member');
    Route::post('/asign-membership', 'MemberController@changeMembership')->name('change.membership');


    Route::resource('level', 'Plan\LevelController');
    Route::resource('membership', 'Plan\MembershipController');
    Route::resource('adpack', 'Plan\AdpackController');

    Route::get('pending-ads', 'AdvertiseController@pendingAdvertise')->name('pending.ads');
    Route::get('approved-ads', 'AdvertiseController@approvedAdvertise')->name('approved.ads');
    Route::get('declined-ads', 'AdvertiseController@declinedAdvertise')->name('declined.ads');
    Route::get('all-ads', 'AdvertiseController@alldAdvertise')->name('all.ads');

    Route::post('pending-ads', 'AdvertiseController@pendingAdvertiseApprove')->name('pending.ads.approve');
    Route::post('decline-ads', 'AdvertiseController@declineAdvertise')->name('pending.ads.decline');

    Route::get('pending-withdraw', 'WithdrawRequestController@index')->name('pending.withdraw');
    Route::post('pending-withdraw', 'WithdrawRequestController@withdrawApprove')->name('withdraw.approve');
    Route::post('decline-withdraw', 'WithdrawRequestController@withdrawDecline')->name('withdraw.decline');
    Route::get('withdraw-history', 'WithdrawRequestController@withdrawHistory')->name('withdraw.history');
    Route::get('transaction-history', 'TransactionController@index')->name('transaction.history');


    Route::get('member-deposits', 'DepositsController@index')->name('check.deposits');
    Route::post('member-deposits', 'DepositsController@depositeReceived')->name('deposite.received');

    Route::get('admin-earnings', 'AdminEarningController@index')->name('admin.earning');

    //Admin profile routes
    Route::get('profile', 'AdminProfileController@index') ->name('admin.profile');
    Route::get('profile/edit', 'AdminProfileController@profileEdit') ->name('admin.profile.edit');
    Route::post('profile/edit', 'AdminProfileController@updateEdit') ->name('admin.profile.update');
    Route::get('profile/change-password', 'AdminProfileController@changePassword') ->name('admin.profile.password');
    Route::post('profile/change-password', 'AdminProfileController@updatePassword') ->name('admin.password.update');

    Route::resource('raffle', 'RaffleController');
    Route::post('raffle-prize', 'RaffleController@storePrize')->name('raffle.prize');
    Route::get('raffle-publish/{id}', 'RaffleController@rafflePublish')->name('raffle.publish');
    Route::post('update-prize/{id}', 'RaffleController@updatePrize')->name('update.prize');
    Route::get('delete-prize/raffle/{raffle_id}/prize/{prize_id}', 'RaffleController@deletePrize')->name('delete.prize');

    Route::get('raffle/{id}/draw', 'RaffleController@drawRaffle')->name('draw.raffle');

    //Admin Logout
    Route::get('admin/logout', 'AdminProfileController@logout')->name('admin.logout');


//System Routes
//###################################################################################

    //CoinPayment Api
    Route::get('coinpayment-api', 'CoinpaymentController@index')->name('coinpayment.api');
    Route::post('coinpayment-api', 'CoinpaymentController@SaveCoinPayment')->name('coinpayment.api');

    //System info
    Route::get('system-info', 'SystemController@systemInfo')->name('system.info');
    Route::post('system-info', 'SystemController@SaveSystemInfo')->name('system.info');

    //System info
    Route::get('seo-info', 'SystemController@seoInfo')->name('seo.info');
    Route::post('seo-info', 'SystemController@SaveSeoInfo')->name('seo.info');

    //System uploads
    Route::get('system-uploads', 'SystemController@systemUploads')->name('system.uploads');
    Route::post('system-uploads', 'SystemController@SaveSystemUploads')->name('system.uploads');
    Route::get('system-uploads/{type}', 'SystemController@uploadsDelete')->name('uploads.delete');

    //Api routes
    Route::get('api', 'SystemController@apiIntegration')->name('api.integration');
    Route::post('api', 'SystemController@SaveApiIntegration')->name('api.integration');

    // Meta Scripts
    Route::get('meta-scripts', 'SystemController@metaScripts')->name('meta.scripts');
    Route::post('meta-scripts', 'SystemController@SaveMetaScripts')->name('meta.scripts');

    Route::resource('faq', 'FaqController');

    Route::get('chatrooms','ChatRoomController@index')->name('admin.chatroom');

    Route::get('chat/{id}','ChatRoomController@myChatRoom')->name('admin.room');
});
