<?php

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// $ip = '179.108.123.210';
// $details = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip={$ip}"));
// $country = $details->geoplugin_countryName ;

// $countries = http_get_contents('https://raw.githubusercontent.com/stefanbinder/countries-states/master/countries.json');
// $result = json_decode(Http::get('https://restcountries.eu/rest/v2/name/'.$country)->body());
// $result = json_decode( Http::get('https://raw.githubusercontent.com/stefanbinder/countries-states/master/countries.json')->body());
// dd($result);

include __DIR__ . "/memberRoutes.php";
include __DIR__ . "/adminRoutes.php";

Auth::routes();



Route::get('/home', function(){
  return redirect('/');
});

Route::get('/', function(){
  return redirect()->route('user.login');
})->name('home');

//Member authentication
Route::get('/user/sign-in', 'PublicAuthController@memberLogin')->name('user.login');
Route::post('/user/sign-in', 'PublicAuthController@authLogin')->name('user.login');

Route::get('/user/sign-up', 'PublicAuthController@memberRegister')->name('user.register');
Route::post('/user/sign-up', 'PublicAuthController@authRegister')->name('user.register');

Route::get('/user/forgot', 'PublicAuthController@userForgotPassword')->name('user.pass.forgot');
Route::post('/user/forgot', 'PublicAuthController@userPasswordResetMail')->name('pass.reset.mail');

Route::get('/user/{token}/reset/{email}', 'PublicAuthController@userResetPassword')->name('user.pass.reset');
Route::post('/user/{token}/reset/{email}', 'PublicAuthController@userConfirmResetPassword')->name('user.pass.reset');

Route::get('/user/{token}/verify/{email}', 'PublicAuthController@verifyMember')->name('user.verify');

Route::get('crt-storage-link', function(){
    $target = storage_path('app/public');
    $shortcut = base_path().'/../public_html/account/storage'; 
    symlink($target, $shortcut);
    echo("Link Created!") ;
});