
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from pixner.net/dooplo/dooplo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 10 Apr 2020 05:51:31 GMT -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="forntEnd-Developer" content="Mamunur Rashid">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('title') - {{ env('APP_NAME') }}</title>
	<!-- favicon -->
	<link rel="shortcut icon" href="{{ asset('assets/images/favicon.html') }}" type="image/x-icon">
	<!-- bootstrap -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<!-- Plugin css -->
	<link rel="stylesheet" href="{{ asset('assets/css/plugin.css') }}">

	<!-- stylesheet -->
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
	<!-- responsive -->
	<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">

</head>

<body>
	<!-- preloader area start -->
	<div class="preloader" id="preloader">
		<div class="loader loader-1">
			<div class="loader-outter"></div>
			<div class="loader-inner"></div>
		</div>
	</div>
	<!-- preloader area end -->
	
	<section class="page-top"></section>
<!-- Breadcrumb Area End -->

<section class="four-zero-four">
	<img class="bg-img" src="{{ asset('assets/images/404-bg.png') }}" alt="">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="content">
					<img src="{{ asset('assets/images/404.png') }}" alt="">
					<div class="inner-content">
						<h4 class="title">
								Oops,
								Page not found!
						</h4>
						<a href="{{ url()->previous() }}" class="mybtn1">
							<i class="fas fa-angle-double-left"></i> BACK TO PREVIOUS PAGE
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	



	<!-- jquery -->
	<script src="{{ asset('assets/js/jquery.js') }}"></script>
	<!-- popper -->
	<script src="{{ asset('assets/js/popper.min.js') }}"></script>
	<!-- bootstrap -->
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
	<!-- plugin js-->
	<script src="{{ asset('assets/js/plugin.js') }}"></script>

	<!-- MpusemoverParallax JS-->
	<script src="{{ asset('assets/js/TweenMax.js') }}"></script>
	<script src="{{ asset('assets/js/mousemoveparallax.js') }}"></script>
	<!-- main -->
	<script src="{{ asset('assets/js/main.js') }}"></script>
	
	@stack('footer')
</body>


<!-- Mirrored from pixner.net/dooplo/dooplo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 10 Apr 2020 05:52:13 GMT -->
</html>





<!-- 404 Area End -->
{{-- @endsection --}}
