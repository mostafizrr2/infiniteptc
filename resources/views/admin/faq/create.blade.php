@extends('admin')


@push('header')

@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create FAQ</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Create FAQ</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
   
        <div class="row mb-2">
            <div class="col-md-12">
              <a href="{{ route('faq.index') }}" class="btn btn-warning btn-sm float-right">
                Return back
              </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Create FAQ</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <form action="{{ route('faq.store') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="question">Question? (*)</label>
                                        <input type="text" class="form-control" id="question" name="question" value="{{ old('question') }}" placeholder="Enter question">
                                        @error('question')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="answer">Answer (*)</label>
                                        <textarea  class="form-control" id="answer" name="answer" value="{{ old('answer') }}" placeholder="Enter Answer"></textarea>
                                        @error('answer')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="video_link">Video Link (Optional)</label>
                                        <input type="text" class="form-control" id="video_link" value="{{ old('video_link') }}" name="video_link"  placeholder="Enter a video link">
                                        @error('video_link')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                            
                                    <button type="submit" class="btn btn-primary float-right">Save FAQ</button>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->
                </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')

@endpush