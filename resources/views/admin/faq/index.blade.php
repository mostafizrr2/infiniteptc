@extends('admin')


@push('header')

@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">All FAQ</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">All FAQ</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
   
        <div class="row mb-2">
            <div class="col-md-12">
              <a href="{{ route('faq.create') }}" class="btn btn-success btn-sm float-right">
                Create FAQ
              </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">All FAQ</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Question</th>
                            <th>Answer</th>
                            <th>Video Link</th>
                            <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($faqs as $key => $faq)
                        <tr>
                           <td>{{ $key+1 }}</td>
                           <td>{{ $faq->question }}</td>
                           <td>{{ Str::words($faq->answer,10) }}</td>
                           <td>{{ ($faq->video_link != '') ? "Yes" : 'No' }}</td>
                           <td>
                               <a href="{{ route('faq.edit', $faq->id) }}" class="badge badge-primary mr-1">Edit</a>
                               <a href="#" class="badge badge-danger" data-toggle="modal" data-target="#delete{{ $faq->id }}">
                                Delete
                            </a>
                           </td>
                        </tr>     
                        
                        <div class="modal fade" id="delete{{ $faq->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-level" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="modal-level">Delete!</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="{{ route('faq.destroy', $faq->id) }}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <div class="modal-body">
                                    <h5>Are you sure to delete this FAQ?</h5>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                        </div>
                        @endforeach
  
                        </tbody>
                      </table>

                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')

@endpush