@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Raffle Details</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Raffle Details</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-warning">
                        <h1 class="card-title">{{ $raffle->title }}</h1>
                        {{-- <button type="button" class="btn btn-info btn-sm float-right" data-toggle="modal" data-target="#createPrize{{ $raffle->id }}">
                            Create prize
                        </button>

                        <div class="modal fade" id="createPrize{{ $raffle->id }}" tabindex="-1" role="dialog" aria-labelledby="createPrize{{ $raffle->id }}Label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="createPrize{{ $raffle->id }}Label">Create prize for ( {{ $raffle->title }} )</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <form action="{{ route('raffle.prize') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="raffle_id" value="{{ $raffle->id }}">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div class="row">
                                                <?php 
                                                
                                                $prize = DB::table('raffle_prizes')->where('raffle_id', $raffle->id)->latest()->first();
                                                if($prize)
                                                {
                                                    $position = $prize->position + 1;
                                                }
                                                else 
                                                {
                                                    $position =  1;
                                                }
                                                    
                                                ?>
                                                <div class="col-md-6">
                                                    <label for="position">Position</label>
                                                    <input type="number" class="form-control" id="position" name="position" value="{{ old('position', $position) }}" placeholder="Enter prize position">
                                                    @error ('position')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="prize_amount">Prize Amount (USD)</label>
                                                    <input type="text" class="form-control" id="prize_amount" name="prize_amount" value="{{ old('prize_amount', 10) }}" placeholder="Enter prize amount">
                                                    @error ('prize_amount')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Create Raffle</button>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p class="card-text">{{ $raffle->description }}</p>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">Position</th>
                                        <th scope="col">Prize</th>
                                        @if ($raffle->status == 'ended')
                                            <th scope="col">Winner</th>
                                        @endif
                                        {{-- <th scope="col">Action</th> --}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($raffle->prizes as $key => $prize)
                                        <tr>
                                            <th>{{ $prize->position }}</th>
                                            <td>{{ $prize->prize_amount }}$</td>
                                            @if ($raffle->status == 'ended')
                                            <td>
                                                @if ($prize->slot != null)
                                                    {{ $prize->slot->member->username }}
                                                @endif
                                            </td>
                                            @endif
                                            {{-- <td>
                                                <a type="button" class="text-primary" data-toggle="modal" data-target="#updatePrize{{ $raffle->id.$prize->id }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>

                                                @if (count($raffle->prizes) == ($key + 1))
                                                    <a href="{{ route('delete.prize',[$raffle->id, $prize->id]) }}" class="text-danger ml-2"  onclick="return confirm('Are you sure to delete this raffle prize?')">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                @endif
                                            </td> --}}

                                            {{-- <div class="modal fade" id="updatePrize{{ $raffle->id.$prize->id }}" tabindex="-1" role="dialog" aria-labelledby="createPrize{{ $raffle->id }}Label" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="createPrize{{ $raffle->id }}Label">Update prize for ( {{ $raffle->title }} )</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <form action="{{ route('update.prize', $prize->id) }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="raffle_id" value="{{ $raffle->id }}">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <div class="row">
                                             
                                                                    <div class="col-md-6">
                                                                        <label for="position">Position</label>
                                                                        <input type="number" class="form-control" id="position" value="{{ old('position', $prize->position) }}" disabled>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <label for="prize_amount">Prize Amount (USD)</label>
                                                                        <input type="text" class="form-control" id="prize_amount" name="prize_amount" value="{{ old('prize_amount', $prize->prize_amount) }}" placeholder="Enter prize amount">
                                                                        @error ('prize_amount')
                                                                            <p class="text-danger">{{ $message }}</p>
                                                                        @enderror
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Update</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                </div>
                                            </div> --}}
                                        </tr>
                                        
                                    @endforeach
                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>{{-- Card Body --}}
                    <div class="card-footer">
                        <div style="width: 200px" class="float-left">
                            Slots: {{ count($raffle->slots) }} | Slot price: {{ $raffle->slot_price }}$
                        </div>

                        @if ($raffle->status == "drawable")
                        <a href="{{ route('draw.raffle', $raffle->id) }}" onclick="return confirm('Are you sure to draw this raffle?')" class="btn btn-success btn-sm ml-2 float-right">
                            Draw the raffle
                        </a>
                        @endif
  
                    </div>
                </div>
            </div>  

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title">All Slots</h1>
                        <div class="modal fade" id="createPrize{{ $raffle->id }}" tabindex="-1" role="dialog" aria-labelledby="createPrize{{ $raffle->id }}Label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="createPrize{{ $raffle->id }}Label">Create prize for ( {{ $raffle->title }} )</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <form action="{{ route('raffle.prize') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="raffle_id" value="{{ $raffle->id }}">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div class="row">
                                                <?php 
                                                
                                                $prize = DB::table('raffle_prizes')->where('raffle_id', $raffle->id)->latest()->first();
                                                if($prize)
                                                {
                                                    $position = $prize->position + 1;
                                                }
                                                else 
                                                {
                                                    $position =  1;
                                                }
                                                    
                                                ?>
                                                <div class="col-md-6">
                                                    <label for="position">Position</label>
                                                    <input type="number" class="form-control" id="position" name="position" value="{{ old('position', $position) }}" placeholder="Enter prize position">
                                                    @error ('position')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="prize_amount">Prize Amount (USD)</label>
                                                    <input type="text" class="form-control" id="prize_amount" name="prize_amount" value="{{ old('prize_amount', 10) }}" placeholder="Enter prize amount">
                                                    @error ('prize_amount')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Create Raffle</button>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div style="width: 250px" class="float-left">
                          <strong class="">Total: {{ count($raffle->slots) }}</strong> | 
                          <strong class="text-danger">Sold: {{ count($raffle->slots) - count($raffle->free_slots) }}</strong> | 
                          <strong class="text-success">Available: {{ count($raffle->free_slots) }}</strong>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @foreach ($raffle->slots as $slot)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-header {{ ($slot->prize_id != null) ? 'bg-success': '' }}">
                                Slot# {{ $slot->id }}
                                @if ($slot->prize_id != null)
                                   <div class="float-right badge badge-warning">{{ $slot->prize->position }} Position</div> 
                                @elseif ($slot->member_id != null)
                                   <div class="float-right badge badge-danger">Sold</div> 
                                @endif
                            </div>
    
                            @if ($slot->member_id != null)
                                <div class="card-footer">

                                   <span class="float-left">{{ $slot->updated_at->format('d M, Y') }}</span>

                                    <a href="{{ route('admin.member', $slot->member->username) }}"  class="float-right">
                                        @if ($slot->member->avatar != '')
                                            <img style="width:20px; height:20px; border-radius:50%" class="img-thumbnail"
                                            src="{{ url('storage/member/'. $slot->member->avatar) }}" alt="" srcset="">
                                        @else
                                            <img style="width:20px; height:20px; border-radius:50%" class="img-thumbnail"
                                            src="{{ url('defaults/user.png') }}" alt="" srcset="">
                                        @endif
                                        {{ $slot->member->username }}
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                   @endforeach
                </div>
            </div>  

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush