@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Raffles</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Raffles</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row mb-3">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#createRaffle">
                  Create new raffle
                </button>

                <div class="modal fade" id="createRaffle" tabindex="-1" role="dialog" aria-labelledby="createRaffleLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="createRaffleLabel">Create new raffle</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action="{{ route('raffle.store') }}" method="POST">
                            @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                  <label for="title">Raffle Title</label>
                                  <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" placeholder="Enter raffle title">
                                  @error ('title')
                                      <p class="text-danger">{{ $message }}</p>
                                  @enderror
                                </div>

                                <div class="form-group">
                                  <label for="description">Raffle Description</label>
                                  <textarea class="form-control" id="description" name="description" placeholder="Enter raffle description"></textarea>
                                    @error ('description')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="total_slots">Total slots</label>
                                            <input type="number" class="form-control" id="total_slots" name="total_slots" value="{{ old('total_slots', 30) }}" placeholder="Enter total slots">
                                            @error ('total_slots')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="slot_price">Slot Price (USD)</label>
                                            <input type="text" class="form-control" id="slot_price" name="slot_price" value="{{ old('slot_price', 10) }}" placeholder="Enter slot price">
                                            @error ('slot_price')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
         
                                </div>
                         
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Create Raffle</button>
                            </div>
                        </form>
                      </div>
                    </div>
                </div>

            </div>
        </div>  

        @if (count($initialRaffles))
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-warning">
                        <h1 class="card-title">New Raffles ({{ count($initialRaffles) }})</h1>

                    </div>
                </div>
            </div>  
            @foreach ($initialRaffles as $item)
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                        {{ $item->title }}
                        <button type="button" class="btn btn-info btn-sm float-right" data-toggle="modal" data-target="#createPrize{{ $item->id }}">
                            Create prize
                        </button>

                        <div class="modal fade" id="createPrize{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="createPrize{{ $item->id }}Label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="createPrize{{ $item->id }}Label">Create prize for ( {{ $item->title }} )</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <form action="{{ route('raffle.prize') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="raffle_id" value="{{ $item->id }}">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div class="row">
                                                <?php 
                                                
                                                $prize = DB::table('raffle_prizes')->where('raffle_id', $item->id)->latest()->first();
                                                if($prize)
                                                {
                                                    $position = $prize->position + 1;
                                                }
                                                else 
                                                {
                                                    $position =  1;
                                                }
                                                    
                                                ?>
                                                <div class="col-md-6">
                                                    <label for="position">Position</label>
                                                    <input type="number" readonly class="form-control" id="position" name="position" value="{{ old('position', $position) }}" placeholder="Enter prize position">
                                                    @error ('position')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="prize_amount">Prize Amount (USD)</label>
                                                    <input type="text" class="form-control" id="prize_amount" name="prize_amount" value="{{ old('prize_amount', 10) }}" placeholder="Enter prize amount">
                                                    @error ('prize_amount')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-success">Create Prize</button>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div>

                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="card-text">{{ $item->description }}</p>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Position</th>
                                            <th scope="col">Prize</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($item->prizes as $key => $prize)
                                            <tr>
                                                <th>{{ $prize->position }}</th>
                                                <td>{{ $prize->prize_amount }}$</td>
                                                <td>
                                                    <a type="button" class="text-primary" data-toggle="modal" data-target="#updatePrize{{ $item->id.$prize->id }}">
                                                        <i class="fa fa-edit"></i>
                                                    </a>

                                                    @if (count($item->prizes) == ($key + 1))
                                                        <a href="{{ route('delete.prize',[$item->id, $prize->id]) }}" class="text-danger ml-2"  onclick="return confirm('Are you sure to delete this raffle prize?')">
                                                            <i class="fa fa-times"></i>
                                                        </a>
                                                    @endif
                                                </td>

                                                <div class="modal fade" id="updatePrize{{ $item->id.$prize->id }}" tabindex="-1" role="dialog" aria-labelledby="createPrize{{ $item->id }}Label" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h5 class="modal-title" id="createPrize{{ $item->id }}Label">Update prize for ( {{ $item->title }} )</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        </div>
                                                        <form action="{{ route('update.prize', $prize->id) }}" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="raffle_id" value="{{ $item->id }}">
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                 
                                                                        <div class="col-md-6">
                                                                            <label for="position">Position</label>
                                                                            <input type="number" class="form-control" id="position" value="{{ old('position', $prize->position) }}" disabled>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <label for="prize_amount">Prize Amount (USD)</label>
                                                                            <input type="text" class="form-control" id="prize_amount" name="prize_amount" value="{{ old('prize_amount', $prize->prize_amount) }}" placeholder="Enter prize amount">
                                                                            @error ('prize_amount')
                                                                                <p class="text-danger">{{ $message }}</p>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-success">Update</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    </div>
                                                </div>
                                            </tr>
                                            
                                        @endforeach
                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>{{-- Card Body --}}
                        <div class="card-footer">
                            <div style="width: 200px" class="float-left">
                                Slots: {{ count($item->slots) }} | Slot price: {{ $item->slot_price }}$
                            </div>

                            @if (count($item->prizes))
                            <a href="{{ route('raffle.publish', $item->id) }}" onclick="return confirm('Are you sure to publish this raffle?')" class="btn btn-success btn-sm ml-2 float-right">Publish</a>
                            @endif
                            
                            <button type="button" class="btn btn-info btn-sm  ml-2 float-right" data-toggle="modal" data-target="#updateRaffle{{ $item->id }}">
                                Edit
                            </button>

                            <button type="button" class="btn btn-danger btn-sm ml-2 float-right" data-toggle="modal" data-target="#deleteRaffle{{ $item->id }}">
                                Delete
                            </button>

                            {{-- <a href="{{ route('raffle.destroy', $item->id) }}" onclick="return confirm('Are you sure to publish this raffle?')" class="btn btn-success btn-sm ml-2 float-right">Delete</a> --}}

                            <div class="modal fade" id="updateRaffle{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="createRaffleLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="createRaffleLabel">Update raffle</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <form action="{{ route('raffle.update', $item->id) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <div class="modal-body">
                                            <div class="form-group">
                                            <label for="title">Raffle Title</label>
                                            <input type="text" class="form-control" id="title" name="title" value="{{ old('title', $item->title) }}" placeholder="Enter raffle title">
                                            @error ('title')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                            </div>
            
                                            <div class="form-group">
                                            <label for="description">Raffle Description</label>
                                            <textarea class="form-control" id="description" name="description" placeholder="Enter raffle description">{{ old('description', $item->description) }}</textarea>
                                                @error ('description')
                                                    <p class="text-danger">{{ $message }}</p>
                                                @enderror
                                            </div>
            
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label for="total_slots">Total slots</label>
                                                        <input type="number" class="form-control" id="total_slots" name="total_slots" value="{{ old('total_slots', count($item->slots)) }}" placeholder="Enter total slots">
                                                        @error ('total_slots')
                                                            <p class="text-danger">{{ $message }}</p>
                                                        @enderror
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label for="slot_price">Slot Price (USD)</label>
                                                        <input type="text" class="form-control" id="slot_price" name="slot_price" value="{{ old('slot_price', $item->slot_price) }}" placeholder="Enter slot price">
                                                        @error ('slot_price')
                                                            <p class="text-danger">{{ $message }}</p>
                                                        @enderror
                                                    </div>
                                                </div>
                    
                                            </div>
                                    
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-success">Update</button>
                                        </div>
                                    </form>
                                </div>
                                </div>
                            </div>

                            <div class="modal fade" id="deleteRaffle{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="createRaffleLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="createRaffleLabel">Delete raffle!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <form action="{{ route('raffle.destroy', $item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <div class="modal-body">
                                            <h5>Are you sure to delete this raffle?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </div>
                                    </form>
                                </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        @endif
        <!-- /.row -->

        @if (count($runningRaffles))
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-info">
                        <h1 class="card-title">Active Raffles ({{ count($runningRaffles) }})</h1>
                    </div>
                </div>
            </div>  
            @foreach ($runningRaffles as $item)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                        {{ $item->title }}
                        </div>

                        <div class="card-footer">
                            <div style="width: 200px" class="float-left">
                               Total Slots: {{ count($item->slots) }} | Free Slots: {{ count($item->free_slots) }}
                            </div>

                            <a href="{{ route('raffle.show', $item->id) }}"  class="btn btn-success btn-sm ml-2 float-right">
                                View details
                            </a>

                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        @endif
        <!-- /.row -->
        @if (count($drawableRaffles))
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-success">
                        <h1 class="card-title">Waiting for draw ({{ count($drawableRaffles) }})</h1>
                    </div>
                </div>
            </div>  
            @foreach ($drawableRaffles as $item)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                        {{ $item->title }}
                        </div>

                        <div class="card-footer">
                            <div style="width: 200px" class="float-left">
                               Total Slots: {{ count($item->slots) }} | Free Slots: {{ count($item->free_slots) }}
                            </div>

                            <a href="{{ route('raffle.show', $item->id) }}"  class="btn btn-success btn-sm ml-2 float-right">
                                View details
                            </a>

                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        @endif
        <!-- /.row -->

        @if (count($endedRaffles))
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header bg-dark">
                        <h1 class="card-title">Ended Raffles ({{ count($endedRaffles) }})</h1>
                    </div>
                </div>
            </div>  
            @foreach ($endedRaffles as $item)
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-footer">
                            <div style="width: 200px" class="float-left">
                                {{ $item->title }}
                            </div>

                            <a href="{{ route('raffle.show', $item->id) }}"  class="btn btn-success btn-sm ml-2 float-right">
                                <i class="fa fa-eye"></i>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="col-md-12">
                {!! $endedRaffles->render() !!}
            </div>
            </div>
        @endif
        <!-- /.row -->

    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush