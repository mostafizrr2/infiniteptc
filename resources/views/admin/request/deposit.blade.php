@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Deposit records</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Deposit records</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">All Waiting deposit records</h3>
                </div>
                <div class="card-body">
                 <table class="table table-sm">
                     <thead>
                       <tr>
                         <th scope="col">#</th>
                         <th scope="col">Deposited from</th>
                         <th scope="col">Amount (USD)</th>
                         <th scope="col">Amount (BTC)</th>
                         <th scope="col">TRX ID</th>
                         <th scope="col">Created at</th>
                         <th scope="col">Status</th>
                       </tr>
                     </thead>
                     <tbody>
                       @foreach ($waiting_records as $key => $item)
                       <tr>
                         <th scope="row">{{ $key + 1 }}</th>
                         <td>
                             @if ($item->member->avatar != '')
                                 <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                 src="{{ url('storage/member/'. $item->member->avatar) }}" alt="" srcset="">
                             @else
                                 <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                 src="{{ url('defaults/user.png') }}" alt="" srcset="">
                             @endif

                             <a href="{{ route('admin.member', $item->member->username) }}" target="_blank">
                                 {{ $item->member->username }}
                             </a>
                         </td>
                         <td>{{ $item->amount_from }}$</td>
                         <td>{{ $item->amount_to }}</td>
                         <td>{{ $item->trx_id }}</td>
                         <td>{{ $item->created_at->format('d M, Y') }}</td>
                         <td>{{ $item->status_text }}</td>
     
                       </tr>
                       @endforeach
                     </tbody>
                   </table>

                   <div class="mt-2 p-2">
                     {!! $waiting_records->render() !!}
                   </div>

                </div>
            </div>
        </div>

     </div>
     <!-- /.row -->

        <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header">
                       <h3 class="card-title">All submitted deposit record ( Waiting for confirmation )</h3>
                   </div>
                   <div class="card-body">
                    <table class="table table-sm">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Deposited from</th>
                            <th scope="col">Amount (USD)</th>
                            <th scope="col">Amount (BTC)</th>
                            <th scope="col">TRX ID</th>
                            <th scope="col">Submitted at</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($submitted_records as $key => $item)
                          <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>
                                @if ($item->member->avatar != '')
                                    <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                    src="{{ url('storage/member/'. $item->member->avatar) }}" alt="" srcset="">
                                @else
                                    <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                    src="{{ url('defaults/user.png') }}" alt="" srcset="">
                                @endif

                                <a href="{{ route('admin.member', $item->member->username) }}" target="_blank">
                                    {{ $item->member->username }}
                                </a>
                            </td>
                            <td>{{ $item->amount_from }}$</td>
                            <td>{{ $item->amount_to }}</td>
                            <td>{{ $item->trx_id }}</td>
                            <td>{{ $item->updated_at->format('d M, Y') }}</td>
                            <td>{{ $item->status_text }}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#viewDetails{{ $item->id }}">
                                    Confirm!
                                </a>
                            </td>
                          </tr>

                          <div class="modal fade" id="viewDetails{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Fund received confimation</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="{{ route('deposite.received') }}" method="POST">
                                  @csrf
                                  <div class="modal-body">
                                      <input type="hidden" name="deposite_id" value="{{ $item->id }}">
                                      <h6>Are you sure you have recived the fund from the member ??</h6>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-danger">Yes! received that.</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          @endforeach
         
                        </tbody>
                      </table>

                      <div class="mt-2 p-2">
                        {!! $submitted_records->render() !!}
                      </div>
                   </div>
               </div>
           </div>
 
        </div>
        <!-- /.row -->
        
        <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header">
                       <h3 class="card-title">All success deposit records</h3>
                   </div>
                   <div class="card-body">
                    <table class="table table-sm">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Deposited from</th>
                            <th scope="col">Amount (USD)</th>
                            <th scope="col">Amount (BTC)</th>
                            <th scope="col">TRX ID</th>
                            <th scope="col">Confirmed at</th>
                            <th scope="col">Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach ($success_records as $key => $item)
                          <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>
                                @if ($item->member->avatar != '')
                                    <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                    src="{{ url('storage/member/'. $item->member->avatar) }}" alt="" srcset="">
                                @else
                                    <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                    src="{{ url('defaults/user.png') }}" alt="" srcset="">
                                @endif

                                <a href="{{ route('admin.member', $item->member->username) }}" target="_blank">
                                    {{ $item->member->username }}
                                </a>
                            </td>
                            <td>{{ $item->amount_from }}$</td>
                            <td>{{ $item->amount_to }}</td>
                            <td>{{ $item->trx_id }}</td>
                            <td>{{ $item->updated_at->format('d M, Y') }}</td>
                            <td>{{ $item->status_text }}</td>
        
                          </tr>
                          @endforeach
                        </tbody>
                      </table>

                      <div class="mt-2 p-2">
                        {!! $success_records->render() !!}
                      </div>

                   </div>
               </div>
           </div>
 
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush