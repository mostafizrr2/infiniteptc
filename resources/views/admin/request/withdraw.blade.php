@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Withdraw Requests</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Withdraw Requests</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
   
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Withdraw Requests</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Member</th>
                            <th>Available Balance</th>
                            <th>Requested Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($withdraw as $key => $wdr)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $wdr->created_at->format('d M, Y') }}</td>
                          <td>
                            @if ($wdr->member->avatar != '')
                                <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                src="{{ url('storage/member/'. $wdr->member->avatar) }}" alt="" srcset="">
                            @else
                                <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                src="{{ url('defaults/user.png') }}" alt="" srcset="">
                            @endif

                              <a href="{{ route('admin.member', $wdr->member->username) }}" target="_blank">
                                {{ $wdr->member->username }}
                              </a>
                          </td>
                          <td>{{ $wdr->member->available_withdrawal }}$</td>
                          <td>
                            <?php
                            if ($wdr->status == 0)
                            {
                              $cls = 'text-dark';
                            }
                            elseif($wdr->status == 1)
                            {
                              $cls = 'text-success';
                            }
                            elseif($wdr->status == 2)
                            {
                              $cls = 'text-danger';
                            }
                            
                            ?>
                            <span class="{{ $cls }}">
                                {{ $wdr->request_amount }}$
                            </span>
                          </td>
                          <td>
                            <span class="{{ $cls }}">
                              @if ($wdr->status == 0)
                                Pending
                              @elseif($wdr->status == 1)
                                Success
                              @elseif($wdr->status == 2)
                                Declined
                              @endif
                            </span>
                          </td>
                          <td>
                            <a href="" class="badge bg-primary" data-toggle="modal" data-target="#approveWithdraw{{ $wdr->id }}">
                                Approve
                            </a>
                            <a href="" class="badge bg-danger" data-toggle="modal" data-target="#declineWithdraw{{ $wdr->id }}">
                                Decline
                            </a>
                            {{-- <a href="" class="badge bg-danger" data-toggle="modal" data-target="#deletMembership{{ $item->id }}">
                                Delete
                            </a> --}}
                        </td>

                        <div class="modal fade" id="approveWithdraw{{ $wdr->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Approve withdraw request</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <form action="{{ route('withdraw.approve') }}" method="POST">
                                @csrf
                                <div class="modal-body">
                                  <input type="hidden" name="id" value="{{ $wdr->id }}">
                                   <p>
                                     <strong>Requested amount: </strong> <span style="font-size:17px !important">{{ $wdr->request_amount }}</span>$ <br>
                                     <strong>Member Available Balance: </strong> <span style="font-size:17px !important">{{ $wdr->member->available_withdrawal }}</span>$ <br> 
                                     <strong>Wallet address: </strong> {{  ($wdr->member->wallet) ? $wdr->member->wallet->address : "No address found" }}
                                  </p> 
                                   <h4>Are you sure to approve this withdraw ??</h4>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                  <button type="submit" class="btn btn-primary">Send funds</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>

                        <div class="modal fade" id="declineWithdraw{{ $wdr->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Decline Withdraw Request</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <form action="{{ route('withdraw.decline') }}" method="POST">
                                @csrf
                                <div class="modal-body">
                                    <input type="hidden" name="id" value="{{ $wdr->id }}">
                                    <h4>Are you sure to decline this withdraw ??</h4>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                  <button type="submit" class="btn btn-danger">Decline</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>

                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>

                      <div class="mt-2 p-2">
                        {!! $withdraw->render() !!}
                      </div>

                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')

@endpush