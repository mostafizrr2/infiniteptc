
@extends('admin')

@section('title', 'Admin - API Intigration')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">API Integration</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">API Integration</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-light">
            <div class="card-header">
              <h3 class="card-title">API Integration</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('system.info') }}" method="POST"> 
                @csrf
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-10 offset-md-1">


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Google App Key</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="google_app_key" id="full-name" value="{{ old('google_app_key', $system->google_app_key) }}" placeholder="Enter google app key">
                                    <p class="text-danger">
                                        {{ $errors->first('google_app_key') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Google App Secret</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="google_app_secret" id="full-name" value="{{ old('google_app_secret', $system->google_app_secret) }}" placeholder="Enter google app secret">
                                    <p class="text-danger">
                                        {{ $errors->first('google_app_secret') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <br><br><h4>Facebook API</h4> <hr>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Facebook App Key</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="facebook_app_key" id="full-name" value="{{ old('facebook_app_key', $system->facebook_app_key) }}" placeholder="Enter facebook app key">
                                    <p class="text-danger">
                                        {{ $errors->first('facebook_app_key') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Facebook App Secret</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="facebook_app_secret" id="full-name" value="{{ old('facebook_app_secret', $system->facebook_app_secret) }}" placeholder="Enter facebook app secret">
                                    <p class="text-danger">
                                        {{ $errors->first('facebook_app_secret') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <br><br><h4>MapBox</h4> <hr>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">MapBox access token</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="mapbox_access_token" id="full-name" value="{{ old('mapbox_access_token', $system->mapbox_access_token) }}" placeholder="Enter mapbox access token">
                                    <p class="text-danger">
                                        {{ $errors->first('mapbox_access_token') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <br><br><h4>Google Map</h4> <hr>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Google map API key</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="gmap_api_key" id="full-name" value="{{ old('gmap_api_key', $system->gmap_api_key) }}" placeholder="Enter mapbox access token">
                                    <p class="text-danger">
                                        {{ $errors->first('gmap_api_key') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <br><br><h4>Realtime Client ( Pusher) 
                            <a href="https://pusher.com/" target="_blank">pusher.com</a></h4> <hr>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">App ID</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="pusher_app_id" id="full-name" value="{{ old('pusher_app_id', $system->pusher_app_id) }}" placeholder="Enter App ID">
                                    <p class="text-danger">
                                        {{ $errors->first('pusher_app_id') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">App Secret key</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="pusher_app_secret" id="full-name" value="{{ old('pusher_app_secret', $system->pusher_app_secret) }}" placeholder="Enter app secret key">
                                    <p class="text-danger">
                                        {{ $errors->first('pusher_app_secret') }}
                                    </p>
                                </div>
                            </div>
                        </div>


                      </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right">Save Changes</button>
                </div>
            </form>          
        </div>
    </div>
    <!-- /.col-md-12 -->
  </div>
  <!-- /.row -->
</div>
</div>


@endsection


@push('js')
 
@endpush