
@extends('admin')

@section('title', 'Admin - App Uploads')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">App Uploads</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">App Uploads</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-light">
            <div class="card-header">
              <h3 class="card-title">App Uploads</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('system.uploads') }}" method="POST" enctype="multipart/form-data"> 
                @csrf
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-10 offset-md-1">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Main logo</label>
                                </div>
                                <div class="col-md-9">
                                    @if ($system->main_logo)
                                    <img class="img img-thumbnail" src="{{ url('storage/system', $system->main_logo) }}" alt="">
                                    <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete?')" 
                                    href="{{ route('uploads.delete', 'main_logo') }}">Remove</a>
                                    <br><br>
                                    @endif
                                    <input type="file" class="form-control" name="main_logo">
                                    <p>max upload size: 2 MB</p>
                                    <p class="text-danger">
                                        {{ $errors->first('main_logo') }}
                                    </p>
                                </div>
                            </div>
                        </div>
<hr>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Mobile Logo</label>
                                </div>
                                <div class="col-md-9">
                                    @if ($system->mobile_logo)
                                    <img class="img img-thumbnail" src="{{ url('storage/system', $system->mobile_logo) }}" alt="">
                                    <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete?')" 
                                    href="{{ route('uploads.delete', 'mobile_logo') }}">Remove</a>
                                    <br><br>
                                    @endif
                                    <input type="file" class="form-control" name="mobile_logo">
                                    <p>max upload size: 2 MB</p>
                                    <p class="text-danger">
                                        {{ $errors->first('mobile_logo') }}
                                    </p>
                                </div>
                            </div>
                        </div>
<hr>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">App favicon</label>
                                </div>
                                <div class="col-md-9">
                                    @if ($system->favicon)
                                    <img class="img img-thumbnail" src="{{ url('storage/system', $system->favicon) }}" alt="">
                                    <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete?')" 
                                    href="{{ route('uploads.delete', 'favicon') }}">Remove</a>
                                    <br><br>
                                    @endif
                                    <input type="file" class="form-control" name="favicon">
                                    <p>max upload size: 1 MB</p>
                                    <p class="text-danger">
                                        {{ $errors->first('favicon') }}
                                    </p>
                                </div>
                            </div>
                        </div>
<hr>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Web Banner</label>
                                </div>
                                <div class="col-md-9">
                                    @if ($system->web_banner)
                                    <img class="img img-thumbnail" src="{{ url('storage/system', $system->web_banner) }}" alt="">
                                    <br><br>
                                    <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete?')" 
                                    href="{{ route('uploads.delete', 'web_banner') }}">Remove</a>
                                    <br><br>
                                    @endif
                                    <input type="file" class="form-control" name="web_banner">
                                    <p>max upload size: 3 MB</p>
                                    <p class="text-danger">
                                        {{ $errors->first('web_banner') }}
                                    </p>
                                </div>
                            </div>
                        </div>


                      </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right">Save Changes</button>
                </div>
            </form>          
        </div>
    </div>
    <!-- /.col-md-12 -->
  </div>
  <!-- /.row -->
</div>
</div>


@endsection


@push('js')
 
@endpush