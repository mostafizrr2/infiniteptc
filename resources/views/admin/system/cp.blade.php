
@extends('admin')

@section('title', 'Admin - API Intigration')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">API Integration</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">API Integration</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-light">
            <div class="card-header">
              <h3 class="card-title">Coinpayyment API Integration</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('coinpayment.api') }}" method="POST"> 
                @csrf
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-10 offset-md-1">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="cp_title">CP Title (Optional)</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="cp_title" id="cp_title" value="{{ old('cp_title', $system->cp_title) }}" placeholder="Enter wallet title">
                                    <p class="text-danger">
                                        {{ $errors->first('cp_title') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="cp_merchant_email">Merchant Email (Optional)</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="email" class="form-control" name="cp_merchant_email" id="cp_merchant_email" value="{{ old('cp_merchant_email', $system->cp_merchant_email) }}" placeholder="Enter merchant email">
                                    <p class="text-danger">
                                        {{ $errors->first('cp_merchant_email') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="cp_merchant_id">Merchant ID (Optional)</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="cp_merchant_id" id="cp_merchant_id" value="{{ old('cp_merchant_id', $system->cp_merchant_id) }}" placeholder="Enter Merchant ID">
                                    <p class="text-danger">
                                        {{ $errors->first('cp_merchant_id') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="cp_ipn_secret">IPN Secret key (Optional)</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="cp_ipn_secret" id="cp_ipn_secret" value="{{ old('cp_ipn_secret', $system->cp_ipn_secret) }}" placeholder="Enter IPN secret key">
                                    <p class="text-danger">
                                        {{ $errors->first('cp_ipn_secret') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="cp_public_key">Public Key *</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="cp_public_key" id="cp_public_key" value="{{ old('cp_public_key', $system->cp_public_key) }}" placeholder="Enter public key">
                                    <p class="text-danger">
                                        {{ $errors->first('cp_public_key') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="cp_private_key">Private Key *</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="cp_private_key" id="cp_private_key" value="{{ old('cp_private_key', $system->cp_private_key) }}" placeholder="Enter private key">
                                    <p class="text-danger">
                                        {{ $errors->first('cp_private_key') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="cp_private_key">IPN Handler URL</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" readonly class="form-control" id="cp_private_key" value="{{ 'https://'.$_SERVER['HTTP_HOST'].'/ipn-handler.php' }}">
                                    <small>Save this URL to your Coinpayments IPN URL section.</small>
                                </div>
                            </div>
                        </div>


                      </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right">Save Changes</button>
                </div>
            </form>          
        </div>
    </div>
    <!-- /.col-md-12 -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">IPN Handler logs</h3>
            </div>
            <div class="card-body">
             <table class="table table-sm">
                 <thead>
                   <tr>
                     <th scope="col">#</th>
                     <th scope="col">Status</th>
                     <th scope="col">Message</th>
                     <th scope="col">Date</th>
                   </tr>
                 </thead>
                 <tbody>
                   @foreach ($ipnLogs as $key => $item)
                   <tr>
                     <th scope="row">{{ $key + 1 }}</th>
                     <td>
                         @if ($item->status == 'error')
                         
                             <span class="text-danger">Error</span>

                         @elseif($item->status == 'success')

                             <span class="text-success">Success</span>

                         @elseif($item->status == 'working')

                             <span class="text-primary">Working</span>

                         @elseif($item->status == 'pending') 

                             <span class="text-info">Pending</span>

                         @elseif($item->status == 'declined') 

                             <span>Declined</span>
                         @endif
                     </td>
                     <td>{{ $item->message }}</td>
                     <td>{{ $item->created_at->format('d M, Y') }}</td>
                   </tr>
                   @endforeach
                 </tbody>
               </table>

               <div class="mt-2 p-2">
                 {!! $ipnLogs->render() !!}
               </div>

            </div>
        </div>
    </div>

 </div>
 <!-- /.row -->

</div>
</div>


@endsection


@push('js')
 
@endpush