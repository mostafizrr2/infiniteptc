@extends('admin')

@section('title', 'Admin - App Info')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">App Info</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">App Info</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-light">
            <div class="card-header">
              <h3 class="card-title">App Info</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('system.info') }}" method="POST"> 
                @csrf
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-6 offset-md-3">

                        <br>
                        <br><br><h4>App info</h4> <hr>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Title</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="title" id="full-name" value="{{ old('title', $system->title) }}" placeholder="Enter title">
                                    <p class="text-danger">
                                        {{ $errors->first('title') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Slogan</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="slogan" id="full-name" value="{{ old('slogan', $system->slogan) }}" placeholder="Enter slogan">
                                    <p class="text-danger">
                                        {{ $errors->first('slogan') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <br><br><h4>Description</h4> <hr>
                        <div class="form-group">
                            <textarea class="form-control" name="description" id="" cols="30" rows="10">{{ old('description', $system->description) }}</textarea>
                                {{ $errors->first('description') }}
                            </p>
                        </div>

                        
                        <br><br><h4>Contact info</h4> <hr>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Contact Phone</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="contact_phone" id="full-name" value="{{ old('contact_phone', $system->contact_phone) }}" placeholder="Enter primary contact number">
                                    <p class="text-danger">
                                        {{ $errors->first('contact_phone') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Contact Email</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="contact_email" id="full-name" value="{{ old('contact_email', $system->contact_email) }}" placeholder="Enter contact email">
                                    <p class="text-danger">
                                        {{ $errors->first('contact_email') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <br><br><h4>Address info</h4> <hr>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Street address</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="street_address" id="full-name" value="{{ old('street_address', $system->street_address) }}" placeholder="Enter street address">
                                    <p class="text-danger">
                                        {{ $errors->first('street_address') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">City</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="city" id="full-name" value="{{ old('city', $system->city) }}" placeholder="Enter city name">
                                    <p class="text-danger">
                                        {{ $errors->first('city') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="exampleInputEmail1">Country</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="country" id="full-name" value="{{ old('country', $system->country) }}" placeholder="Enter country name">
                                    <p class="text-danger">
                                        {{ $errors->first('country') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <br><br><h4>Geo location info</h4> <hr>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="lat">Latitude</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" id="lat" class="form-control" name="latitude" id="full-name" value="{{ old('latitude', $system->latitude) }}" placeholder="Enter latitude">
                                    <p class="text-danger">
                                        {{ $errors->first('latitude') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="long">Longitude</label>
                                </div>
                                <div class="col-md-8">
                                    <input type="text" id="long" class="form-control" name="longitude" id="full-name" value="{{ old('longitude',$system->longitude) }}" placeholder="Enter longitude">
                                    <p class="text-danger">
                                        {{ $errors->first('longitude') }}
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-8">
                                    <button type="button" onclick="getLocation()">Get Latitude and logitude from isp</button>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right">Save Changes</button>
                </div>
            </form>          
        </div>
    </div>
    <!-- /.col-md-12 -->
  </div>
  <!-- /.row -->
</div>
</div>


@endsection


@push('js')
<script>

    var x = document.getElementById("demo");
    var lat = document.getElementById("lat");
    var long = document.getElementById("long");

    function getLocation() {
        console.log(navigator.geolocation);
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
      } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
      }
    }
    
    function showPosition(position) {
    //   x.innerHTML = "Latitude: " + position.coords.latitude + 
    //   "<br>Longitude: " + position.coords.longitude;
        lat.value = position.coords.latitude;
        long.value = position.coords.longitude;
    }
</script>    
@endpush