
@extends('admin')

@section('title', 'Admin - Meta Scripts')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Meta Scripts</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Meta Scripts</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-light">
            <div class="card-header">
              <h3 class="card-title">Meta Scripts</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('system.info') }}" method="POST"> 
                @csrf
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-10 offset-md-1">


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Header scripts</label>
                                </div>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="header_scripts" id="" cols="30" rows="12">{!! old('header_scripts',$system->header_scripts) !!}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-3">
                                    <label for="exampleInputEmail1">Footer scripts</label>
                                </div>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="footer_scripts" id="" cols="30" rows="12">{!! old('footer_scripts',$system->footer_scripts) !!}</textarea>
                                </div>
                            </div>
                        </div>


                      </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right">Save Changes</button>
                </div>
            </form>          
        </div>
    </div>
    <!-- /.col-md-12 -->
  </div>
  <!-- /.row -->
</div>
</div>


@endsection


@push('js')
 
@endpush