@extends('admin')

@section('title', 'Admin - Basic SEO')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Basic SEO</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Basic SEO</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-light">
            <div class="card-header">
              <h3 class="card-title">Basic SEO</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('system.info') }}" method="POST"> 
                @csrf
                <div class="card-body">
                    <div class="row">
                      <div class="col-md-6 offset-md-3">


                        <div class="form-group">
                            <label for="exampleInputEmail1">Meta Title</label>
                            <input type="text" class="form-control" name="meta_title" id="full-name" value="{{ old('meta_title', $system->meta_title) }}" placeholder="Enter meta title">
                            <p class="text-danger">
                                {{ $errors->first('meta_title') }}
                            </p>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Meta Description</label>
                            <textarea class="form-control" name="meta_description" id="" cols="30" rows="10">{{ old('meta_description', $system->meta_description) }}</textarea>
                                {{ $errors->first('meta_description') }}
                            </p>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Meta Keywords</label>
                            <input type="text" class="form-control" name="meta_keywords" id="full-name" value="{{ old('meta_keywords', $system->meta_keywords) }}" placeholder="Enter meta title">
                            <small>Seperate every keywords by comma ( , )</small>
                            <p class="text-danger">
                                {{ $errors->first('meta_keywords') }}
                            </p>
                        </div>


                      </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary float-right">Save Changes</button>
                </div>
            </form>          
        </div>
    </div>
    <!-- /.col-md-12 -->
  </div>
  <!-- /.row -->
</div>
</div>


@endsection


@push('js')
 
@endpush