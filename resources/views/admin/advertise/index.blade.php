@extends('admin')


@push('header')

@endpush

@section('content')
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $title }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                <li class="breadcrumb-item active">{{ $title }}</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">{{ $title }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                        <table class="table table-sm">

                            <thead>
                              <tr>
                                <th style="width: 10px">#</th>
                                <th>Title</th>
                                {{-- <th>Link</th> --}}
                                <th>require clicks</th>
                                @if ($type != 'pending')
                                    <th>Total Clicked</th>
                                @endif
                                <th>Advertisment</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <th>Action</th>
                              </tr>
                            </thead>

                            <tbody>

                            @foreach ($advertises as $key => $ad)
                            <tr>
                              <td>{{ $key + 1 }}</td>
                              <td>{{ $ad->title }}</td>
                              {{-- <td>{{ $ad->link }}</td> --}}
                              <td>{{ $ad->required_clicks }}</td>

                              @if ($type != 'pending')
                                <td>{{ $ad->total_clicked }}</td>
                              @endif

                              <td>{{ ($ad->is_running == 0) ? "Paused" : "Running" }}</td>
                              <td>
                                  @if ($ad->status == 0)
                                  <span class="text-info">Pending</span>
                                  @elseif($ad->status == 1)
                                  <span class="text-success">Approved</span>
                                  @elseif($ad->status == 2)
                                  <span class="text-default">Ended</span>
                                  @elseif($ad->status == 3)
                                  <span class="text-danger">Declined</span>
                                  @endif
                              </td>
                              <td>{{ $ad->created_at->format('d M, Y') }}</td>
                              <td>

                                <a href="" class="badge bg-primary" data-toggle="modal" data-target="#approve{{ $ad->id }}">
                                    View
                                </a>

                                @if ($ad->status != 2 && $ad->status != 3)
                                <a href="" class="badge bg-danger" data-toggle="modal" data-target="#decline{{ $ad->id }}">
                                    Decline
                                </a>
                                @endif
                              </td>


                            <div class="modal fade" id="approve{{ $ad->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Advertise approval window</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <form action="{{ route('pending.ads.approve') }}" method="POST">
                                    @csrf
                                    <div class="modal-body">

                                        <input type="hidden" name="advertise_id" value="{{ $ad->id }}">

                                    <h4>{{ $ad->title }}</h4>
                                    <p>{{ $ad->description }}</p>
                                    <a target="_blank" href="{{ $ad->link }}">{{ $ad->link }}</a>
                                        <hr>
                                    Advertise by: <a target="_blank" href="{{ route('admin.member', $ad->member->username) }}">{{ $ad->member->username }}</a>
                                    <br>
                                    Available Clicks: {{ $ad->member->available_adclick }}
                                    <br>
                                    Required Clicks: {{ $ad->required_clicks }}

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        @if ($type == 'pending')
                                        <button type="submit" class="btn btn-success">Approve Now</button>
                                        @endif
                                    </div>
                                    </form>
                                </div>
                                </div>
                            </div>


                              <div class="modal fade" id="decline{{ $ad->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Decline!</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>

                                    <form action="{{ route('pending.ads.decline') }}" method="POST">
                                      @csrf
                                      <div class="modal-body">
                                        <input type="hidden" name="advertise_id" value="{{ $ad->id }}">
                                        <h5>Are you sure to decline this Advertisement?</h5>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-danger">Decline</button>
                                      </div>
                                    </form>


                                  </div>
                                </div>
                              </div>

                            </tr>
                            @endforeach

                            </tbody>
                        </table>

                        <div class="mt-2 p-2">
                          {!! $advertises->render() !!}
                        </div>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<!-- /.content -->
@endsection

@push('footer')

@endpush
