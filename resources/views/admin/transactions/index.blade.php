@extends('admin')


@push('header')

@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Transactions History</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Transactions History</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
   
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Transactions History</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Member</th>
                            <th>Amount</th>
                            {{-- <th>Walltet</th> --}}
                            <th>TransactionId</th>
                            <th>Status</th>
                            <th>Message</th>
                            
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($transactions as $key => $wdr)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $wdr->created_at->format('d F, Y') }}</td>
                          <td>
                            @if ($wdr->member->avatar != '')
                                <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                src="{{ url('storage/member/'. $wdr->member->avatar) }}" alt="" srcset="">
                            @else
                                <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                src="{{ url('defaults/user.png') }}" alt="" srcset="">
                            @endif

                              <a href="{{ route('admin.member', $wdr->member->username) }}" target="_blank">
                                {{ $wdr->member->username }}
                              </a>
                          </td>
                          <td><span class="">{{ $wdr->amount }}$</span></td>
                          <td>{{ $wdr->transaction_id }}</td>
                          <td>
                            <?php 
                                  if($wdr->status == 0)
                                  {
                                    $cls = "text-danger";
                                  }
                                  else 
                                  {
                                    $cls = "text-success";
                                  }
                              ?>
                            <span class="{{ $cls }}">
                              @if ($wdr->status == 0)
                                Error
                              @elseif($wdr->status == 1)
                                Success
                              @endif
                            </span>
                          </td>
                          {{-- <td>{{ $wdr->wallet_address }}</td> --}}
                          <td>{{ $wdr->message }}</td>
                          
            
                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>

                      <div class="mt-2 p-2">
                        {!! $transactions->render() !!}
                      </div>

                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')

@endpush