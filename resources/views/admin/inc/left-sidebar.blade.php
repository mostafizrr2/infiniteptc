<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.index') }}" class="brand-link text-center">
      @if ($system->main_logo)
      <img style="height: 30px; width: 120px;" src="{{ url('storage/system', $system->main_logo ) }}" alt="{{ $system->title }}">
      @else
      <img style="height: 30px; width: 120px;" src="{{ asset('assets/images/logo.png') }}" alt="{{ $system->title }}">
      @endif
    </a>
    
    <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="{{ route('admin.index') }}" class="nav-link {{ setActive(['admin.index'], 'active') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>Admin Dashboard</p>
                </a>
            </li>

            <li class="nav-item has-treeview {{ setActive(['level.index', 'membership.index', 'adpack.index'], 'menu-open') }}">
              <a href="#" class="nav-link">
                <i class="fa fa-tasks nav-icon"></i>
                <p>
                  Company Plan
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
  
                <li class="nav-item">
                  <a href="{{ route('level.index') }}" class="nav-link {{ setActive(['level.index'], 'active') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Member Levels</p>
                  </a>
                </li>

                <li class="nav-item ">
                  <a href="{{ route('membership.index') }}" class="nav-link {{ setActive(['membership.index'], 'active') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Memberships</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="{{ route('adpack.index') }}" class="nav-link {{ setActive(['adpack.index'], 'active') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Advertise packs</p>
                  </a>
                </li>
  
              </ul>
          </li>

            <li class="nav-item has-treeview {{ setActive(['admin.addMember','admin.allMembers', 'admin.paidMembers', 'admin.freeMembers', 'admin.blockedMembers'], 'menu-open') }}">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-users"></i>
                <p>
                  Manage Members
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
  
                <li class="nav-item ">
                  <a href="{{ route('admin.addMember') }}" class="nav-link {{ setActive(['admin.addMember'], 'active') }}">
                    <i class="fa fa-user-plus nav-icon"></i>
                    <p>Add Member</p>
                  </a>
                </li>

                <li class="nav-item ">
                  <a href="{{ route('admin.allMembers') }}" class="nav-link {{ setActive(['admin.allMembers'], 'active') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>All Members</p>
                  </a>
                </li>
  
                <li class="nav-item">
                  <a href="{{ route('admin.paidMembers') }}" class="nav-link {{ setActive(['admin.paidMembers'], 'active') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Paid Members</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="{{ route('admin.freeMembers') }}" class="nav-link {{ setActive(['admin.freeMembers'], 'active') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Free Members</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="{{ route('admin.blockedMembers') }}" class="nav-link {{ setActive(['admin.blockedMembers'], 'active') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Blocked Members</p>
                  </a>
                </li>
  
              </ul>
          </li>

          <li class="nav-item has-treeview {{ setActive(['pending.ads', 'approved.ads', 'declined.ads', 'all.ads'], 'menu-open') }}">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-edit"></i>
              <p>
                Advertisements
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item ">
                <a href="{{ route('pending.ads') }}" class="nav-link {{ setActive(['pending.ads'], 'active') }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pending</p>
                </a>
              </li>

              <li class="nav-item ">
                <a href="{{ route('approved.ads') }}" class="nav-link {{ setActive(['approved.ads'], 'active') }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Approved</p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="{{ route('declined.ads') }}" class="nav-link {{ setActive(['declined.ads'], 'active') }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Declined</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{ route('all.ads') }}" class="nav-link {{ setActive(['all.ads'], 'active') }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>All advertisements</p>
                </a>
              </li>

            </ul>
        </li>

        <li class="nav-item">
          <a href="{{ route('raffle.index') }}" class="nav-link {{ setActive(['raffle.index'], 'active') }}">
            <i class="nav-icon fa fa-list-ul" aria-hidden="true"></i>
            <p>Raffle Drawings</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('check.deposits') }}" class="nav-link {{ setActive(['check.deposits'], 'active') }}">
            <i class="fa fa-btc nav-icon" aria-hidden="true"></i>
            <p>Member Deposits</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('pending.withdraw') }}" class="nav-link {{ setActive(['pending.withdraw'], 'active') }}">
            <i class="fa fa-google-wallet nav-icon" aria-hidden="true"></i>
            <p>Withdraw Requests</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('withdraw.history') }}" class="nav-link {{ setActive(['withdraw.history'], 'active') }}">
              <i class="nav-icon fa fa-history nav-icon" aria-hidden="true"></i>
              <p>Withdraw History</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('admin.earning') }}" class="nav-link {{ setActive(['admin.earning'], 'active') }}">
              <i class="fa fa-usd nav-icon" aria-hidden="true"></i>
              <p>Admin Earnings</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="{{ route('transaction.history') }}" class="nav-link {{ setActive(['transaction.history'], 'active') }}">
              <i class="fa fa-history nav-icon" aria-hidden="true"></i>
              <p>All Transactions</p>
          </a>
        </li>
    
            <li class="nav-header">Settings & Tools</li>

            <li class="nav-item">
              <a href="{{ route('coinpayment.api') }}" class="nav-link {{ setActive(['coinpayment.api'], 'active') }}">
                  <i class="fa fa-btc nav-icon" aria-hidden="true"></i>
                  <p>Coinpayment</p>
              </a>
            </li>
            
            <li class="nav-item has-treeview {{ setActive(['system.info', 'seo.info', 'system.uploads', 'meta.scripts','api.integration'], 'menu-open') }}">
                <a href="#" class="nav-link">
                  <i class="nav-icon fa fa-cog"></i>
                  <p>
                    App Settings
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
    
                  <li class="nav-item ">
                    <a href="{{ route('system.info') }}" class="nav-link {{ setActive(['system.info'], 'active') }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>App info</p>
                    </a>
                  </li>
    
                  <li class="nav-item">
                    <a href="{{ route('seo.info') }}" class="nav-link {{ setActive(['seo.info'], 'active') }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Basic SEO</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="{{ route('system.uploads') }}" class="nav-link {{ setActive(['system.uploads'], 'active') }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>App Uploads</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="{{ route('meta.scripts') }}" class="nav-link {{ setActive(['meta.scripts'], 'active') }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Meta Scripts</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="{{ route('api.integration') }}" class="nav-link {{ setActive(['api.integration'], 'active') }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>API Integration</p>
                    </a>
                  </li>
    
                </ul>
            </li>

            <li class="nav-item has-treeview {{ setActive(['faq.index', 'faq.create', 'faq.edit'], 'menu-open') }}">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-question-circle-o"></i>
                <p>
                   Application FAQ
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
  
                <li class="nav-item ">
                  <a href="{{ route('faq.index') }}" class="nav-link {{ setActive(['faq.index'], 'active') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>All FAQ</p>
                  </a>
                </li>

                <li class="nav-item ">
                  <a href="{{ route('faq.create') }}" class="nav-link {{ setActive(['faq.create'], 'active') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Create FAQ</p>
                  </a>
                </li>
  
              </ul>
          </li>
            
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
</aside>
<!-- /.sidebar -->