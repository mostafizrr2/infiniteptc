@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            {{-- <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Starter Page</li>
            </ol> --}}
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3>{{ $totalEarningCount }}</h3>
  
                  <p>Total Earning</p>
                </div>
                <div class="icon">
                  <i class="fa fa-usd"></i>
                </div>
                <a href="{{ route('admin.earning') }}" class="small-box-footer">Earning history <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                <h3>{{ $memberCount }}</h3>
  
                  <p>Total Member</p>
                </div>
                <div class="icon">
                  <i class="ion ion-android-contacts"></i>
                </div>
                <a href="{{ route('admin.allMembers') }}" class="small-box-footer">View all members <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <h3>{{ $advertiseCount }}</h3>
  
                  <p>Pending advertise</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ route('pending.ads') }}" class="small-box-footer">Check all advertises <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h3>{{ $withdrawCount }}</h3>
  
                    <p>Withdrawal requests</p>
                </div>
                <div class="icon">
                  <i class="fa fa-credit-card"></i>
                </div>
                <a href="{{ route('pending.withdraw') }}" class="small-box-footer">Check all requests <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        @if (count($drawableRaffles))
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title">Raffles Waiting for draw ({{ count($drawableRaffles) }})</h1>
                    </div>
                </div>
            </div>  
            @foreach ($drawableRaffles as $item)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                        {{ $item->title }}
                        </div>

                        <div class="card-footer">
                            <div style="width: 200px" class="float-left">
                              Total Slots: {{ count($item->slots) }} | Free Slots: {{ count($item->free_slots) }}
                            </div>

                            <a href="{{ route('raffle.show', $item->id) }}"  class="btn btn-success btn-sm ml-2 float-right">
                                View details
                            </a>

                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        @endif
        <!-- /.row -->

        <div class="row">
          <div class="col-md-12">
             <div class="card">
               <div class="card-heard">
                <div class="card-header">
                  <h3 class="card-title">Last 12 month statistics</h3>
                </div>
               </div>
               <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      @if($earningChart)
                      {!! $earningChart->container() !!}
                      @endif
                    </div>
                    <div class="col-md-6">
                      @if($withdrawChart)
                      {!! $withdrawChart->container() !!}
                      @endif
                    </div>
                  </div>
                  <div class="row">

            
      
                  </div>
               </div>
             </div>
          </div>
        </div>

    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')

  {{-- ChartScript --}}
  @if($earningChart)
  {!! $earningChart->script() !!}
  @endif

  @if($withdrawChart)
  {!! $withdrawChart->script() !!}
  @endif

@endpush