@extends('admin')


@push('header')

@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $member->username }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.allMembers') }}">Members</a></li>
                <li class="breadcrumb-item active">{{ $member->username }}</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4">
                <div class="card card-dark" style="padding:30px">
                    <!-- form start -->
                    <div class="text-center">
                        @if ($member->avatar != '')
                            <img style="width:150px; height:150px; border-radius:50%" class="img-thumbnail"
                            src="{{ url('storage/member/'. $member->avatar) }}" alt="" srcset="">
                        @else
                            <img style="width:150px; height:150px; border-radius:50%" class="img-thumbnail"
                            src="{{ url('defaults/user.png') }}" alt="" srcset="">
                        @endif
                    </div>
                </div>
                <div class="card card-dark" style="padding:30px">
                    <p>Acount Level - {{  $member->level->level }}</p>
                    <p>Membership - {{ $member->membership->title }}</p>
                    @if ($member->sponsor)
                    <p>Referred By -
                        <a href="{{ route('admin.member', $member->sponsor->username) }}">
                            {{  $member->sponsor->name }}
                        </a>
                    </p>
                    @endif
                    <p>Account Status - {{  ($member->block_status == 0) ? "Active" : "Blocked" }}</p>

                    @if ($member->block_status == 0)
                    <a href="{{ route('admin.memberBlock', $member->username) }}" class="btn btn-warning btn-block"
                        onclick="return confirm('Are you sure to block this user')">
                            Block {{ $member->username }}
                    </a>

                    @else
                    <a href="{{ route('admin.memberUnBlock', $member->username) }}" class="btn btn-success btn-block"
                        onclick="return confirm('Are you sure to unblock this user')">
                            Unblock {{ $member->username }}
                    </a>
                    @endif
                </div>

            </div>
            <!-- /.col-md-6 -->
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            {{ $member->username }}'s details
                        </h3>
                        <a href="{{ route('admin.member.edit', $member->id) }}" class="btn btn-info btn-sm float-right">Update Member Profile</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <tbody>
                                                <tr role="row" class="odd">
                                                    <td>Username</td>
                                                    <td>{{ $member->username }}</td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td>Full Name</td>
                                                    <td>{{ $member->name }}</td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td>Email</td>
                                                    <td>{{ $member->email }}</td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td>Referral ID</td>
                                                    <td>{{ $member->referral_id }}</td>
                                                </tr>

                                                <tr role="row" class="odd">
                                                    <td>Address</td>
                                                    <td>{{ $member->address }}</td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td>State</td>
                                                    <td>{{ $member->state }}</td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td>Country</td>
                                                    <td>{{ $member->country }}</td>
                                                </tr>
                                                <tr role="row" class="odd">
                                                    <td>Zip</td>
                                                    <td>{{ $member->zip }}</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Withdraw History</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Requested Date</th>
                            <th>Approval Date</th>
                            <th>Amount</th>
                            <th>Status</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($member->withdraws as $key => $wdr)
                        @if ($wdr->status == 1 || $wdr->status == 2)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $wdr->created_at->format('d M, Y') }}</td>
                          <td>{{ $wdr->updated_at->format('d M, Y') }}</td>
                          <td>
                            <?php
                            if ($wdr->status == 0)
                            {
                              $cls = 'text-dark';
                            }
                            elseif($wdr->status == 1)
                            {
                              $cls = 'text-success';
                            }
                            elseif($wdr->status == 2)
                            {
                              $cls = 'text-danger';
                            }

                            ?>
                            <span class="{{ $cls }}">
                                {{ $wdr->request_amount }}$
                            </span>
                          </td>
                          <td>
                            <span class="{{ $cls }}">
                              @if ($wdr->status == 0)
                                Pending
                              @elseif($wdr->status == 1)
                                Success
                              @elseif($wdr->status == 2)
                                Declined
                              @endif
                            </span>
                          </td>
                        </tr>
                        @endif
                        @endforeach

                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>

    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')

@endpush
