@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $title }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Admin</a></li>
                <li class="breadcrumb-item active">{{ $title }}</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-2 col-6">
                        <a style="width:80px !important" class="btn btn-dark btn-sm w-10 m-2  {{ setActive(['admin.allMembers'], 'disabled') }}"
                         href="{{ route('admin.allMembers') }}">
                            All Members
                        </a>
                    </div>
                    <div class="col-md-2 col-6">
                        <a style="width:80px !important" class="btn btn-success btn-sm m-2 {{ setActive(['admin.paidMembers'], 'disabled') }}" 
                        href="{{ route('admin.paidMembers') }}">
                            Paid Members
                        </a>
                    </div>
                    <div class="col-md-2 col-6">
                        <a style="width:80px !important" class="btn btn-primary btn-sm m-2 {{ setActive(['admin.freeMembers'], 'disabled') }}" 
                        href="{{ route('admin.freeMembers') }}">
                            Free Members
                        </a>
                    </div>
                    <div class="col-md-2 col-6">
                        <a style="width:80px !important" class="btn btn-danger btn-sm m-2 {{ setActive(['admin.blockedMembers'], 'disabled') }}" 
                        href="{{ route('admin.blockedMembers') }}">
                            Blocked Members
                        </a>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row">

            <div class="col-md-12">
              <div class="card">
                <div class="card-body">

                  <form action="{{ route('admin.allMembers') }}" method="get">

                    <div class="input-group">


                      <?php 
                        
                        if(isset($_GET['field']))
                        {
                          $field = $_GET['field'];
                        } 
                        else 
                        {
                          $field = null;
                        }

                        if(isset($_GET['search']))
                        {
                          $search = $_GET['search'];
                        } 
                        else 
                        {
                          $search = "";
                        }
                      ?>

                      <select class="custom-select" name="field" style="max-width:200px">
                        <option
                        {{ ($field == 'username') ? 'selected' : '' }}
                        value="username">Username</option>
                        <option 
                        {{ ($field == 'email') ? 'selected' : '' }}
                        value="email">Email</option>
                        <option 
                        {{ ($field == 'name') ? 'selected' : '' }}
                        value="name">Name</option>
                      </select>
                      <input type="text" class="form-control" name="search" placeholder="Enter Username, Email or name" value="{{ $search }}" style="max-width:300px">
                      <div class="input-group-append">
                        <button type="submit" class="btn btn-outline-secondary" type="button">Search</button>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
              
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">{{ $title }}</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th style="width: 10px">#</th>
                            <th style="width: 10px">Image</th>
                            <th>Userame</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Level</th>
                            <th>P.I.M</th>
                            <th>Membership</th>
                            <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($members as $key => $item)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>
                            @if ($item->avatar != '')
                                <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                src="{{ url('storage/member/'. $item->avatar) }}" alt="" srcset="">
                            @else
                                <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                src="{{ url('defaults/user.png') }}" alt="" srcset="">
                            @endif
                          </td>
                          <td>{{ $item->username }}</td>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->email }}</td>
                          <td>Level {{ $item->level->level }}</td>
                          <td>
                              {{ $item->matrices->count() }} times
                              @if ($item->matrices->count())
                                <a href="#" class="badge badge-success" data-toggle="modal" data-target="#viewMtx{{ $item->id }}">
                                    <i class="fa fa-eye"></i>
                                </a>
                              @endif
                            </td>
                          <td>
                              {{ $item->membership->title }}
                            
                              @if ($item->membership->type == 'free')
                                  <a href="#" class="badge badge-info" data-toggle="modal" data-target="#change{{ $item->id }}">
                                    Change
                                  </a>
                              @endif
                          </td>
                          <td>
                              <a href="{{ route('admin.member', $item->username) }}" class="badge bg-danger">
                                  View
                              </a>
                          </td>
                        </tr>       
                        
                        <div class="modal fade" id="change{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-level" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="modal-level">Change membership!</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="{{ route('change.membership') }}" method="POST">
                                  @csrf
                                  <div class="modal-body">
                                    <div class="form-group">
                                        <input type="hidden" name="member_id" value="{{ $item->id }}">
                                        <label for="membership_id">Membership plans</label>
                                        <select type="text" class="form-control" id="membership_id" name="membership_id">
                                            <option selected disabled>Select a membership plan</option>
                                            @foreach ($memberships as $key => $plan)
                                                <option value="{{ $plan->id }}">{{ "(". ($key+1) . ")" }} {{ $plan->title }}</option>
                                            @endforeach
                                        </select>
                                        @error('question')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger">Change</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                        </div>


                        <div class="modal fade" id="viewMtx{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="modal-level" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="modal-level">Positions in Matrix ( {{  $item->username }} )</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                  <div class="modal-body text-center">
                            
                             
                                    @foreach ($item->matrices as $key => $mtx)
                                        Position: {{ $key+1 }} &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
                                        Date: {{ $mtx->created_at->format('d M Y') }}
                                        
                                        @if (count($item->matrices) != ($key + 1))
                                            <hr>
                                        @endif
                                    @endforeach
                    
                                      {{-- </table> --}}
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  </div>
                              </div>
                            </div>
                        </div>
                        @endforeach
  
                        </tbody>
                      </table>
                      <div class="mt-2 p-2">
                          {!! $members->render() !!}
                      </div>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>
             
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<!-- /.content -->
@endsection

@push('footer')
    
@endpush