@extends('admin')

@section('title', 'Update member\'s profile')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Update member's profile</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Update member's profile</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-light">
            <div class="card-header">
              <h3 class="card-title">Update member's profile</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('admin.member.update', $member->id) }}" role="form" method="POST">
                @csrf

                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 offset-md-3">

                        <div class="form-group">
                          <label for="username">Username</label>
                          @error('username')
                          - <span class="text-danger">{{ $message }}</span>
                          @enderror
                          <input type="text" class="form-control" id="username" value="{{ old('username', $member->username) }}" placeholder="Enter username" name="username">
                        </div>

                        <div class="form-group">
                          <label for="name">Name</label>
                          @error('name')
                          - <span class="text-danger">{{ $message }}</span>
                          @enderror
                          <input type="text" class="form-control" id="name" value="{{ old('name', $member->name) }}" placeholder="Enter name" name="name">
                        </div>

                        <div class="form-group">
                          <label for="email">Email</label>
                          @error('email')
                          - <span class="text-danger">{{ $message }}</span>
                          @enderror
                          <input type="text" class="form-control" id="email" value="{{ old('email', $member->email) }}" placeholder="Enter email" name="email">
                        </div>


                        <div class="form-group">
                            <label for="address1">Address</label>
                            @error('address')
                                - <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form-control" id="address" value="{{ old('address', $member->address) }}" placeholder="Enter address" name="address">
                        </div>


                        <div class="form-group">
                            <label for="state">State</label>
                            @error('state')
                                - <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form-control" id="state" value="{{ old('state', $member->state) }}" placeholder="Enter state" name="state">
                        </div>

                        <div class="form-group">
                            <label for="zip">Zip</label>
                            @error('zip')
                                - <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form-control" id="zip" value="{{ old('zip', $member->zip) }}" placeholder="Enter zip code" name="zip">
                        </div>
                        <br>
                        <hr>

                        <div class="form-group">
                            <label for="password">Password (Optional)</label>
                            @error('password')
                                - <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form-control" id="password" value="{{ old('password') }}" placeholder="Enter password" name="password">
                            <p class="text-danger"><strong>Warning:</strong> Keep the field blank if you don't want to change the password.</p>
                        </div>

                    </div>
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <a class="btn btn-dark" href="{{ route('admin.member', $member->username) }}">Cancel</a>
                  <button type="submit" class="btn btn-primary float-right">Update Profile</button>
                </div>
            </form>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div>
</div>


@endsection
