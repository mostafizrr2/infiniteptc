@extends('admin')


@push('header')

@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add New Member</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Add New Member</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid"> <br>
   
        <div class="row mb-2">
            <div class="col-md-12">
              <a href="{{ route('admin.allMembers') }}" class="btn btn-warning btn-sm float-right">
                All members
              </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Add Member</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <form action="{{ route('add.member') }}" method="POST">
                                    @csrf

                                    <div class="form-group">
                                        <label for="name">Full Name (*)</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Enter full name">
                                        @error('name')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="username">Username (*)</label>
                                        <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" placeholder="Enter full username">
                                        @error('username')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email (*)</label>
                                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Enter full email">
                                        @error('email')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                  
                                    <div class="form-group">
                                        <label for="country">Country (*)</label>
                                        <select name="country" class="form-control">
                                          <option seleceted disabled>Select a country</option>
                                          @foreach ($countries as $item)
                                            <option value="{{ $item }}" >{{ $item }}</option>
                                          @endforeach
                                        </select>
                                        @error('countries')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password (*)</label>
                                        <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" placeholder="Enter password">
                                        @error('password')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="referral_id">Referral ID (Optional)</label>
                                        <input type="referral_id" class="form-control" id="referral_id" name="referral_id" value="{{ old('referral_id') }}" placeholder="Enter referral_id">
                                        @error('referral_id')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                    
                                    <button type="submit" class="btn btn-primary float-right">Add Member</button>
                                </form>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-body -->
                </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')

@endpush