@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Level Plans</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Level Plans</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Level Plans</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Level</th>
                            <th>Total Positions</th>
                            <th>Payouts</th>
                            <th>Refferal Bonus</th>
                            <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($levels as $key => $item)
                        <tr>
                          <td>{{ $key + 1 }}- </td>
                          <td>Level {{ $item->level }}</td>
                          <td>{{ $item->position }}</td>
                          <td>{{ $item->payouts }}$</td>
                          <td>{{ $item->refferal_bonus }}$</td>
                          <td>
                              <a href="" class="badge bg-primary" data-toggle="modal" data-target="#editLevel{{ $item->id }}">
                                  Update
                              </a>
                          </td>

                          <div class="modal fade" id="editLevel{{ $item->id }}" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit Level</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="{{ route('level.update', $item->id) }}" method="POST">
                                  @csrf
                                  @method('PUT')
                                  <div class="modal-body">
                                    <div class="row">
                                      <div class="col-md-4">
                                        <div class="form-group">
                                          <label for="level">Level *</label>
                                          <input type="number" class="form-control" name="level" id="level" value="{{ old('level', $item->level) }}" placeholder="Level No">
                                          @error('level')
                                            <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                        </div>
                                      </div>
                                      <div class="col-md-8">
                                        <div class="form-group">
                                          <label for="positions">Positions *</label>
                                          <input type="number" class="form-control" name="position" id="positions" value="{{ old('position', $item->position) }}" placeholder="Total positions">
                                          @error('positions')
                                            <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                        </div>
                                      </div>
                                    </div>
              
                                    <div class="row">
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="payouts">Payouts *</label>
                                          <input type="text" class="form-control" name="payouts" id="payouts" value="{{ old('payouts', $item->payouts) }}" placeholder="Payouts">
                                          @error('payouts')
                                            <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                        </div>
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="refferal_bonus">Refferal bonus</label>
                                          <input type="text" class="form-control" name="refferal_bonus" id="refferal_bonus" value="{{ old('refferal_bonus', $item->refferal_bonus) }}" placeholder="Refferal bonus">
                                          @error('refferal_bonus')
                                            <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                        </div>
                                      </div>
                                    </div>
              
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label for="payouts">Description (optional)</label>
                                          <textarea class="form-control" name="description" id="description" placeholder="Description">{{ old('description', $item->description) }}</textarea>
                                          @error('description')
                                            <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                        </div>
                                      </div>
                                    </div>
              
                                  </div>

                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          
                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush