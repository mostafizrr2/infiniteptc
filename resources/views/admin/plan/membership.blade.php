@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Membership Plans</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Membership Plans</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-md-12">
            <a href="#" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#createMembership">
              Create Membership
            </a>

            <div class="modal fade" id="createMembership" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Membership</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form action="{{ route('membership.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-8">
                          <div class="form-group">
                            <label for="title">Title *</label>
                            <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}" placeholder="Membership Title">
                            @error('title')
                              <p class="text-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="can_click">Clicks / day *</label>
                            <input type="number" class="form-control" name="can_click" id="can_click" value="{{ old('can_click') }}" placeholder="Total click">
                            @error('can_click')
                              <p class="text-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="cpc">CPC * (USD) </label>
                            <input type="text" class="form-control" name="cpc" id="cpc" value="{{ old('cpc', 0.00) }}" placeholder="CPC">
                            @error('cpc')
                              <p class="text-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="sponsor_bonus">Sponsor bonus (USD)</label>
                            <input type="text" class="form-control" name="sponsor_bonus" id="sponsor_bonus" value="{{ old('sponsor_bonus',  0.00) }}" placeholder="Refferal bonus">
                            @error('sponsor_bonus')
                              <p class="text-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="admin_fees">Admin Fees (USD)</label>
                            <input type="text" class="form-control" name="admin_fees" id="admin_fees" value="{{ old('admin_fees',  0.00) }}" placeholder="Refferal bonus">
                            @error('admin_fees')
                              <p class="text-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="price">Price (USD)</label>
                            <input type="text" class="form-control" name="price" id="price" value="{{ old('price',  0.00) }}" placeholder="Refferal bonus">
                            @error('price')
                              <p class="text-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="payouts">Description (optional)</label>
                            <textarea class="form-control" name="description" id="description" placeholder="Description">{{ old('description') }}</textarea>
                            @error('description')
                              <p class="text-danger">{{ $message }}</p>
                            @enderror
                          </div>
                        </div>
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Membership Plans</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Title</th>
                            <th>Price</th>
                            <th>Clicks</th>
                            <th>CPC (USD)</th>
                            <th>Sponsor Bonus (USD)</th>
                            <th>Admin Fees (USD)</th>
                            <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>

                         @foreach ($memberships as $key => $item)
                          <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->price }}$</td>
                            <td>{{ $item->can_click }}</td>
                            <td>{{ $item->cpc }}$</td>
                            <td>{{ $item->sponsor_bonus }}$</td>
                            <td>{{ $item->admin_fees }}$</td>
                            <td>
                                <a href="" class="badge bg-primary mr-1" data-toggle="modal" data-target="#editMembership{{ $item->id }}">
                                    Update
                                </a>

                                @if ($item->type != 'free' && $item->type != 'paid')
                                  <a href="" class="badge bg-danger" data-toggle="modal" data-target="#deletMembership{{ $item->id }}">
                                      Delete
                                  </a>
                                @endif

                            </td>

                            <div class="modal fade" id="editMembership{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Membership</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="{{ route('membership.update', $item->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                      <div class="row">
                                        <div class="col-md-8">
                                          <div class="form-group">
                                            <label for="title">Title *</label>
                                            <input type="text" class="form-control" name="title" id="title" value="{{ old('title', $item->title ) }}" placeholder="Membership Title">
                                            @error('title')
                                              <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                          </div>
                                        </div>
                                        <div class="col-md-4">
                                          <div class="form-group">
                                            <label for="can_click">Clicks *</label>
                                            <input type="number" class="form-control" name="can_click" id="can_click" value="{{ old('can_click', $item->can_click) }}" placeholder="Total click">
                                            @error('can_click')
                                              <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="cpc">CPC * (USD)</label>
                                            <input type="text" class="form-control" name="cpc" id="cpc" value="{{ old('cpc', $item->cpc) }}" placeholder="CPC">
                                            @error('cpc')
                                              <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="sponsor_bonus">Sponsor bonus (USD)</label>
                                            <input type="text" class="form-control" name="sponsor_bonus" id="sponsor_bonus" value="{{ old('sponsor_bonus', $item->sponsor_bonus) }}" placeholder="Refferal bonus">
                                            @error('sponsor_bonus')
                                              <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="admin_fees">Admin Fees (USD)</label>
                                            <input type="text" class="form-control" name="admin_fees" id="admin_fees" value="{{ old('admin_fees', $item->admin_fees) }}" placeholder="Refferal bonus">
                                            @error('admin_fees')
                                              <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label for="price">Price (USD)</label>
                                            <input type="text" class="form-control" name="price" id="price" value="{{ old('price', $item->price) }}" placeholder="Refferal bonus">
                                            @error('price')
                                              <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="payouts">Description (optional)</label>
                                            <textarea class="form-control" name="description" id="description" placeholder="Description">{{ old('description', $item->description) }}</textarea>
                                            @error('description')
                                              <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                            <div class="modal fade" id="deletMembership{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Delete!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="{{ route('membership.destroy', $item->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <div class="modal-body">
                                      <h5>Are you sure to delete this Membership?</h5>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                          </tr>                                       
                         @endforeach
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush