@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Ads Plans</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Ads Plans</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-md-12">
            <a href="#" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#createAds">
              Create Ad pack
            </a>

            <div class="modal fade" id="createAds" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Ad pack</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form action="{{ route('adpack.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">

                      <div class="row">

                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="title">title *</label>
                            <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}" placeholder="Adpack title">
                          </div>
                        </div>

                      </div>

                      <div class="row">

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="clicks">Clicks *</label>
                            <input type="number" class="form-control" name="clicks" id="clicks" value="{{ old('clicks') }}"  placeholder="Ad Clicks">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="price">Price * (USD)</label>
                            <input type="text" class="form-control" name="price" id="price" value="{{ old('price', 0.00) }}" placeholder="Ad price">
                          </div>
                        </div>

                      </div>

                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="payouts">Description (optional)</label>
                            <textarea class="form-control" name="description" id="description" placeholder="Description">{{ old('description') }}</textarea>
                          </div>
                        </div>
                      </div>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Ads Plans</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Title</th>
                            <th>Clicks</th>
                            <th>Price</th>
                         
                            <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($adpacks as $key => $ad)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $ad->title }}</td>
                          <td>{{ $ad->clicks }}</td>
                          <td>{{ $ad->price }}$</td>
                          <td>
                              <a href="" class="badge bg-primary" data-toggle="modal" data-target="#editAdpack{{ $ad->id }}">
                                  Update
                              </a>
                              <a href="" class="badge bg-danger" data-toggle="modal" data-target="#deletAdpack{{ $ad->id }}">
                                  Delete
                              </a>
                          </td>

                          <div class="modal fade" id="editAdpack{{ $ad->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Edit Ads</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="{{ route('adpack.update', $ad->id) }}" method="POST">
                                  @csrf
                                  @method('PUT')
                                  <div class="modal-body">

                                    <div class="row">
              
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label for="title">title *</label>
                                          <input type="text" class="form-control" name="title" id="title" value="{{ old('title', $ad->title) }}" placeholder="Adpack title">
                                        </div>
                                      </div>
                                    </div>
              
                                    <div class="row">


                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="clicks">Clicks *</label>
                                          <input type="number" class="form-control" name="clicks" id="clicks" value="{{ old('clicks', $ad->clicks) }}"  placeholder="Ad Clicks">
                                        </div>
                                      </div>
              
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label for="price">Price * (USD)</label>
                                          <input type="text" class="form-control" name="price" id="price" value="{{ old('price', $ad->price) }}" placeholder="Ad price">
                                        </div>
                                      </div>
              
                                    </div>
              
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label for="payouts">Description (optional)</label>
                                          <textarea class="form-control" name="description" id="description" placeholder="Description">{{ old('description', $ad->description) }}</textarea>
                                        </div>
                                      </div>
                                    </div>
              
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="deletAdpack{{ $ad->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Delete!</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="{{ route('adpack.destroy', $ad->id) }}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <div class="modal-body">
                                    <h5>Are you sure to delete this Ads?</h5>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush