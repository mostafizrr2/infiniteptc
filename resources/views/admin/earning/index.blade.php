@extends('admin')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Admin Earnings</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Admin Earnings</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
   
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Admin Earnings</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Earning From</th>
                            <th>Amount</th>
                            <th>Description</th>
                            <th>Date</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($earnings as $key => $earning)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>
                            @if ($earning->member->avatar != '')
                                <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                src="{{ url('storage/member/'. $earning->member->avatar) }}" alt="" srcset="">
                            @else
                                <img style="width:30px; height:30px; border-radius:50%" class="img-thumbnail"
                                src="{{ url('defaults/user.png') }}" alt="" srcset="">
                            @endif
                              <a href="{{ route('admin.member', $earning->member->username) }}" target="_blank">
                                {{ $earning->member->username }}
                              </a>
                          </td>
                          <td>{{ $earning->amount }}$</td>
                          <td>{{ $earning->description }}</td>
                          <td>{{ $earning->created_at->format('d M, Y') }}</td>
                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>

                      <div class="mt-2 p-2">
                        {!! $earnings->render() !!}
                      </div>
                      
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')

@endpush