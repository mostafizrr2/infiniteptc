@extends('admin')


@push('header')
<style>
    .left .direct-chat-text {
        max-width: 60% !important;
    }
    .right .direct-chat-text {
        margin-left: 0px;
        margin-right: 7px;
        width: auto;
        max-width: 60% !important;
        float: right;
    }
    .direct-chat-messages{
        min-height: 350px !important;
    }
</style>
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Chat Room</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Chat Room</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">

                @livewire('chat.messanger', ['roomId' => $roomId])
                
            </div>
            <div class="col-md-3">

                @livewire('chat.room-members', ['roomId' => $roomId])

            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="modal" id="roomCreate">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Create new room</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @livewire('chat.room-form')
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="chatMemberUpdate" tabindex="-1" role="dialog" aria-labelledby="chatMemberUpdateLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add or Remove Members</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @livewire('chat.chat-update', ['roomId' => $roomId])
        </div>
        {{-- <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div> --}}
      </div>
    </div>
  </div>
<!-- /.content -->
@endsection

@push('footer')
<script>

    $(".direct-chat-messages").stop().animate({ scrollTop: $(".direct-chat-messages")[0].scrollHeight}, -1000);
    
    
    window.livewire.hook('afterDomUpdate', () => {
        // Add your custom JavaScript here.
        $(".direct-chat-messages").stop().animate({ scrollTop: $(".direct-chat-messages")[0].scrollHeight}, -1000);
    });
    
</script>
@endpush
