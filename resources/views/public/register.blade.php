@extends('public')

@push('header')

@endpush


@section('title', 'Sign up')

@section('content')

<!-- Breadcrumb Area Start -->
{{-- <section class="page-top"></section> --}}
<!-- Breadcrumb Area End -->

<section class="contact page">
    {{-- <img class="left-img" src="{{ asset('assets/images/contact-left.png') }}" alt=""> --}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="section-heading">
                    <a class="navbar-brand" href="{{ 'http://'.$_SERVER['HTTP_HOST'] }}">
                        <img src="{{ asset('assets/images/logo.png') }}" alt="">
                    </a><br><br>
                    <h5 class="text-light">
                        CREATE AN ACCOUNT
                    </h5>
               
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="contact-form-wrapper">
                    <div class="contact-box">
                        <h4 class="title">
                            Register Form
                        </h4>
                        <div class="form-area">
                            <form action="{{ route('user.register') }}" method="POST">
                                @csrf

								<div class="form-group">
                                    <label for="name">Full Name*</label>
                                    @error('name')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="text" class="input-field" name="name" id="name" value="{{ old('name') }}" placeholder="Enter your Full Name">
                                </div>

								<div class="form-group">
                                    <label for="username">Username* (unique)</label>
                                    @error('username')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="text" class="input-field" name="username" id="username" value="{{ old('username') }}" placeholder="Enter username">
                                </div>
                                
								<div class="form-group">
                                    <label for="input-email">Email* (unique)</label>
                                    @error('email')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="email" class="input-field" name="email" id="input-email" value="{{ old('email') }}" placeholder="Enter your Email">
                                </div>
                                
								<div class="form-group">
                                    <label for="address">Address*</label>
                                    @error('address')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="text" class="input-field" name="address" id="address" value="{{ old('address') }}" placeholder="Enter your street address">
                                </div>
                                
								<div class="form-group">
                                    <label for="state">State*</label>
                                    @error('state')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="text" class="input-field" name="state" id="state" value="{{ old('state') }}" placeholder="Enter your state name">
                                </div>
                                
								<div class="form-group">
                                    <label for="country">Country*</label>
                                    @error('country')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="text" class="input-field" disabled id="country" value="{{ $country }}" placeholder="Enter your country">
                                </div>
                                
								<div class="form-group">
                                    <label for="zip">ZIP*</label>
                                    @error('zip')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="text" class="input-field" name="zip" id="zip" value="{{ old('zip') }}" placeholder="Enter zip code">
                                </div>
                                
								<div class="form-group">
                                    <label for="input-password">Password*</label>
                                    @error('password')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="password" class="input-field" name="password" id="input-password" placeholder="Enter your password">
								</div>
								<div class="form-group">
                                    @error('password_confirmation')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <label for="input-con-password">confirm password**</label>
                                    <input type="password" class="input-field" name="password_confirmation" id="input-con-password" placeholder="Enter your Confirm Password">
                                </div>
                                
                                <div class="form-group">
                                    <label for="referral_id">Referral ID</label>
                                    @error('referral_id')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="text" class="input-field" name="referral_id" id="referral_id" value="{{ old('referral_id', $ref_id ) }}" placeholder="Enter a refferal id" {{ ($ref_id != null) ? 'readonly' : '' }}>
                                </div>
	
								<div class="form-group">
									<div class="check-group">
                                        <input type="checkbox" class="check-box-field" name="terms_agree" id="input-terms" checked="">
                                        <label for="input-terms">
                                                I agree with <a href="#">terms and Conditions</a> and  <a href="#">privacy policy</a>
                                        </label>
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="mybtn1">SIGN UP</button>
								</div>
		
							</form>
                        </div>

                        <div class="form-footer">
                            <p>
                                Already a member? 
                                <a href="{{ route('user.login') }}">
                                    LOGIN <i class="fas fa-angle-double-right"></i>
                                </a>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>    
@endsection

@push('footer')
    
@endpush