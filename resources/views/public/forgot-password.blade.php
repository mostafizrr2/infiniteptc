@extends('public')

@push('header')

@endpush


@section('title', 'Sign in')

@section('content')

<!-- Breadcrumb Area Start -->
{{-- <section class="page-top"></section> --}}
<!-- Breadcrumb Area End -->

<section class="contact page">
    {{-- <img class="left-img" src="{{ asset('assets/images/contact-left.png') }}" alt=""> --}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="section-heading">
                    <a class="navbar-brand" href="{{ 'http://'.$_SERVER['HTTP_HOST'] }}">
                        <img src="{{ asset('assets/images/logo.png') }}" alt="">
                    </a><br><br>
                    <h5 class="text-light">
                        FORGOT PASSWORD
                    </h5>
               
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="contact-form-wrapper">
                    <div class="contact-box">
                        <h4 class="title">
                            Forgot password
                        </h4>

                        @include('public.parts.flash')
                        
                        <div class="form-area">
                            <form action="{{ route('pass.reset.mail') }}"  method="POST">
                                @csrf

                                <div class="form-group">
                                    <label for="login-input-email">Email*</label>
                                    @error('email')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="email" class="input-field" name="email" id="login-input-email"  placeholder="Enter your Email">
                                </div>
               
                                <div class="form-group">
                                    <button type="submit" class="mybtn1">SEND RESET LINK</button>
                                </div>
                            </form>
                        </div>
                        <div class="form-footer">
                            <p>
                                Not a member? 
                                <a href="{{ route('user.register') }}">
                                    Create an account <i class="fas fa-angle-double-right"></i>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>    
@endsection

@push('footer')
    
@endpush