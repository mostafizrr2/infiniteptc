@extends('public')

@push('header')

@endpush


@section('title', 'Sign in')

@section('content')

<!-- Breadcrumb Area Start -->
{{-- <section class="page-top"></section> --}}
<!-- Breadcrumb Area End -->

<section class="contact page">
    {{-- <img class="left-img" src="{{ asset('assets/images/contact-left.png') }}" alt=""> --}}
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="section-heading">
                    <a class="navbar-brand" href="{{ 'http://'.$_SERVER['HTTP_HOST'] }}">
                        <img src="{{ asset('assets/images/logo.png') }}" alt="">
                    </a><br><br>
                    <h5 class="text-light">
                        RESET PASSWORD
                    </h5>
               
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="contact-form-wrapper">
                    <div class="contact-box">
                        <h4 class="title">
                            RESET PASSWORD
                        </h4>

                        @include('public.parts.flash')
                        
                        <div class="form-area">
                            <form action="{{ route('user.pass.reset', ['token' => $token, 'email' => $email]) }}"  method="POST">
                                @csrf

                                <div class="form-group">
                                    <label for="password">New Password*</label>
                                    @error('password')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="password" class="input-field" name="password" id="password"  placeholder="Enter your password">
                                </div>

                                <div class="form-group">
                                    <label for="password">Confirm Password*</label>
                                    @error('password_confirmation')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <input type="password" class="input-field" name="password_confirmation" id="password_confirmation"  placeholder="Enter password again">
                                </div>
               
                                <div class="form-group">
                                    <button type="submit" class="mybtn1">SEND RESET LINK</button>
                                </div>
                            </form>
                        </div>
        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>    
@endsection

@push('footer')
    
@endpush