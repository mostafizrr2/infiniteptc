<div class="mainmenu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">                 
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="{{ route('home') }}">
                        <img src="{{ asset('assets/images/logo.png') }}" alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_menu" aria-controls="main_menu"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse fixed-height" id="main_menu">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link {{ setActive(['home'], 'active') }}" 
                                href="{{ route('home') }}" >
                                    Home <div class="mr-hover-effect"></div>
                                </a>
                            </li>
                  
                            <li class="nav-item">
                                <a class="nav-link" href="contact.html">Abou Us
                                    <div class="mr-hover-effect"></div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact.html">Contact
                                    <div class="mr-hover-effect"></div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ setActive(['user.login'], 'active') }}" 
                                href="{{ route('user.login') }}">Sign In
                                    <div class="mr-hover-effect"></div>
                                </a>
                            </li>
                        </ul>

                        <a href="{{ route('user.register') }}" class="mybtn1"> Join us</a>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>