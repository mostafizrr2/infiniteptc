<section class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="content">
                    <div class="left-content">
                        <ul class="left-list">
                            <li>
                                <p>
                                    <i class="fas fa-headset"></i>	Support
                                </p>
                            </li>
                            <li>
                                <p>
                                    <i class="fas fa-envelope"></i>	info@Dooplo.com
                                </p>
                            </li>
                            <li>
                                <div class="language-selector">
                                    <select name="language" class="language">
                                        <option value="1">English</option>
                                        <option value="2">France</option>
                                        <option value="3">Japan</option>
                                    </select>
                                </div>
                            </li>
                        </ul>
                    </div>
           
                </div>
            </div>
        </div>
    </div>
</section>