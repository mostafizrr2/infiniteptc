<div>
    <div class="card direct-chat direct-chat-warning">
        <div class="card-header">
            <div style="width:90%" class="float-left">
                <h3 class="">{{ $room->title }}</h3>
                <p>{{ $room->description }}</p>
            </div>
            <button type="button" class="btn btn-default btn-sm float-right" wire:click="$emit('refreshChat')">
                <i class="fa fa-refresh"></i>
            </button>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <!-- Conversations are loaded here -->
            <div class="direct-chat-messages">

                @foreach ($messages as $item)
                    <!-- Message. Default to the left -->
                    @if($userClass == 'admin')
                        <div class="direct-chat-msg {{ ($item->sender_type == 'admin') ? 'right' : 'left' }}">
                            <div class="direct-chat-infos clearfix">
                                <span class="direct-chat-name  {{ ($item->sender_type == 'admin') ? 'float-right' : '' }} ">
                                    {{ ($item->sender_type == 'admin') ? $item->user->name . ' (Admin)' : $item->member->name }}
                                </span>
                                <small class="direct-chat-timestamp {{ ($item->sender_type == 'admin') ? 'float-right mr-2' : '' }}">
                                    {{ $item->created_at->diffForHumans() }} 
                                    {{-- - {{ $item->created_at->format('d M, Y') }} --}}
                                </small>
                            </div>
                            <!-- /.direct-chat-infos -->
                            @if($item->sender_type == 'admin')
                                <img class="direct-chat-img" src="{{ url('storage/user', $item->user->avatar) }}" alt="Admin Avatar">
                            @else 
                                @if($item->member->avatar != null)
                                    <img class="direct-chat-img" src="{{ url('storage/member', $item->member->avatar) }}" alt="Member Avatar">
                                @else 
                                    <img class="direct-chat-img" src="{{ asset('adminlte/dist/img/user1-128x128.jpg') }}" alt="Member Avatar">      
                                @endif
                            @endif
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                            {{  $item->message }}
                            </div>
                            <!-- /.direct-chat-text -->
                        </div>
                    @elseif($userClass == 'member')
                        <div class="direct-chat-msg {{ ($item->sender_type == 'member' && $item->member_id == auth('member')->user()->id) ? 'right' : 'left' }}">
                            <div class="direct-chat-infos clearfix">
                                <span class="direct-chat-name {{ ($item->sender_type == 'member' && $item->member_id == auth('member')->user()->id) ? 'float-right' : 'float-left' }} ">
                                
                                    {{ ($item->sender_type == 'admin') ? $item->user->name . ' (Admin)' : $item->member->name }} <br>
                                    
                                </span>
                                <small class="direct-chat-timestamp {{ ($item->sender_type == 'member' && $item->member_id == auth('member')->user()->id) ? 'float-right mr-2' : 'ml-2' }}"
                                    style="margin-top: 3px;">
                                    {{ $item->created_at->diffForHumans() }}
                                     {{-- - {{ $item->created_at->format('d M, Y') }} --}}
                                </small>
                            </div>
                            <!-- /.direct-chat-infos -->
                            @if($item->sender_type == 'member')

                              @if($item->member->avatar != null)
                                <img class="direct-chat-img" src="{{ url('storage/member', $item->member->avatar) }}" alt="Member Avatar">
                              @else 
                                <img class="direct-chat-img" src="{{ asset('adminlte/dist/img/user1-128x128.jpg') }}" alt="Member Avatar">      
                              @endif

                            @else 

                                <img class="direct-chat-img" src="{{ url('storage/user', $item->user->avatar) }}" alt="Admin Avatar">

                            @endif
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                            {{  $item->message }}
                            </div>
                            {{-- <p class="text-muted {{ ($item->sender_type == 'member' && $item->member_id == auth('member')->user()->id) ? 'float-right' : 'float-left' }}">
                                23 Jan 2:00 pm
                            </p> --}}
                            <!-- /.direct-chat-text -->
                        </div>
                        
                    @endif
                    <!-- /.direct-chat-msg -->
                @endforeach

                {{-- <!-- Message to the right -->
                <div class="direct-chat-msg right">
                    <div class="direct-chat-infos clearfix">
                    <span class="direct-chat-name float-right">Sarah Bullock</span>
                    <span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>
                    </div>
                    <!-- /.direct-chat-infos -->
                    <img class="direct-chat-img" src="{{ asset('adminlte/dist/img/user3-128x128.jpg') }}" alt="message user image">
                    <!-- /.direct-chat-img -->
                    <div class="direct-chat-text">
                    You better believe it!
                    </div>
                    <!-- /.direct-chat-text -->
                </div>
                <!-- /.direct-chat-msg --> --}}

            </div>
            <!--/.direct-chat-messages-->

            <!-- /.direct-chat-pane -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <form action="#" wire:submit.prevent="submitMessage" method="post">
            <div class="input-group">
                <input type="text" wire:model="messageText" placeholder="Type Message ..." class="form-control">
                <span class="input-group-append">
                <button type="submit" id="msgBtn" class="btn btn-success">Send</button>
                </span>
            </div>
            </form>
        </div>
        <!-- /.card-footer-->
    </div>
    
</div>
