<div>
    <form wire:submit.prevent="saveChanges">

        {{-- <div class="form-group">
          <label for="exampleInputEmail1">Room Title</label>
          <input type="text" class="form-control" wire:model.lazy="title" placeholder="Enter room title">
          @error('title') <span class="text-danger">{{ $message }}</span> @enderror
        </div> --}}
      
        {{-- <div class="form-group">
          <label for="exampleInputPassword1">Description</label>
          <textarea name="" id="" cols="30" rows="6" wire:model.lazy="description" class="form-control" placeholder="Enter room description"></textarea>
          @error('description') <span class="text-danger">{{ $message }}</span> @enderror
        </div> --}}
      
        <div class="form-group">
          <label for="">Select room members</label>
          <select multiple class="form-control" wire:model.lazy="selectedUsers">
            @if($userClass = 'admin')
              @foreach ($members as $user)
                @if($user->id != $room->user_id)
                  <option  value="{{ $user->id }}" {{ (in_array($user->id, $selectedUsers)) ? "selected" : "" }}>
                    {{ $user->name }}
                  </option> 
                @endif
              @endforeach
            @else
              @foreach ($members as $user)
                @if($user->id != $userId)
                  <option  value="{{ $user->id }}" {{ (in_array($user->id, $selectedUsers)) ? "selected" : "" }}>
                    {{ $user->name }}
                  </option>
                @endif
              @endforeach
            @endif
          </select>
        </div>
      
        <div class="mt-2 mb-2">
          @if (session()->has('success'))
      
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{ session('success') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
        </div>

        <button type="submit" class="btn btn-primary btn-sm float-right">Save Changes</button>
        <button type="button" class="btn btn-secondary btn-sm float-right mr-1" data-dismiss="modal">Cancel</button>
    </form>

</div>
