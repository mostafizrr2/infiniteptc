<div class="card">
    {{-- Care about people's approval and you will be their prisoner. --}}
    <div class="card-header">
        Chat Members

        @if($room->user_id == $userId || $userClass == 'admin')
            <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#chatMemberUpdate">
                Update
            </button>
        @endif

    </div>
    <div class="card-body">
        @if(count($roomMembers))
            <ul class="chat-members">
                @foreach ($roomMembers as $item)
                <li>
                    {{ $item->name }}
                </li>
                @endforeach
            </ul>
        @else 
            <h6>No members available</h6>
        @endif
    </div>
    <div class="card-footer">

        @if($room->user_type == 'member')
            @php
               $creator = $room->member->username;
            @endphp
        @else 
           @php
              $creator = "Admin"; 
           @endphp
        @endif

        Room Creator: <strong>{{ $creator }}</strong> 
    </div>

</div>
