<li class="nav-item dropdown">

    <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
        <i class="fa fa-envelope-o" aria-hidden="true"></i>
        
        @if (count($notifications) > 0)
            <span class="badge badge-info">{{ count($notifications) }}</span>
        @endif
    </a>

    <div class="dropdown-menu dropdown-menu-xl" style="left: inherit; right: 0px;">
        <span class="dropdown-item dropdown-header">{{ count($notifications) }} Unread {{ (count($notifications) > 1) ? "Messages" : "Message" }} </span>
        <div class="dropdown-divider"></div>

        @foreach ($notifications as $key => $item)
        <span class="dropdown-item pt-3 pb-3">
            
            {{ $key+1 }}. {{ $item->notification }} <br>
            <small class="ml-3 text-muted">
                <small>{{ $item->created_at->diffForHumans() }}</small>
            </small>

            <button type="button" wire:click="enterMessageRoom({{ $item->id }})" class="btn btn-info btn-sm float-right">Enter Room</button>
            {{-- <span class="float-right text-muted text-sm">{{ $item->slot_price }}$</span> --}}
        </span>
        <div class="dropdown-divider"></div>
        @endforeach
        
        <a href="{{ route('member.chatrooms') }}" class="dropdown-item dropdown-footer">All Rooms</a>
    </div>
</li>

