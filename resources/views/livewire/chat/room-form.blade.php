<form wire:submit.prevent="submit">

  <div class="form-group">
    <label for="exampleInputEmail1">Room Title</label>
    <input type="text" class="form-control" wire:model.lazy="title" placeholder="Enter room title">
    @error('title') <span class="text-danger">{{ $message }}</span> @enderror
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Description</label>
    <textarea name="" id="" cols="30" rows="6" wire:model.lazy="description" class="form-control" placeholder="Enter room description"></textarea>
    @error('description') <span class="text-danger">{{ $message }}</span> @enderror
  </div>

  <div class="form-group">
    <label for="">Select room members</label>

    <select multiple class="form-control" wire:model="users">
      @foreach ($members as $user)
        @if($userClass == 'member')
            @if($user->id != $userId)
              <option value="{{ $user->id }}">
                {{ $user->name }}
              </option>
            @endif
        @else
              <option value="{{ $user->id }}">
                {{ $user->name }}
              </option>
        @endif
      @endforeach
    </select>
  </div>

  <div class="mt-2 mb-2">
    @if (session()->has('success'))

    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
  </div>

  <button type="submit" class="btn btn-primary float-right">Create room</button>
</form>