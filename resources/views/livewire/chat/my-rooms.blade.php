<div>
    <div class="card bg-dark">
        <div class="card-header">
            <h1 class="card-title">
                My Rooms
                <span class="badge badge-light ml-2">
                {{ $total }}
                </span>
            </h1>
        </div>

    </div>
    
    <div class="mt-2 mb-2">
        @if (session()->has('warning'))

        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ session('warning') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        @endif
    </div>

    
    @foreach ($rooms as $item)
    <div class="card">
        <div class="card-header">
            <h1 class="card-title">{{ $item->title }}</h1> 
            <span class="badge badge-success ml-2">{{ count($item->members) }} Members</span>
            {{-- <small class="ml-2 text-muted">Created by <a href="#">{{ $item['creator'] }}</a></small> --}}
        </div>
        <div class="card-body">
            {{ $item->description }}
        </div>

        <div class="card-footer">

            <span class="float-left text-muted">Discussion going on..</span>

            <a href="{{ route($route, $item->id) }}" class="btn btn-info btn-sm float-right ml-2">
                <i class="fa fa-pencil"></i> Join Chat
            </a>

            <button class="btn btn-danger btn-sm float-right"  data-toggle="modal" data-target="#delete{{ $item->id }}">
                <i class="fa fa-times"></i>
            </button>
            {{-- <small class="ml-2 text-muted">Created by <a href="#">{{ $item['creator'] }}</a></small> --}}
        </div>
    </div>

 
    <div class="modal" id="delete{{ $item->id }}">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Confirmation</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Are you sure to delete this room?</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="button" wire:click="remove({{$item->id}})" class="btn btn-danger">Delete</button>
            </div>
          </div>
        </div>
    </div>
        
    @endforeach

    <div class="text-center mb-3">
        @if ($showLoadButton)
            <button class="btn btn-primary" wire:loading.attr="disabled" wire:click="loadMore()">
                Load more
                <div class="spinner-border spinner-border-sm ml-2" wire:loading role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </button>
        @endif
    </div>


</div>

