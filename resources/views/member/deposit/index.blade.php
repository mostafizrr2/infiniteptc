@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Deposit funds</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Deposit funds</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Your current account balance - <strong>{{ $user->balance }}</strong>$</h4>
                        {!! ($user->balance < 10) ? "<p class='text-danger'>Your account balance is looking low. Please deposite some funds to your account balance.</p>" : "" !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header">
                       <h4>Deposit funds to account</h4>
                   </div>
                   <div class="card-body">
                      <div class="row">
                          <div class="col-lg-6 col-6">
                              <div class="small-box bg-danger">
                                <div class="inner">
                                  <h5>Deposit from waithdrawal balance</h5>
                                  <p>You can deposite 2$ minimum.</p>
                                </div>
                                <div class="icon">
                                  <i class="ion ion-stats-bars"></i>
                                </div>
                                  <input type="hidden" name="adpack_id">
                                  <a href="javascript:void(0)" class="small-box-footer"  data-toggle="modal" data-target="#depositFromEarning">
                                      Deposit Now <i class="fas fa-arrow-circle-right"></i>
                                  </a>
                              </div>
                          </div>

                          <div class="col-lg-6 col-6">
                            <div class="small-box bg-info">
                              <div class="inner">
                                <h5>Deposit from external wallet</h5>
                                <p>You can deposite 7.5$ minimum.</p>
                              </div>
                              <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                              </div>
                                <input type="hidden" name="adpack_id">
                                <a href="javascript:void(0)" class="small-box-footer"  data-toggle="modal" data-target="#depositFromOutside">
                                    Deposit Now <i class="fas fa-arrow-circle-right"></i>
                                </a>
                              </div>
                           </div>

                          <div class="modal fade" id="depositFromEarning" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Deposit from waithdrawal balance</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="{{ route('deposit.earning') }}" method="POST">
                                    @csrf
                                    <div class="modal-body">
                                      <div class="text-center">
                                        <p>
                                          You can deposit minimum <strong>2</strong>$<br>
                                          You can deposit Maximum <strong>{{ (int) $user->available_withdrawal }}</strong>$ (Your available withdrawal balance)<br>
                                        </p>
                                      </div>

                                      <div class="form-group">
                                        <div class="row">
                                          <div class="col-md-2">
                                          </div>
                                          <div class="col-md-2">
                                            <label for="amount">Amount</label>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="input-group mb-3">
                                              <input type="text" class="form-control form-control" onkeyup="checkAuth()" name="amount" id="amount" value="2" placeholder="Enter amount">
                                              <div class="input-group-append input-group-append">
                                                <span class="input-group-text" id="basic-addon2">$</span>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-2"></div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-12 text-center">
                                            @error('amount')
                                              <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                            <p class="text-danger" id="message"></p>
                                          </div>
                                        </div>
                                      </div>
                   
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                      <button type="submit" id="btn-deposit" class="btn btn-success">Continue</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                          </div>

                          <div class="modal fade" id="depositFromOutside" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Deposit from external wallet</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="{{ route('deposit.outside') }}" method="POST">
                                    @csrf
                                    <div class="modal-body">
                                      <div class="text-center">
                                        <p>
                                          You can deposit minimum <strong>7.5</strong>$<br>
                                        </p>
                                      </div>

                                      <div class="form-group">
                                        <div class="row">
                                          <div class="col-md-2">
                                          </div>
                                          <div class="col-md-2">
                                            <label for="amount">Amount</label>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="input-group mb-3">
                                              <input type="text" class="form-control form-control" onkeyup="checkAuthDeposite()" name="amount" id="amount-outside" value="7.5" placeholder="Enter amount">
                                              <div class="input-group-append input-group-append">
                                                <span class="input-group-text" id="basic-addon2">$</span>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-2"></div>
                                        </div>
                                        <div class="row">
                                          <div class="col-md-12 text-center">
                                            @error('amount')
                                              <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                            <p class="text-danger" id="message-outside"></p>
                                          </div>
                                        </div>
                                      </div>
                   
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                      <button type="submit" id="btn-deposit-outside" class="btn btn-success">Yes! Deposit</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                          </div>

                      </div>
                   </div>
               </div>
           </div>
        </div>
        {{-- Row --}}

        <div class="row">
           <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Waiting for funds receive</h4>
                </div>
                <div class="card-body">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Amount (USD)</th>
                        <th scope="col">Amount (BTC)</th>
                        <th scope="col">TRX ID</th>
                        <th scope="col">Date</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($depositWaiting as $key => $item)
                      <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <td>{{ $item->amount_from }}$</td>
                        <td>{{ $item->amount_to }}</td>
                        <td>{{ $item->trx_id }}</td>
                        <td>{{ $item->created_at->format('d M, Y') }}</td>
                        <td>{{ $item->status_text }}</td>
                        <td>
                            <a href="{{ route('member.transaction', $item->id ) }}">View details</a>
                        </td>
                      </tr>
                      @endforeach
     
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
      </div>


      <div class="row">
           <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Deposite history and statuses</h4>
                </div>
                <div class="card-body">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Amount (USD)</th>
                        <th scope="col">Amount (BTC)</th>
                        <th scope="col">TRX ID</th>
                        <th scope="col">Date</th>
                        <th scope="col">Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($depositHistory as $key => $item)
                      <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <td>{{ $item->amount_from }}$</td>
                        <td>{{ $item->amount_to }}</td>
                        <td>{{ $item->trx_id }}</td>
                        <td>{{ $item->created_at->format('d M, Y') }}</td>
                        <td>
                          @if ($item->status == 'submitted')
                            <span>{{ $item->status_text }}</span> 
                          @elseif($item->status == 'pending')
                            <span class="text-info">{{ $item->status_text }}</span> 
                          @elseif($item->status == 'declined')
                            <span class="text-danger">{{ $item->status_text }}</span> 
                          @elseif($item->status == 'success')
                            <span class="text-success">{{ $item->status_text }}</span> 
                          @endif
                        </td>
                      </tr>
                      @endforeach
     
                    </tbody>
                  </table>

                  
                  <div class="mt-2 p-2">
                    {!! $depositHistory->render() !!}
                  </div>


                </div>
            </div>
        </div>
      </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
<script>
 
  function checkAuth()
  {
    var available_withdrawal = '{{ $user->available_withdrawal }}';
    var min_deposit = 2;
    var val = $('#amount').val();

    if(val < min_deposit )
    {
       console.log("You cannot deposit less than "+ min_deposit +"$");
       $('#message').html("You cannot deposit less than "+ min_deposit +"$");
       $('#btn-deposit').addClass('d-none');
    }
    else if(val > parseFloat(available_withdrawal) )
    {
       console.log("You cannot exceet your available balance "+ available_withdrawal +"$");
       $('#message').html("You cannot exceed your available balance "+ available_withdrawal +"$");
       $('#btn-deposit').addClass('d-none');
    }
    else 
    {
      $('#message').html("");
      $('#btn-deposit').removeClass('d-none');
    }

  }

  function checkAuthDeposite()
  {

    var min_deposit = 7.5;
    var val = $('#amount-outside').val();

    if(val < min_deposit )
    {
       console.log("You cannot deposit less than "+ min_deposit +"$");
       $('#message-outside').html("You cannot deposit less than "+ min_deposit +"$");
       $('#btn-deposit-outside').addClass('d-none');
    }
    else 
    {
      $('#message-outside').html("");
      $('#btn-deposit-outside').removeClass('d-none');
    }

  }
</script> 
@endpush