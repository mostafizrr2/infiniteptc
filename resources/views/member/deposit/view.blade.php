@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Transaction details</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Transaction details</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">

          <div class="col-md-12">
              <div class="card">
                  <div class="card-header">
                      <h3 class="card-title">Transaction details (Waiting for receive amounts)</h3>
                  </div>
                  <div class="card-body">
                      <div class="text-center">
                         <img src="{{ $record->qrcode_url }}" alt="{{ $record->merchant_wallet_Address }}">
                         <span>Scan this code to send</span>
                      </div>

                      <div class="row">
                          <div class="col-md-8 offset-md-2">
                              <div class="input-group mb-4 mt-4">
                                  <div class="input-group-prepend">
                                      <span class="input-group-text bg-danger">You have to send amont (BTC) to</span>
                                  </div>
                                  <input type="text" class="form-control" readonly value="{{ $record->merchant_wallet_Address }}" id="copyText">
                                  <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button" id="copyBtn" onclick="clickAndCopy()" >Copy</button>
                                  </div>
                              </div>
                              <table class="table">
                                <tbody>
                                  <tr>
                                    <td>Amount to send</td>
                                    <td>{{ $record->amount_to }} {{ $record->received_currency }}</td>
                                  </tr>
                                  <tr>
                                    <td>Payment Id</td>
                                    <td>{{ $record->trx_id }}</td>
                                  </tr>
                                  <tr>
                                    <td>You have to pay in</td>
                                    <td>{{  date("H", $record->timeout)}} hours</td>
                                  </tr>
                                </tbody>
                              </table>
                              <hr>

                              <div>
                                <h4> What to do next?</h4>

                                1) Please send {{ $record->amount_to }} {{ $record->received_currency }} to address {{ $record->merchant_wallet_Address }}. (Make sure to send enough to cover any coin transaction fees!) You will need to initiate the payment using your software or online wallet and copy/paste the address and payment amount into it. We will email you when all funds have been received. <br>

                                2) After sending payment, please wait and click the below ( Confirm ) button to update your account balance. it may take 7 - 48 hours. So try several time if your balance is'nt updated. 

                              </div>
                          </div>
                      </div>

                  </div>

                  <div class="card-footer">
                    <div class="row">
                      <div class="col-md-8 offset-md-2">
                        <form action="{{ route('deposit.complete') }}" method="post">
                          @csrf
                            <input type="hidden" name="trx_id" value="{{ $record->trx_id }}"> 
                           
                            <div>
                              Yes! i have sent <strong class="text-danger">{{ $record->amount_to }}</strong> BTC to 
                              <strong>{{ $record->merchant_wallet_Address }}</strong> <br><br>
                              <button type="submit" class="btn btn-success float-right">
                                Confirm
                              </button>
                              <a href="{{ route('member.deposit') }}" class="btn btn-dark">
                               Go Back
                              </a>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>

              </div>


          </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush
