@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Refferal earnings</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Refferal earnings</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Refferal earning history</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Earning from</th>
                            <th>Amount</th>
                            <th>Description</th>
                            <th>Date</th>
                          </tr>
                        </thead>

                        <tbody> 

                        @foreach ($earnings as $key => $earning)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $earning->member->username }}</td>
                          <td>{{ $earning->amount }}$</td>
                          <td>{{ $earning->description }}</td>
                          <td>{{ $earning->created_at->format('d M, Y') }}</td>
                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>

                      <div class="mt-2 p-2">
                        {!! $earnings->render() !!}
                      </div>
                      
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
@endpush