@extends('member')


@push('header')
    
@endpush

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Raffle Details</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Raffle Details</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title">Raffle# {{ $raffle->title }}</h1>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p class="card-text">{{ $raffle->description }}</p> <br>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <th scope="col">Position</th>
                                        <th scope="col">Prize</th>
                                        @if ($raffle->status == 'ended')
                                        <th scope="col">Winner</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($raffle->prizes as $key => $prize)
                                        <tr>
                                            <th>{{ $prize->position }}</th>
                                            <td>{{ $prize->prize_amount }}$</td>
                                            @if ($raffle->status == 'ended')
                                            <td>
                                                @if ($prize->slot != null)
                                                    {{ $prize->slot->member->username }}
                                                @endif
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>{{-- Card Body --}}
                    <div class="card-footer">
                        <div style="width: 200px" class="float-left">
                            Slots: {{ count($raffle->slots) }} | Slot price: {{ $raffle->slot_price }}$
                        </div>
                    </div>
                </div>
            </div>  

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title">Buy Slots for this raffle</h1>
                        <div class="modal fade" id="createPrize{{ $raffle->id }}" tabindex="-1" role="dialog" aria-labelledby="createPrize{{ $raffle->id }}Label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <h5 class="modal-title" id="createPrize{{ $raffle->id }}Label">Create prize for ( {{ $raffle->title }} )</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                                <form action="{{ route('raffle.prize') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="raffle_id" value="{{ $raffle->id }}">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <div class="row">
                                                <?php 
                                                
                                                $prize = DB::table('raffle_prizes')->where('raffle_id', $raffle->id)->latest()->first();
                                                if($prize)
                                                {
                                                    $position = $prize->position + 1;
                                                }
                                                else 
                                                {
                                                    $position =  1;
                                                }
                                                    
                                                ?>
                                                <div class="col-md-6">
                                                    <label for="position">Position</label>
                                                    <input type="number" class="form-control" id="position" name="position" value="{{ old('position', $position) }}" placeholder="Enter prize position">
                                                    @error ('position')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="prize_amount">Prize Amount (USD)</label>
                                                    <input type="text" class="form-control" id="prize_amount" name="prize_amount" value="{{ old('prize_amount', 10) }}" placeholder="Enter prize amount">
                                                    @error ('prize_amount')
                                                        <p class="text-danger">{{ $message }}</p>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Create Raffle</button>
                                    </div>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        @php
                            $user = Auth::guard('member')->user();
                            $mySlots = DB::table('raffle_slots')->where('raffle_id', $raffle->id)->where('member_id', $user->id)->get();
                        @endphp
                        You can buy multiple slots for this raffles. Once all slots are sold the admin will draw the raffle and random user will win the prizes. 
                    </div>
                    <div class="card-footer">
                        <div class="float-left">
                          <strong class="">Total: {{ count($raffle->slots) }}</strong> | 
                          <strong class="text-danger">Sold: {{ count($raffle->slots) - count($raffle->free_slots) }}</strong> | 
                          <strong class="text-info">Available: {{ count($raffle->free_slots) }}</strong> |
                          <strong class="text-success">Purchased: {{ count($mySlots) }}</strong>
                        </div>
                    </div>
                </div>

                <div class="row">
                    @foreach ($raffle->slots as $slot)
                    <div class="col-md-4">
                        <div class="card">
                  
                            <div class="card-header {{ ($slot->member_id != null && $slot->member_id == $user->id ) ? 'bg-success': '' }}">
                                Slot# {{ $slot->id }}
                                @if ($slot->member_id != null)

                                    @if ($slot->prize_id != null)
                                        <div class="float-right badge badge-warning">{{ $slot->prize->position }} Position</div> 
                                    @elseif ($slot->member_id == $user->id)
                                        <div class="float-right badge badge-danger">Purchased</div> 
                                    @elseif ($slot->member_id != null)
                                        <div class="float-right badge badge-danger">Sold</div> 
                                    @endif

                                @else 
                                  <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#purchaseRaffle{{ $slot->id }}">
                                     Purchase
                                  </button>

                                  <div class="modal fade" id="purchaseRaffle{{ $slot->id }}" tabindex="-1" role="dialog" aria-labelledby="purchaseRaffle{{ $slot->id }}Label" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Purchase the Slot</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <form action="{{ route('raffle.purchase', $slot->id) }}" method="post">
                                            @csrf
                                            <div class="modal-body">
                                              <strong>Raffle</strong>: {{ $raffle->title }} <br>
                                              <strong>Slot</strong># {{ $slot->id }} | <strong>Price</strong>: {{ $raffle->slot_price }}$ <hr>
                                              <h5>Are you sure to purchase the slot?</h5>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                              <button type="submit" class="btn btn-success">Purchase</button>
                                            </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                  
                                @endif
                            </div>
    
                            @if ($slot->member_id != null)
                                <div class="card-footer">

                                   <span class="float-left">{{ $slot->updated_at->format('d M, Y') }}</span>
                                   <span class="float-right">
                                       @if ($slot->member->avatar != '')
                                           <img style="width:20px; height:20px; border-radius:50%" class="img-thumbnail"
                                           src="{{ url('storage/member/'. $slot->member->avatar) }}" alt="" srcset="">
                                       @else
                                           <img style="width:20px; height:20px; border-radius:50%" class="img-thumbnail"
                                           src="{{ url('defaults/user.png') }}" alt="" srcset="">
                                       @endif
                                       {{ $slot->member->username }}
                                   </span>
                                </div>
                            @endif
                        </div>
                    </div>
                   @endforeach
                </div>
            </div>  

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush