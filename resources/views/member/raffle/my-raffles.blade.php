@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">My Raffles</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">My Raffles</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        @if (count($MyActiveRaffles))
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title">Active Raffles ({{ count($MyActiveRaffles) }})</h1>
                    </div>
                </div>
            </div>  
            @foreach ($MyActiveRaffles as $item)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                        {{ $item->title }}
                        </div>

                        <div class="card-footer">
                            <div style="width: 200px" class="float-left">
                                @php
                                    $user = Auth::guard('member')->user();
                                    $mySlots = DB::table('raffle_slots')->where('raffle_id', $item->id)->where('member_id', $user->id)->get();
                                @endphp

                               Available: {{ count($item->free_slots) }} | Purchased: {{ count($mySlots) }}
                            </div>

                            <a href="{{ route('raffle.details', $item->id) }}"  class="btn btn-success btn-sm ml-2 float-right">
                                View details
                            </a>

                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        @endif
        <!-- /.row -->
        @if (count($MyDrawableRaffles))
            <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1 class="card-title">Waiting for draw ({{ count($MyDrawableRaffles) }})</h1>
                    </div>
                </div>
            </div>  
            @foreach ($MyDrawableRaffles as $item)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                        {{ $item->title }}
                        </div>

                        <div class="card-footer">
                            <div style="width: 200px" class="float-left">
                                @php
                                    $user = Auth::guard('member')->user();
                                    $mySlots = DB::table('raffle_slots')->where('raffle_id', $item->id)->where('member_id', $user->id)->get();
                                @endphp

                               Available: {{ count($item->free_slots) }} | Purchased: {{ count($mySlots) }}
                            </div>

                            <a href="{{ route('raffle.details', $item->id) }}"  class="btn btn-success btn-sm ml-2 float-right">
                                View details
                            </a>

                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        @endif
        <!-- /.row -->

        @if (count($MyEndedRaffles))
        <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h1 class="card-title">Ended Raffles ({{ count($MyEndedRaffles) }})</h1>
                </div>
            </div>
        </div>  
        @foreach ($MyEndedRaffles as $item)

           @php
               $user = auth()->guard('member')->user();
               $slots = \App\RaffleSlot::with('prize')
               ->where('raffle_id', $item->id)
               ->where('member_id', $user->id)
               ->where('prize_id', '!=', null)
               ->get();
           @endphp
            <div class="col-md-12">
                <div class="card">
                    <div class="card-footer">
                        <div class="float-left">
                            {{ $item->title }} <br>
                            @if (count($slots) > 0)
                            You won: 
                            @foreach ($slots as $key => $slot)
                               <span class="badge badge-success">
                                   #{{ $slot->id }} (position {{ $slot->prize->position }})
                                </span>
                            @endforeach
            
                            @endif
                        </div>

                        <a href="{{ route('raffle.details', $item->id) }}"  class="btn {{ (count($slots) > 0) ? 'btn-success':'bg-info' }} btn-sm ml-2 float-right">
                            <i class="fa fa-eye"></i>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
        {{-- <div class="col-md-12">
            {!! $MyEndedRaffles->render() !!}
        </div> --}}
        </div>
    @endif
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush