@extends('member')

@section('title', 'Profile')

@section('content')

<div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">User detailes</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">User detailes</li>
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    
    
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-4">
            <div class="card card-dark" style="padding:30px">
                <!-- form start -->
                <div class="text-center">
                    @if ($user->avatar != '')
                        <img style="width:200px; height:200px; border-radius:50%" class="img-thumbnail"
                        src="{{ url('storage/member/'. $user->avatar) }}" alt="" srcset="">
                    @else
                        <img style="width:200px; height:200px; border-radius:50%" class="img-thumbnail"
                        src="{{ url('defaults/user.png') }}" alt="" srcset="">
                    @endif
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Level Info</h3>
                </div>
                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                            <tr role="row" class="odd">
                                                <td>Level</td>
                                                <td>{{ ($user->level->level == 0) ? "No level" : $user->level->level }}</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>Payouts</td>
                                                <td>{{ $user->level->payouts }}$</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>Refferal Bonus</td>
                                                <td>{{ $user->level->refferal_bonus }}$</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          
          <!-- /.col-md-6 -->
          <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">User detailes
                        @if ($user->level->level == 0)
                            <span class="badge badge-dark">No Level</span>
                        @elseif ($user->level->level == 1)
                            <span class="badge badge-success">Level - 1</span>
                        @elseif ($user->level->level == 2)
                            <span class="badge badge-success">Level - 2</span>
                        @elseif ($user->level->level == 3)
                            <span class="badge badge-success">Level - 3</span>
                        @elseif ($user->level->level == 4)
                            <span class="badge badge-success">Level - 4</span>
                        @elseif ($user->level->level == 5)
                            <span class="badge badge-success">Level - 5</span>
                        @elseif ($user->level->level == 6)
                            <span class="badge badge-success">Level - 6</span>
                        @elseif ($user->level->level == 7)
                            <span class="badge badge-success">Level - 7</span>
                        @endif
                    </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                            <tr role="row" class="odd">
                                                <td>Username</td>
                                                <td>{{ $user->username }}</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>Full Name</td>
                                                <td>{{ $user->name }}</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>Email</td>
                                                <td>{{ $user->email }}</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>Referral ID</td>
                                                <td>{{ $user->referral_id }}</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>Referral Link</td>
                                                <td>{{ getRefferalLink($user->referral_id) }}</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>Address</td>
                                                <td>{{ $user->address }}</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>State</td>
                                                <td>{{ $user->state }}</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>Zip</td>
                                                <td>{{ $user->zip }}</td>
                                            </tr>
                                            <tr role="row" class="odd">
                                                <td>Referred By</td>
                                                <td>{{ ($user->sponsor) ? $user->sponsor->name : "" }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div>
</div>

@endsection