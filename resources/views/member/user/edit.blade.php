@extends('member')

@section('title', 'Edit Profile')

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit profile</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Edit profile</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Update profile</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="{{ route('profile.update') }}" role="form" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6 offset-md-3">

                        <div class="form-group">
                          <label for="name">Name</label>
                          @error('name')
                          - <span class="text-danger">{{ $message }}</span>
                          @enderror
                          <input type="text" class="form-control" id="name" value="{{ old('name', $member->name) }}" placeholder="Enter name" name="name">
                        </div>
    
                        <div class="form-group">
                          <label for="email">Email</label>
                          @error('email')
                          - <span class="text-danger">{{ $message }}</span>
                          @enderror
                          <input type="text" class="form-control" id="email" value="{{ old('email', $member->email) }}" placeholder="Enter email" name="email">
                        </div>
    
                        <div class="form-group">
                            <label for="address">Address</label>
                            @error('address')
                                - <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form-control" id="address" value="{{ old('address', $member->address) }}" placeholder="Enter address 1" name="address">
                        </div>
    
                        <div class="form-group">
                            <label for="state">State</label>
                            @error('state')
                                - <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form-control" id="state" value="{{ old('state', $member->state) }}" placeholder="Enter your state" name="state">
                        </div>
    
                        <div class="form-group">
                            <label for="zip">Zip</label>
                            @error('zip')
                                - <span class="text-danger">{{ $message }}</span>
                            @enderror
                            <input type="text" class="form-control" id="zip" value="{{ old('zip', $member->zip) }}" placeholder="Enter zip code" name="zip">
                        </div>
    
                        <div class="form-group">
                          <label for="avatar">User avatar</label>
                          @error('avatar')
                            - <span class="text-danger">{{ $message }}</span>
                          @enderror
                          <input type="file" class="form-control" id="avatar" name="avatar">
                        </div>

                    </div>
                  </div>

                </div>
                <!-- /.card-body -->
      
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary float-right">Submit</button>
                </div>
            </form>
        </div>
      </div>
      <!-- /.col-md-12 -->
    </div>
    <!-- /.row -->
  </div>
</div>


@endsection