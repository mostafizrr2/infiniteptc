
<div class="modal fade" id="makeDefault{{ $wallet->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Change default wallet</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('wallet.default') }}" method="POST">
          @csrf
          <input type="hidden" name="id" value="{{ $wallet->id }}">
          <div class="modal-body">
             <h5>{{ $wallet->title }}</h5> 
             <p>Address: <strong>{{ $wallet->address }}</strong></p>
             <hr>
            <p>Are you sure to make it the default wallet to receive funds ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success">Yes, Make Default</button>
          </div>
        </form>
      </div>
    </div>
</div>


<div class="modal fade" id="editWallet{{ $wallet->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Update Wallet</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('wallet.update', $wallet->id) }}" method="POST">
          @csrf
          @method('PUT')
          <div class="modal-body">
            <h4>Create Wallet for BTC</h4>
            <hr>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="title">Wallet title *</label>
                  <input type="text" class="form-control" name="title" id="title" value="{{ old('title', $wallet->title) }}" placeholder="Enter wallet title">
                  @error('title')
                    <p class="text-danger">{{ $message }}</p>
                  @enderror
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="address">Wallet Address *</label>
                  <input type="text" class="form-control" name="address" id="address" value="{{ old('address', $wallet->address) }}" placeholder="Enter wallet address">
                   @error('address')
                    <p class="text-danger">{{ $message }}</p>
                   @enderror
                   <small>Note: Please provide a valid BTC wallet address.</small>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="payouts">Wallet Description (optional)</label>
                  <textarea class="form-control" name="description" id="description" placeholder="Description">{{ old('description', $wallet->description) }}</textarea>
                  @error('description')
                    <p class="text-danger">{{ $message }}</p>
                  @enderror
                </div>
              </div>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success">Update Wallet</button>
          </div>
        </form>
      </div>
    </div>
</div>


<div class="modal fade" id="deleteWallet{{ $wallet->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('wallet.destroy', $wallet->id) }}" method="POST">
          @csrf
          @method('DELETE')
          <div class="modal-body">
            <h5>Are you sure to delete this walltet?</h5>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-danger">Delete</button>
          </div>
        </form>
      </div>
    </div>
</div>