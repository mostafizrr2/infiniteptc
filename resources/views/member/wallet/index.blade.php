@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">My Wallets</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">My Wallets</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">My Wallets</h3>

                      <a href="#" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#createAdvertise">
                        Create Wallet
                      </a>

                      <div class="modal fade" id="createAdvertise" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Create wallet</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <form action="{{ route('wallet.store') }}" method="POST">
                              @csrf
                              <div class="modal-body">
          
                                <h4>Create Wallet for BTC</h4>
                                <hr>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="title">Wallet title *</label>
                                      <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}" placeholder="Enter wallet title">
                                      @error('title')
                                        <p class="text-danger">{{ $message }}</p>
                                      @enderror
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="address">Wallet Address *</label>
                                      <input type="text" class="form-control" name="address" id="address" value="{{ old('address') }}" placeholder="Enter wallet address">
                                       @error('address')
                                        <p class="text-danger">{{ $message }}</p>
                                       @enderror
                                       <small>Note: Please provide a valid BTC wallet address.</small>
                                    </div>
                                  </div>
                                </div>
          
      
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="payouts">Wallet Description (optional)</label>
                                      <textarea class="form-control" name="description" id="description" placeholder="Description">{{ old('description') }}</textarea>
                                      @error('description')
                                        <p class="text-danger">{{ $message }}</p>
                                      @enderror
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                       <div class="row">
                                         <div class="col-md-8">
                                           <label for="status">Make it the default wallet to receive funds</label>
                                         </div>
                                         <div class="col-md-4">
                                          <input type="checkbox" name="status" id="status">
                                         </div>
                                       </div>
                                    </div>
                                  </div>
                                </div>
        
          
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Create Wallet</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
          
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Title</th>
                            <th>Address</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($wallets as $key => $wallet)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $wallet->title }}</td>
                          <td>{{ $wallet->address }}</td>
                          <td>{{ $wallet->type }}</td>
                          <td>
                              @if ($wallet->status == 0)
                              <a href="" class="badge bg-info" data-toggle="modal" data-target="#makeDefault{{ $wallet->id }}">
                                Make Default
                              </a>
                              @elseif($wallet->status == 1)
                                <span class="text-success">Default Wallet</span> 
                              @endif
                          </td>
                          <td>
                              <a href="" class="badge bg-primary" data-toggle="modal" data-target="#editWallet{{ $wallet->id }}">
                                  Update
                              </a>
                              <a href="" class="badge bg-danger" data-toggle="modal" data-target="#deleteWallet{{ $wallet->id }}">
                                  Delete
                              </a>
                          </td>

                          @include('member.wallet.modals')

                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush