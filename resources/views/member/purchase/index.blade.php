@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Purchase</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            {{-- <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Starter Page</li>
            </ol> --}}
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header">
                       <h4>Purchase ad pack</h4>
                   </div>
                   <div class="card-body">
                      <div class="row">

                        @if (count($adpacks))
                          @foreach ($adpacks as $item)
                          <div class="col-lg-3 col-sm-6">
                              <div class="small-box bg-info">
                                <div class="inner">
                                  <h3>{{ $item->clicks }} <span style="font-size: 20px">clicks</span></h3>
                                  <h4>Price: {{ $item->price }}$</h4>
                                </div>
                                <div class="icon">
                                  <i class="ion ion-stats-bars"></i>
                                </div>
                                  <input type="hidden" name="adpack_id">
                                  <a href="javascript:void(0)" class="small-box-footer"  data-toggle="modal" data-target="#purchaseAdpack{{ $item->id }}">
                                      Purchase Now <i class="fas fa-arrow-circle-right"></i>
                                  </a>
                              </div>
                          </div>

                          <div class="modal fade" id="purchaseAdpack{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Purchase confirmation</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form action="{{ route('adpack.purchase') }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="adpack_id" value="{{ $item->id }}">
                                    <div class="modal-body">
                                      <h5>Are you sure to buy <strong>{{ $item->clicks }} </strong>clicks against <strong>{{ $item->price }}</strong>$ ?</h5>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                      <button type="submit" class="btn btn-success">Sure Purchase</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          @endforeach

                        @else 

                          <div class="col-md-12">
                            <h4>No ad pack available right now</h4>
                            <p>Maybe you have to upgrade to next level to buy ad packs</p>
                          </div>
                            
                        @endif


                      </div>
                   </div>
               </div>
           </div>

           <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Purchase History</h4>
                </div>
                <div class="card-body">
                  <table class="table table-sm">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Clicks</th>
                        <th scope="col">Price</th>
                        <th scope="col">Purchase Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($purchase_history as $key => $item)
                      <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <td>{{ $item->clicks }}</td>
                        <td>{{ $item->price }}$</td>
                        <td>{{  $item->created_at->format('d M, Y') }}</td>
                      </tr>
                      @endforeach
     
                    </tbody>
                  </table>

                  <div class="mt-2 p-2">
                    {!! $purchase_history->render() !!}
                  </div>

                </div>
            </div>
        </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush