<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('member.index') }}" class="brand-link text-center">
      @if ($system->main_logo)
      <img style="height: 30px; width: 120px;" src="{{ url('storage/system', $system->main_logo ) }}" alt="{{ $system->title }}">
      @else
      <img style="height: 30px; width: 120px;" src="{{ asset('assets/images/logo.png') }}" alt="{{ $system->title }}">
      @endif
    </a>
    
    <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="{{ route('member.index') }}" class="nav-link {{ setActive(['member.index'], 'bg-danger') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>Dashboard</p>
                </a>
            </li>

            <li class="nav-item">
              <a href="{{ route('member.membership') }}" class="nav-link {{ setActive(['member.membership'], 'bg-danger') }}">
                  <i class="nav-icon fa fa-handshake-o" aria-hidden="true"></i>
                  <p>Membership</p>
              </a>
            </li> 


            <li class="nav-item">
              <a href="{{ route('my.offers') }}" class="nav-link {{ setActive(['my.offers'], 'bg-danger') }}">
                <i class="nav-icon fa fa-line-chart" aria-hidden="true"></i>
                <p>My Offers</p>
              </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('member.deposit') }}" class="nav-link {{ setActive(['member.deposit'], 'bg-danger') }}">
                  <i class="nav-icon fa fa-university" aria-hidden="true"></i>
                  <p>Deposit Funds</p>
                </a>
            </li>

            <li class="nav-item has-treeview {{ setActive(['member.geneology', 'member.referred'], 'menu-open') }}">
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-users" aria-hidden="true"></i>
                <p>My Network
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">

                <li class="nav-item">
                  <a href="{{ route('member.geneology') }}" class="nav-link {{ setActive(['member.geneology'], 'bg-danger') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>My Geneology</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="{{ route('member.referred') }}" class="nav-link {{ setActive(['member.referred'], 'bg-danger') }}">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Referred members</p>
                  </a>
                </li>
                
              </ul>
          </li>


            <li class="nav-item">
                <a href="{{ route('advertise.index') }}" class="nav-link {{ setActive(['advertise.index'], 'bg-danger') }}">
                  <i class="nav-icon fa fa-bar-chart" aria-hidden="true"></i>
                  <p>Advertising</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('member.purchase') }}" class="nav-link {{ setActive(['member.purchase'], 'bg-danger') }}">
                  <i class="nav-icon fa fa-shopping-cart" aria-hidden="true"></i>
                  <p>Purchase ads</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('refferal.earnings') }}" class="nav-link {{ setActive(['refferal.earnings'], 'bg-danger') }}">
                  <i class="nav-icon fa fa-money" aria-hidden="true"></i>
                  <p>Referral Earnings</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('member.history') }}" class="nav-link {{ setActive(['member.history'], 'bg-danger') }}">
                  <i class="nav-icon fa fa-history" aria-hidden="true"></i>
                  <p>Cash History</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('member.withdraw') }}" class="nav-link {{ setActive(['member.withdraw'], 'bg-danger') }}">
                  <i class="nav-icon fa fa-usd" aria-hidden="true"></i>
                  <p>Withdraw Fund</p>
                </a>
            </li>

            <li class="nav-item">
                <a href="{{ route('transfer.funds') }}" class="nav-link {{ setActive(['transfer.funds'], 'bg-danger') }}">
                  <i class="nav-icon fa fa-rocket" aria-hidden="true"></i>
                  <p>Transfer Funds</p>
                </a>
            </li>

            <li class="nav-item">
              <a href="{{ route('wallet.index') }}" class="nav-link {{ setActive(['wallet.index'], 'bg-danger') }}">
                <i class="nav-icon fa fa-google-wallet" aria-hidden="true"></i>
                <p>My Wallets</p>
              </a>
            </li>

            {{-- <li class="nav-item">
              <a href="" class="nav-link">
                <i class="nav-icon fa fa-cog" aria-hidden="true"></i>
                <p>Settings</p>
              </a>
            </li> --}}
    
            <li class="nav-item has-treeview {{ setActive(['profile.edit', 'profile.password'], 'menu-open') }}">
                <a href="#" class="nav-link">
                  <i class="nav-icon fa fa-user"></i>
                  <p>
                    User Account
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
    
                  <li class="nav-item">
                    <a href="{{ route('profile.edit') }}" class="nav-link {{ setActive(['profile.edit'], 'bg-danger') }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Update profile</p>
                    </a>
                  </li>
    
                  <li class="nav-item">
                    <a href="{{ route('profile.password') }}" class="nav-link {{ setActive(['profile.password'], 'bg-danger') }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Change Password</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="{{ route('member.logout') }}" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Logout</p>
                    </a>
                  </li>
    
                </ul>
            </li>
            
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
</aside>
<!-- /.sidebar -->
