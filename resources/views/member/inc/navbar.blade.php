<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button">
                <i class="fas fa-bars"></i>
            </a>
        </li>
    </ul>

    <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a href="{{ route('my.offers') }}" class="btn btn-success btn-sm">
                <i class="fa fa-usd" aria-hidden="true"></i>
                My Offers
            </a>
        </li>

        <li class="nav-item dropdown">

            <button class="btn btn-warning btn-sm ml-2" href="{{ route('my.offers') }}" data-toggle="dropdown" href="#" aria-expanded="true">
                <i class="fa fa-list-ul" aria-hidden="true"></i>
                Raffles
                @if (count($activeRaffles) > 0)
                    <span class="badge badge-info">{{ count($activeRaffles) }}</span>
                @endif
            </button>

            <div class="dropdown-menu dropdown-menu-xl" style="left: inherit; right: 0px;">
              <span class="dropdown-item dropdown-header">{{ count($activeRaffles) }} Active Raffles</span>
              <div class="dropdown-divider"></div>

              @foreach ($activeRaffles as $key => $item)
                <a href="{{ route('raffle.details', $item->id) }}" class="dropdown-item">
                    {{ $key+1 }}. {{ $item->title }}
                    <span class="float-right text-muted text-sm">{{ $item->slot_price }}$</span>
                </a>
                <div class="dropdown-divider"></div>
              @endforeach
              
              <a href="{{ route('my.raffles') }}" class="dropdown-item dropdown-footer">My Raffles</a>
            </div>
          </li>

    </ul>

    <ul class="navbar-nav ml-auto">

        @livewire('chat.message-notification-livewire')

        <li class="nav-item">
            <a href="{{ route('member.faq') }}" class="nav-link">
                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                FAQ
            </a>
        </li>
    </ul>
    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
            <i class="fas fa-user"></i>
            <span class="d-none d-sm-inline-block">
                {{ ( auth()->guard('member')->user()->username ) ? auth()->guard('member')->user()->username : "testuser12" }}
            </span>
        </a>
        </li>
    </ul>
    </nav>