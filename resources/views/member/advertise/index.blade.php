@extends('member')


@push('header')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Advertisement</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Advertisement</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
           <div class="col-md-3">
            <div class="small-box bg-light text-center">
                <div class="inner">
                  <h3>{{ $user->available_adclick }}</h3>
                  <p>Clicks are available</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ route('member.purchase') }}" class="small-box-footer">
                  Buy ad pack
                </a>
              </div>
           </div>
           <div class="col-md-9">

           </div>

        </div>

       
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">My Advertisments</h3>

                      <a href="#" class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#createAdvertise">
                        Create Advertise
                      </a>

                      <div class="modal fade" id="createAdvertise" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Create Advertisement</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <form action="{{ route('advertise.store') }}" method="POST">
                              @csrf
                              <div class="modal-body">
          
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="title">title *</label>
                                      <input type="text" class="form-control" name="title" id="title" value="{{ old('title') }}" placeholder="Advertise title">
                                      @error('title')
                                        <p class="text-danger">{{ $message }}</p>
                                      @enderror
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="link">Advertise URL *</label>
                                      <input type="text" class="form-control" name="link" id="link" value="{{ old('link') }}" placeholder="Advertise link">
                                      <small>Note: Add http:// or https:// at the front of the link.</small>
                                      @error('link')
                                        <p class="text-danger">{{ $message }}</p>
                                       @enderror
                                    </div>
                                  </div>
                                </div>
          
      
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label for="payouts">Description</label>
                                      <textarea class="form-control" name="description" id="description" placeholder="Description">{{ old('description') }}</textarea>
                                      @error('description')
                                        <p class="text-danger">{{ $message }}</p>
                                      @enderror
                                    </div>
                                  </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                        <label for="required_clicks">Required Clicks * (numeric)</label>
                                        <input type="number" class="form-control" name="required_clicks" id="required_clicks" value="{{ old('required_clicks') }}" placeholder="Required clicks">
                                        @error('required_clicks')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                          <label for="countries">Exclude/Include Countries (Optional)</label>

                                          <p class="text-muted">If you want to show ad to all countries, just leave the "No Action" option checked. 
                                            Otherwise you can check "exclude" or "include" to disable or enable some specific Countries
                                             to show your ad.</p>

                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="country" id="not" value="not" checked>
                                            <label class="form-check-label" for="not">No Action</label>
                                          </div>
                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="country" id="exclude" value="exclude">
                                            <label class="form-check-label" for="exclude">Exclude</label>
                                          </div>
                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="country" id="include" value="include">
                                            <label class="form-check-label" for="include">Include</label>
                                          </div>

                                          <select name="countries[]" class="form-control" id="countries-create"  multiple="multiple" style="width: 100%">
                                            <option seleceted disabled>Select countries</option>
                                            @foreach ($countries as $item)
                                              <option value="{{ $item }}" >{{ $item }}</option>
                                            @endforeach
                                          </select>
                                          @error('countries')
                                              <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                        </div>
                                    </div>
                                </div>
        
          
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success">Create Ad</button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
          
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Title</th>
                            <th>Link</th>
                            <th>require clicks</th>
                            <th>Total Clicked</th>
                            <th>Advertisment</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($advertises as $key => $ad)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $ad->title }}</td>
                          <td>{{ $ad->link }}</td>
                          <td>{{ $ad->required_clicks }}</td>
                          <td>{{ $ad->total_clicked }}</td>
                          <td>{{ ($ad->is_running == 0) ? "Paused" : "Running" }}</td>
                          <td>
                              @if ($ad->status == 0)
                                <span class="text-info">Pending</span> 
                              @elseif($ad->status == 1)
                                <span class="text-success">Approved</span> 
                              @elseif($ad->status == 2)
                                <span class="text-default">Ended</span>
                              @elseif($ad->status == 3)
                                <span class="text-danger">Declined</span>
                              @endif
                          </td>
                          <td>
                              <a href="" class="badge bg-primary" data-toggle="modal" data-target="#editAdpack{{ $ad->id }}">
                                  Update
                              </a>
                              <a href="" class="badge bg-danger" data-toggle="modal" data-target="#deletAdpack{{ $ad->id }}">
                                  Delete
                              </a>
                          </td>

                          <div class="modal fade" id="editAdpack{{ $ad->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Create Ad pack</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="{{ route('advertise.update', $ad->id) }}" method="POST">
                                  @csrf
                                  @method('PUT')

                                  <div class="modal-body">              
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label for="title">title *</label>
                                          <input type="text" class="form-control" name="title" id="title" value="{{ old('title', $ad->title) }}" placeholder="Advertise title">
                                          @error('title')
                                            <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                        </div>
                                      </div>
                                    </div>
    
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label for="link">Advertise URL *</label>
                                          <input type="text" class="form-control" name="link" id="link" value="{{ old('link', $ad->link) }}" placeholder="Advertise link">
                                          <small>Note: Add http:// or https:// at the front of the link.</small>
                                          @error('link')
                                            <p class="text-danger">{{ $message }}</p>
                                           @enderror
                                        </div>
                                      </div>
                                    </div>
              
          
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label for="payouts">Description</label>
                                          <textarea class="form-control" name="description" id="description" placeholder="Description">{{ old('description', $ad->description) }}</textarea>
                                          @error('description')
                                            <p class="text-danger">{{ $message }}</p>
                                          @enderror
                                        </div>
                                      </div>
                                    </div>
    

                                    <div class="row">
                                      <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="countries">Exclude Countries (Optional)</label>
                                            <p class="text-muted">If you want to show ad to all countries, just leave the "No Action" option checked. 
                                              Otherwise you can check "exclude" or "include" to disable or enable some specific Countries
                                               to show your ad.</p>
                                     
                                            <div class="form-check form-check-inline">
                                              <input class="form-check-input" type="radio" name="country" id="not{{ $ad->id }}" value="not" 
                                              @if ($ad->country_should == null)
                                                  checked
                                              @endif
                                              >
                                              <label class="form-check-label" for="not{{ $ad->id }}">No Action</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                              <input class="form-check-input" type="radio" name="country" id="exclude{{ $ad->id }}" value="exclude"
                                              @if ($ad->country_should == 'exclude')
                                                  checked
                                              @endif
                                              >
                                              <label class="form-check-label" for="exclude{{ $ad->id }}">Exclude</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                              <input class="form-check-input" type="radio" name="country" id="include{{ $ad->id }}" value="include"
                                              @if ($ad->country_should == 'include')
                                                  checked
                                              @endif
                                              >
                                              <label class="form-check-label" for="include{{ $ad->id }}">Include</label>
                                            </div>
                                            
                                            <select name="countries[]" class="form-control" id="countries-edit{{ $ad->id }}"  multiple="multiple" style="width: 100%">
                                              <?php
                                                $itemCountry = DB::table('advertise_countries')->where('advertise_id', $ad->id)->get();

                                                $slctCtr = [];
                                                foreach ($itemCountry as $country) {
                                                  $slctCtr[] = $country->country;
                                                }
                                                
                                              ?>
                                                @foreach ($countries as $item)
                                                    {{-- @foreach ($itemCountry as $ctr)
                                                      @if ($ctr->country ==  $item)
                                                        <option selected value="{{ $item }}" >{{ $item }}</option>
                                                      @else  --}}
                                                      
                                                        <option 
                                                         
                                                        @if (in_array($item, $slctCtr))
                                                            selected
                                                        @endif
                                                        
                                                        value="{{ $item }}" >{{ $item }}</option>
                                                      {{-- @endif
                                                    @endforeach --}}
                                                @endforeach
                                            </select>
                                            @error('countries')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                          </div>
                                      </div>
                                    </div>
    
                                    {{-- <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                            <label for="required_clicks">Required Clicks * (numeric)</label>
                                            <input type="number" class="form-control" name="required_clicks" id="required_clicks" value="{{ old('required_clicks', $ad->required_clicks) }}" placeholder="Required clicks">
                                            @error('required_clicks')
                                                <p class="text-danger">{{ $message }}</p>
                                            @enderror
                                            </div>
                                        </div>
                                    </div> --}}
            
              
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-success">Create Ad</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          <div class="modal fade" id="deletAdpack{{ $ad->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Delete!</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="{{ route('advertise.destroy', $ad->id) }}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <div class="modal-body">
                                    <h5>Are you sure to delete this Ads?</h5>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>

                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
  $(document).ready(function() {
      $('#countries-create').select2({
        placeholder: 'Search or select multiple cuntries to exclude or include'
      });
  });


</script>

@foreach ($advertises as $key => $ad)
  <script>
    $(document).ready(function() {
        $('#countries-edit{{ $ad->id }}').select2({
          placeholder: 'Search or select multiple cuntries to exclude or include'
        });
    });
  </script>
@endforeach
@endpush