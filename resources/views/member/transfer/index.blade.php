@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Funds Transfer</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Funds Transfer</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
      

      @if ($user->membership->type == 'free')
          <div class="row">
              <div class="col-md-12">
                  <div class="card">
                      <div class="card-header">
                          <h4>Your current menbership plan - <strong>{{ $user->membership->title }}</strong></h4>
                          <p class='text-danger'>You have to upgrade your membership plan to paid to be able to transfer your account balance.</p>
                          <a href="{{ route('member.membership') }}" class="btn btn-success btn-sm">Upgrade membership</a>
                      </div>
                  </div>
              </div>
          </div>
      @else 
        <div class="row">
          <div class="col-md-3">
            <div class="small-box bg-light text-center">
                <div class="inner">
                  <p>Available for Transefer</p>
                  <h3>{{ $user->balance }}$</h3>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>

                <a href="#" class="small-box-footer" 
                {!! ( $user->balance >= 7.5) ? 'data-toggle="modal" data-target="#tranferModal"' : '' !!}>
                {{ ( $user->balance < 7.5) ? 'Cannot transfer right now' : 'Transfer now' }} 
                </a>


                <div class="modal fade" id="tranferModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Transfer Funds</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <form action="{{ route('post.transfer') }}" method="post" class="disabled">
                        @csrf
                          <div class="modal-body text-left">
                            You have <strong>{{ $user->balance }}$</strong> available Balance .
                              You cannot transfer less then <strong>7.5$</strong>.
                              <hr>

                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-12">
                                    <label for="amount">Amount (*)</label>
                                    <div class="input-group mb-3">
                                      <input type="text" class="form-control" onkeyup="checkAuth()" name="amount" id="amount" value="7.5" placeholder="Enter amount">
                                      <div class="input-group-append input-group-append">
                                        <span class="input-group-text" id="basic-addon2">$</span>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-12 text-center">
                                    @error('amount')
                                      <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                    <p class="text-danger" id="message"></p>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-12">
                                    <label for="username">Send To (*)</label>
                                    <div class="input-group mb-3">
                                      <input type="text" class="form-control" name="username" id="username" placeholder="Enter Username">
                                      @error('username')
                                      <p class="text-danger">{{ $message }}</p>
                                      @enderror
                                  </div>
                                  </div>
                                </div>

                                <div class="row">
                                  <div class="col-md-12">
                                    <label for="note">Note (Optional)</label>
                                    <div class="input-group mb-3">
                                      <textarea type="text" class="form-control" name="note" id="note" placeholder="Enter a short note.."></textarea>
                                      @error('note')
                                      <p class="text-danger">{{ $message }}</p>
                                      @enderror
                                  </div>
                                  </div>
                                </div>

                              </div>

                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" id="btn-transfer" class="btn btn-danger">Tranfer Now</button>
                          </div>
                      </form>
                    </div>
                  </div>
                </div>


              </div>
          </div>
        </div>
      @endif

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">My transfer histories</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Receiver</th>
                            <th>Amount</th>
                            <th>Note</th>
                            <th>Date</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($transfers as $key => $trns)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>
                              {{ $trns->receiver->username }}
                          </td>
                          <td>
                              {{ $trns->amount }}$
                          </td>
                          <td>
                              {{ $trns->note }}
                          </td>
                          <td>{{ $trns->created_at->format('d M, Y') }}</td>
                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>

                      <div class="mt-2 p-2">
                        {!! $transfers->render() !!}
                      </div>

                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    <script>

      function checkAuth()
      {
        var val = $('#amount').val();
        var balance = '{{ $user->balance }}';
        var min_tranfer = 7.5;

        if(val < min_tranfer )
        {
           console.log("You cannot transfer less than "+ min_tranfer +"$");
           $('#message').html("You cannot transfer less than "+ min_tranfer +"$");
           $('#btn-transfer').addClass('d-none');
        }
        else if( val > parseFloat(balance) )
        {
           console.log("You cannot exceet your available balance "+ balance +"$");
           $('#message').html("You cannot exceed your available balance "+ balance +"$");
           $('#btn-transfer').addClass('d-none');
        }
        else 
        {
          $('#message').html("");
          $('#btn-transfer').removeClass('d-none');
        }

      }
    </script>
@endpush