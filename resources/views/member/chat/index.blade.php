@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Chat Rooms</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">Chat Rooms</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-info btn-sm float-right mb-2"  data-toggle="modal" data-target="#roomCreate">
                    Create new room
                </button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">

                @livewire('chat.myrooms')
                
            </div>
            <div class="col-md-6">
                @livewire('chat.otherrooms')
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>


<div class="modal" id="roomCreate">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Create new room</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @livewire('chat.room-form')
        </div>
      </div>
    </div>
  </div>


<!-- /.content -->
@endsection

@push('footer')
    
@endpush
