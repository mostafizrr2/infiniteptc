@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">withdraw Funds</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">withdraw Funds</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">


      @if ($user->membership->type == 'free')
          <div class="row">
              <div class="col-md-12">
                  <div class="card">
                      <div class="card-header">
                          <h4>Your current menbership plan - <strong>{{ $user->membership->title }}</strong></h4>
                          <p class='text-danger'>You have to upgrade your membership plan to paid to be able to withdraw your eanings.</p>
                          <a href="{{ route('member.membership') }}" class="btn btn-success btn-sm">Upgrade membership</a>
                      </div>
                  </div>
              </div>
          </div>
      @else 

          <div class="row">
            <div class="col-md-3">
              <div class="small-box bg-light text-center">
                  <div class="inner">
                    <p>Available for withdraw</p>
                    <h3>{{ $user->available_withdrawal }}$</h3>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>

                  <a href="#" class="small-box-footer" 
                  {!! ( $user->available_withdrawal >= 10) ? 'data-toggle="modal" data-target="#widrawModal"' : '' !!}>
                  {{ ( $user->available_withdrawal < 10) ? 'Cannot withdraw right now' : 'withdraw now' }} 
                  </a>


                  <div class="modal fade" id="widrawModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-md" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Withdraw Fund</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action="{{ route('action.withdraw') }}" method="post" class="disabled">
                          @csrf
                            <div class="modal-body text-left">
                              You have available Balance <strong>{{ $user->available_withdrawal }}$</strong> to withdraw.
                                You cannot withdraw less then <strong>10$</strong>.
                                Once you Request to withdraw a fund it will waiting for admin clearance.
                                <hr>

                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-2">
                                    </div>
                                    <div class="col-md-2">
                                      <label for="amount">Amount</label>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="input-group mb-3">
                                        <input type="text" class="form-control form-control" onkeyup="checkAuth()" name="amount" id="amount" value="{{ $user->available_withdrawal }}" placeholder="Enter amount">
                                        <div class="input-group-append input-group-append">
                                          <span class="input-group-text" id="basic-addon2">$</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12 text-center">
                                      @error('amount')
                                        <p class="text-danger">{{ $message }}</p>
                                      @enderror
                                      <p class="text-danger" id="message"></p>
                                    </div>
                                  </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                              <button type="submit" id="btn-withdraw" class="btn btn-danger">Withdraw</button>
                            </div>
                        </form>
                      </div>
                    </div>
                  </div>


                </div>
            </div>
          </div>

      @endif

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">My withdrawn histories</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Wallet</th>
                            <th>Status</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($withdraw as $key => $wdr)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $wdr->created_at->format('d M, Y') }}</td>
                          <td>
                            <?php
                            if ($wdr->status == 0)
                            {
                              $cls = 'text-dark';
                            }
                            elseif($wdr->status == 1)
                            {
                              $cls = 'text-success';
                            }
                            elseif($wdr->status == 2)
                            {
                              $cls = 'text-danger';
                            }
                            
                            ?>
                            <span class="{{ $cls }}">
                                {{ $wdr->request_amount }}$
                            </span>
                          </td>
                          <td>{{ ($wdr->wallet) ? $wdr->wallet->title : "_____" }}</td>
                          <td>
                            <span class="{{ $cls }}">
                              @if ($wdr->status == 0)
                                Pending for clearance
                              @elseif($wdr->status == 1)
                                Success
                              @elseif($wdr->status == 2)
                                Declined
                              @endif
                            </span>
                          </td>
                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>

                      <div class="mt-2 p-2">
                        {!! $withdraw->render() !!}
                      </div>

                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    <script>

      function checkAuth()
      {
        var val = $('#amount').val();
        var available_withdrawal = '{{ (int) $user->available_withdrawal }}';
        var min_withdrawal = 10;

        if(val < min_withdrawal )
        {
           console.log("You cannot withdraw less than "+ min_withdrawal +"$");
           $('#message').html("You cannot withdraw less than "+ min_withdrawal +"$");
           $('#btn-withdraw').addClass('d-none');
        }
        else if( val > parseInt(available_withdrawal) )
        {
           console.log("You cannot exceet your available balance "+ available_withdrawal +"$");
           $('#message').html("You cannot exceed your available balance "+ available_withdrawal +"$");
           $('#btn-withdraw').addClass('d-none');
        }
        else 
        {
          $('#message').html("");
          $('#btn-withdraw').removeClass('d-none');
        }

      }
    </script>
@endpush