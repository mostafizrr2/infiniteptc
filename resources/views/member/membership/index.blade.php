@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Membership</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Membership</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Your current membership plan is - <strong>{{ $user->membership->title }}</strong></h4>
                        {!! ($user->membership->type == "free") ? "<p class='text-danger'>You have to upgrade to paid membership to get availability of additional facilities.</p>" : "" !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
           <div class="col-md-12">
                @if ($user->membership->type == 'free')
                    <div class="card">
                        <div class="card-header">
                            <h4>Purchase membership plan</h4>
                        </div>
                        <div class="card-body">
                        <div class="row">
                            @if (count($memberships))
                            @foreach ($memberships as $item)
                            <div class="col-lg-4">
                                <div class="small-box bg-light">
                                    <div class="inner">
                                        <div class="card-header">
                                            <h4 class="card-title">{{ $item->title }} </h4>
                                            <span class="badge badge-info float-right" style="font-size:16px">Price - {{ $item->price }}$</span>
                                        </div>
                                        <div class="card-body">
                                            <span>You can view: <strong>{{ $item->can_click }} ads / day</strong></span> <br>
                                            <span>You can earn: <strong>{{ $item->cpc }}$ / ads</strong></span> <br>
                                        </div>
                                    </div>
                               
                                    <a href="javascript:void(0)" class="small-box-footer"  data-toggle="modal" data-target="#purchasMembership{{ $item->id }}">
                                        Purchase now <i class="fas fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="modal fade" id="purchasMembership{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Purchase confirmation</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{ route('membership.purchase') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="membership_id" value="{{ $item->id }}">
                                        <div class="modal-body">
                                        <h5>Are you sure to buy the membership plan against <strong>{{ $item->price }}</strong>$ ?</h5>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-success">Sure Purchase</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                                </div>
                            @endforeach

                            @else 

                            <div class="col-md-12">
                                <h4>No membership plan available right now</h4>
                                {{-- <p></p> --}}
                            </div>
                                
                            @endif


                        </div>
                        </div>
                    </div>
                @endif    
           </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush