@extends('member')

@push('header')

@endpush

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            {{-- <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Starter Page</li>
            </ol> --}}
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-7">

                <div class="input-group mb-4 mt-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text bg-danger">Referral Link</span>
                    </div>
                    <input type="text" class="form-control" readonly value="{{ getRefferalLink($user->referral_id) }}" id="copyText">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="button" id="copyBtn" onclick="clickAndCopy()" >Copy</button>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1">
                    <i class="fa fa-usd"></i>
                </span>

                <div class="info-box-content">
                  <span class="info-box-text">Referral Earning</span>
                  <span class="info-box-number">
                    {{ $user->refferal_earning }}
                    <small>$</small>
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
              <div class="info-box mb-3">
                <span class="info-box-icon bg-warning elevation-1">
                    <i class="fa fa-usd"></i>
                </span>

                <div class="info-box-content">
                  <span class="info-box-text">Today Earning</span>
                  <span class="info-box-number">
                    {{ $todaysEarn }}
                    <small>$</small>
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-info elevation-1">
                    <i class="fa fa-usd"></i>
                  </span>
                  <div class="info-box-content">
                    <span class="info-box-text">Total Earning</span>
                    <span class="info-box-number">
                      {{ $user->total_earning }}
                      <small>$</small>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
              <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-danger elevation-1">
                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                  </span>

                  <div class="info-box-content">
                    <span class="info-box-text">Withdrawable</span>
                    <span class="info-box-number">
                      {{ $user->available_withdrawal }}
                      <small>$</small>
                    </span>
                </div>
                <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->

        </div>

        @if ($user->balance < 10)
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Your current account balance - <strong>{{ $user->balance }}</strong>$</h4>
                            <p class='text-danger'>Your account balance is looking low. Please deposit some funds to your account balance.</p>
                            <a href="{{ route('member.deposit') }}" class="btn btn-success btn-sm">Deposit now</a>
                        </div>
                    </div>
                </div>
            </div>
        @else

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Your current account balance - <strong>{{ $user->balance }}</strong>$</h4>
                        <p class='text-success'>You can transfer funds to other member from your account balance.</p>
                        <a href="{{ route('transfer.funds') }}" class="btn btn-success btn-sm">Transfer Funds</a>
                    </div>
                </div>
            </div>
        </div>

        @endif

        <div class="row">
        <div class="col-lg-6">
            <div class="card">
              <div class="card-header">
                <h2 class="stats">Account Statistics</h2>
              </div>
              <div class="card-body">
                  <div class="mid">
                    <table class="table table-striped">
                        <tbody>

                            <tr>
                                <td>Account Level:</td>
                                <td>{{ ($user->level->level == 0) ? "No level" : $user->level->level }}</td>
                            </tr>
                            <tr>
                                <td>Account Balance:</td>
                                <td>{{ $user->balance }}$</td>
                            </tr>
                            <tr>
                                <td>Available withdrawal:</td>
                                <td>{{ $user->available_withdrawal }}$
                                    @if ($user->available_withdrawal >= 10)

                                     <a style="font-size: 14px" class="text-success" href="{{ route('member.withdraw') }}">Withdraw Funds</a>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>Payouts:</td>
                                <td>{{ $user->level->payouts }}$</td>
                            </tr>
                            <tr>
                                <td>Refferal Bonus:</td>
                                <td>{{ $user->level->refferal_bonus }}$</td>
                            </tr>
                            <tr>
                                <td>User Country:</td>
                                <td><span>{{ $user->country }}</span></td>
                            </tr>

                        </tbody>
                    </table>

                </div>
              </div>
            </div>
        </div>
        <!-- /.col-md-6 -->
        <div class="col-lg-6">

            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h5 class="m-0">Account balance are {{ $user->balance }}$</h5>
                </div>
                <div class="card-body">
                    <h6 class="card-title">Deposit balance</h6>
                    <p class="card-text">If you need to purchase ad pack plans and your balance is
                        low then you can deposit to your account balance.</p>
                    <a href="{{ route('member.deposit') }}" class="btn btn-primary">Deposit balance</a>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h5 class="m-0">Ads pack plan</h5>
                </div>
                <div class="card-body">
                    <h6 class="card-title">Buy an ads pack </h6>

                    <p class="card-text">Once you will buy an ads pack first time then your account will updgade to paid user.</p>
                    <a href="{{ route('member.purchase') }}" class="btn btn-success">Buy ads pack now</a>
                </div>
            </div>
        </div>
        <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Cash Histories</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0 table-responsive">
                      <table class="table table-sm">

                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Amount</th>
                            <th>Date</th>
                          </tr>
                        </thead>

                        <tbody>

                        @foreach ($histories as $key => $history)
                        <tr>
                          <td>{{ $key + 1 }}</td>
                          <td>{{ $history->title }}</td>
                          <td>{{ $history->description }}</td>
                          <td>
                            <span class="{{ ($history->type == 'in') ? 'text-success' : 'text-danger' }}">
                                {{ $history->amount }}$
                            </span>
                          </td>
                          <td>{{ $history->created_at->format('d M, Y') }}</td>
                        </tr>            
                        @endforeach
  
                        </tbody>
                      </table>

                      {{-- <div class="mt-2 p-2">
                        {!! $histories->render() !!}
                      </div> --}}


                    </div>
                    <!-- /.card-body -->
                  </div>
            </div>

        </div>


    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

@endsection

@push('footer')

@endpush
