@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">My Offers</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">My Offers</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<?php 
    $avalableAdsCount = $user->membership->can_click - $todaysClicksCount;
    if($avalableAdsCount < 0)
    {
    $avalableAdsCount = 0;
    }    
?>

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5><strong>{{ $avalableAdsCount }} offers left</strong>. Click an ad and watch minimum 25 Seconds to get paid.</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- /.col-md-6 -->
            <div class="col-lg-8">
           
                @if ($avalableAdsCount > 0)
                    @foreach ($advertises as $item)
                        <div class="card">
                            <div class="card-header">
                                <h5 class="m-0 float-left">{{ $item->title }}</h5>
                            </div>
                            <div class="card-body">
                                <p class="card-text">{{ $item->description }}</p>
                                <a href="{{ route('offer.view',$item->id) }}" class="btn btn-danger float-right">Earn $0.02</a>
                            </div>
                        </div>
                    @endforeach
                @else 
                <div class="card">
                    <div class="card-header bg-warning">
                        <h5>No offers available right now.</h5>
                    </div>
                </div> 
                @endif

            </div>
            <!-- /.col-md-8 -->

            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                      <h4 class="stats">Today's Ads and clicks</h4>
                    </div>
                    <div class="card-body">
                        <div class="mid">
                          <table class="table">
                              <tbody>
                                <tr>
                                    <td>Account Status:</td>
                                    <td> <span>{{ $user->membership->title }}</span></td>
                                </tr>

                                <tr>
                                    <td>Available Ads:</td>
                                    <td> <span>{{ $avalableAdsCount }}</span></td>
                                </tr>

                                <tr>
                                    <td>Todays Clicks:</td>
                                    <td> <span>{{ $todaysClicksCount }}</span></td>
                                </tr>

                                <tr>
                                    <td>Todays Earning:</td>
                                    <td> <span>${{ $todaysClicksEarn }}</span></td>
                                </tr>
                                
                              </tbody>
                          </table>
                      </div>
                    </div>
                  </div>
            </div>


        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<!-- /.content -->
@endsection

@push('footer')
    
@endpush