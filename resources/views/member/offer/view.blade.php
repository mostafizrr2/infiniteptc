<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ env('APP_NAME') }} - {{ $advertise->title }}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <style>
        *{
            margin: 0;
            padding: 0;
            overflow: hidden !important;
        }
        html, body, .main{
            height: 100%;
        }
        iframe{
            
        }
    </style>
</head>
<body>
    <nav class="navbar navbar-expand navbar-dark bg-dark">

        <a class="navbar-brand">{{ $advertise->title }}</a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">

            <li class="nav-item">
              <span class="nav-link mr-2" id="countdown"></span>
            </li>

            <li class="nav-item d-none" id="back-btn">
                <a class="btn btn-danger btn-sm mt-1" href="{{ route('my.offers') }}">
                  <i class="fa fa-chevron-left"></i>  
                  Go back
                </a>
            </li>

          </ul>
        </div>
      </nav>

    <div class="main">
        <iframe src="{{ $advertise->link }}" frameborder="0" width="100%" height="100%"></iframe>
    </div>
    
    <script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script>

        document.getElementById("countdown").innerHTML = "Please wait...";

        $(document).ready(function(){
            var timeleft = 25;
            document.getElementById("countdown").innerHTML = "Please wait <strong>"+timeleft+"</strong> seconds";
            var url = "{{ route('ad.click', $advertise->id ) }}";
            var token = "{{ csrf_token() }}";
            var data = { _token: token };
            
            var countdown = setInterval(function(){
                if(timeleft < 0){
                    clearInterval(countdown);

                    $('#back-btn').removeClass('d-none');
                    document.getElementById("countdown").innerHTML = "Congrates. You earned {{ $user->membership->cpc }}$ just now";

                    $.post( url, data ).done(function( data ) {

                        console.log( data );

                    }).fail(function( error ){

                        console.log( error );

                    });

                    
                } else {
                    document.getElementById("countdown").innerHTML = "Please wait <strong>"+timeleft+"</strong> seconds";
                }

                timeleft -= 1;

            }, 1000);
        });
    </script>
</body>
</html>