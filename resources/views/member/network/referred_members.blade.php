@extends('member')


@push('header')
<style>
    .parent {
      margin-bottom: 5px;
    }
    li.parent-li{
        list-style: none;
    }
    .parent > img {
        height: 20px;
        width: 20px;
        border-radius: 50%;
        margin-right: 3px;
    }
    .parent-li{
        /* list-style: none; */
        margin: 0px;
        padding: 0px;
    }


</style>
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Network</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Network</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">

           <div class="col-md-12">
       
           </div>

           <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">All members referred by me</h3>
                    </div>
                    <div class="card-body">
                        <div>
                            <ol>
                               <li class="parent-li">
                                  <div class="parent">
                                     <img src="{{ ($user->avatar) ? url('storage/member/'. $user->avatar) : url('defaults/user.png') }}">
                                     {{ $user->username }} (You)
                                  </div>
                                  <ol>
                                      @foreach ($referred_users as $item)
                                      <li class="child-li">
                                         <div class="parent">
                                             <img src="{{ ($item->avatar) ? url('storage/member/'. $avatar) : url('defaults/user.png') }}">
                                             {{ $item->username }} ( {{ $item->membership->title }} )
                                         </div>
                                         {{-- <ol class="childs"></ol> --}}
                                      </li>
                                      @endforeach
                                  </ol>
                               </li>
                            </ol>
                         </div>
                    </div>
                </div>

           </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush