@extends('member')


@push('header')
<style>
    .parent {
      margin-bottom: 2px;
      /* custom style */
      border-top:1px solid #9B9B9B; 
      border-bottom:1px solid #9B9B9B; 
      border-right:1px solid #9B9B9B; 
      border-radius: 1px 1px 1px 1px;

      background: #F4F6F9; 
      width: 220px;
      overflow: hidden;
      padding-left:15px !important;
      padding:5px;
    }

    .genelogy-view li { 
        /* list-style: none; */
        border-left:1px solid #9B9B9B; 
    }

    .parent > img {
        height: 20px;
        width: 20px;
        border-radius: 50%;
        margin-right: 3px;
    }

    .childs{
       margin-left: 50px;
    }


ul, #myUL {
  list-style-type: none;
}

#myUL {
  margin: 0;
  padding: 0;
}

.caret {
  cursor: pointer;
  -webkit-user-select: none; /* Safari 3.1+ */
  -moz-user-select: none; /* Firefox 2+ */
  -ms-user-select: none; /* IE 10+ */
  user-select: none;
}

.caret::before {
  content: "\2B9B";
  color: black;
  display: inline-block;
  margin-right: 6px;
}

.caret-down::before {
  -ms-transform: rotate(180deg); /* IE 9 */
  -webkit-transform: rotate(180deg); /* Safari */'
  transform: rotate(180deg);  
}

.nested {
  display: block;

}

.active {
  display: none;
}


</style>
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Network</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                <li class="breadcrumb-item active">Network</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">

           <div class="col-md-12">
       
           </div>

           <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">My geneology</h3>
                    </div>
                    <div class="card-body">
                        <div class="genelogy-view">
                            <ol id="myUL">
                                <li class="parent-li">
        
                                    <div class="parent caret">
                                        <img src="{{ $user->avatar ? url('storage/member/'. $user->avatar ) : url('defaults/user.png') }}">
                                        {{ $user->username }} ( me )
                                    </div>
        
                                    <ol class="childs nested">
                                        {!! $tree !!}
                                    </ol>
                                </li>
                            </ul>
                        </div>  
                    </div>
                </div>

           </div>

        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
<script src="{{ asset('js/tree.js') }}"></script>
@endpush