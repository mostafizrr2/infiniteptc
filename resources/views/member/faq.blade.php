@extends('member')


@push('header')
    
@endpush

@section('content')
<!-- Content-->
<!-- Content-->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">FAQ</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <div id="accordion">

                @foreach ($faqs as $key => $item)
                    <div class="card">
                        <div class="card-header" id="heading{{ $key }}">
                            <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="true" aria-controls="collapse{{ $key }}">
                                #{{ $key + 1 }}  {{ $item->question }} 
                            </button>
                            </h5>
                        </div>
                    
                        <div id="collapse{{ $key }}" class="collapse {{ (($key + 1) == 1) ? 'show' : '' }}" aria-labelledby="heading{{ $key }}" data-parent="#accordion">
                            <div class="card-body">
                                {{ $item->answer }} 

                                @if ( $item->video_link != "")
                                    <hr>
                                    <a target="_blank" class="float-right" href="{{ $item->video_link }}">
                                        <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                        Watch Video
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                @endforeach
                

         

              </div>
          </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection

@push('footer')
    
@endpush
