@component('mail::message')
# Welcome to {{ config('app.name') }}
Hello {{ $name }}, <br>
Your account is created and activated by an admin. Now you can login to your account directly<br><br>

Login credentials: <br>
Email: {{ $email }} <br>
Password: {{ $password }} <br>

<a href="{{ route('user.login') }}" class="btn btn-primary">
    Login Now
</a> <br><br>

<strong>Note: </strong> don't forgot to change your current password after login. <br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
