@component('mail::message')
# Account Created
Hello {{ $name }}, <br>
Your account is created successfully. Now you have to verify your account. just click the link given below<br>
Link: 
<a href="{{ route('user.verify', ['token' => $token, 'email' => $email]) }}">
    {{ route('user.verify', ['token' => $token, 'email' => $email]) }}
</a> <br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
