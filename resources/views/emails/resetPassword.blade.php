@component('mail::message')
# YOUR PASSWORD RESET LINK
Hello {{ $name }}, <br>
Maybe you have requested to reset your account password. if you don't want to reset just ignore that email and try to login with your old credentials. Otherwise just click the below link to get the password reset page.<br>
Link: 
<a href="{{ route('user.pass.reset', ['token' => $token, 'email' => $email]) }}">
    {{ route('user.pass.reset', ['token' => $token, 'email' => $email]) }}
</a> <br>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
