<!DOCTYPE html>
<html lang="en">

<!-- The whole application is developed by Mostafiz Rahaman From Bangladesh -->
<!-- Available on Fiverr - https://www.fiverr.com/d.mostafiz -->

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta name="forntEnd-Developer" content="Mamunur Rashid">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('title') - {{ env('APP_NAME') }}</title>
	<!-- favicon -->
	<link rel="shortcut icon" href="{{ asset('assets/images/favicon.html') }}" type="image/x-icon">
	<!-- bootstrap -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<!-- Plugin css -->
	<link rel="stylesheet" href="{{ asset('assets/css/plugin.css') }}">

	<!-- stylesheet -->
	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
	<!-- responsive -->
	<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
	<style>
        .page-top {
            padding: 136px 0px 0px;
            background: #070b28;
            position: relative;
        }

		.page {
			padding: 61px 0px 147px !important;
		}

		.contact .contact-form-wrapper {
			padding-right: 0px !important;
		}

		.navbar-brand > img {
			height: 56px;
			width: 184px;
		}
    </style>
	@stack('header')
</head>

<body>
	<!-- preloader area start -->
	<div class="preloader" id="preloader">
		<div class="loader loader-1">
			<div class="loader-outter"></div>
			<div class="loader-inner"></div>
		</div>
	</div>
	<!-- preloader area end -->

	<!-- Header Area Start  -->
	{{-- <header class="header nav-fixed"> --}}
		<!-- Top Header Area Start -->
		{{-- @include('public.inc.topbar') --}}
		<!-- Top Header Area End -->
		<!--Main-Menu Area Start-->
		{{-- @include('public.inc.menubar') --}}
		<!--Main-Menu Area Start-->
	{{-- </header> --}}
	<!-- Header Area End  -->
	
	@yield('content')
	<!-- Footer Area Start -->
	{{-- @include('public.inc.footer') --}}
	<!-- Footer Area End -->

	<!-- Back to Top Start -->
	<div class="bottomtotop">
		<i class="fas fa-chevron-right"></i>
	</div>
	<!-- Back to Top End -->
	

	<!-- jquery -->
	<script src="{{ asset('assets/js/jquery.js') }}"></script>
	<!-- popper -->
	<script src="{{ asset('assets/js/popper.min.js') }}"></script>
	<!-- bootstrap -->
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
	<!-- plugin js-->
	<script src="{{ asset('assets/js/plugin.js') }}"></script>

	<!-- MpusemoverParallax JS-->
	<script src="{{ asset('assets/js/TweenMax.js') }}"></script>
	<script src="{{ asset('assets/js/mousemoveparallax.js') }}"></script>
	<!-- main -->
	<script src="{{ asset('assets/js/main.js') }}"></script>
	
	@stack('footer')

</body>


<!-- Mirrored from pixner.net/dooplo/dooplo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 10 Apr 2020 05:52:13 GMT -->
</html>