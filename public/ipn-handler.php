<?php 

use App\System;
use App\Deposit;
use App\IpnMessage;
use App\Transaction;
use App\AdminEarning;
use App\Helpers\CoinPayments;

require __DIR__.'/../vendor/autoload.php';
$app = require_once __DIR__.'/../bootstrap/app.php';
$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$kernel->handle(
$request = Illuminate\Http\Request::capture()
);

if(isset($_POST['txn_id']))
{
    $txn_id = $_POST['txn_id'];
    
    $ipnWorking = new IpnMessage();
    $ipnWorking->message = "IPN Started working for TRX: ".$txn_id;
    $ipnWorking->status = "working";
    $ipnWorking->save();
    
    
    $app = System::first();
    $cps = new CoinPayments();
    
    $cps->Setup($app->cp_private_key, $app->cp_public_key);
    
    $trxInfo = $cps->getTransactionInfo($txn_id);
    
    
    if($trxInfo['error'] == 'ok')
    {
        $status = $trxInfo['result']['status'];
        $status_text = $trxInfo['result']['status_text'];
        
        $deposit = Deposit::where('trx_id', $txn_id)->first();
        
        
        if($deposit && $deposit->status != 'success')
        {
            if ($status >= 100 || $status == 2) 
            {
                // payment is complete or queued for nightly payout, success
                
                $deposit->status_text = $status_text;
                $deposit->status = 'success';
                $deposit->save();
            
                $user = $deposit->member;
                $user->balance = $user->balance + $deposit->amount_from;
                $user->save();
            
                $trx = new Transaction();
                $trx->member_id = $user->id;
                $trx->type = 'in';
                $trx->transaction_id = $deposit->trx_id;
                $trx->amount = $deposit->amount_from;
                $trx->message = "Fund deposited to the wallet.";
                $trx->status = true;
                $trx->save();
            
                $trx = new AdminEarning();
                $trx->member_id = $user->id;
                $trx->amount = $deposit->amount_from;
                $trx->description = "You recevied a deposit fund";
                $trx->save(); 
                
                
                $ipnError = new IpnMessage();
                $ipnError->message = $status_text." - TRX: ".$txn_id;
                $ipnError->status = "success";
                $ipnError->save();
            
            } 
            else if ($status < 0) 
            {
                //payment error, this is usually final but payments will sometimes be reopened if there was no exchange rate conversion or with seller consent
                $deposit->status_text = $status_text;
                $deposit->status = 'declined';
                $deposit->save();
                
                $ipnError = new IpnMessage();
                $ipnError->message = $status_text." - TRX: ".$txn_id;
                $ipnError->status = "declined";
                $ipnError->save();
            
            } 
            else 
            {
                //payment is pending, you can optionally add a note to the order page
                $deposit->status_text = $status_text;
                $deposit->status = 'pending';
                $deposit->save();
                
                $ipnError = new IpnMessage();
                $ipnError->message = $status_text." - TRX: ".$txn_id;
                $ipnError->status = "pending";
                $ipnError->save();
            
            }
        }
        else 
        {
            die('Trabsaction aready succeed.');
        }
    
    }
    
    die('IPN OK');
}
else 
{
    die('IPN ERROR');
}
