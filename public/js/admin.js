function clickAndCopy() {
  var copyText = document.getElementById("copyText");
  var copyBtn = document.getElementById("copyBtn");
  copyText.select();
  copyText.setSelectionRange(0, 99999)
  document.execCommand("copy");
  copyBtn.innerHTML = "Copied!";
}