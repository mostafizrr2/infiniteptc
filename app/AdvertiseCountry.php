<?php

namespace App;

use App\Advertise;
use Illuminate\Database\Eloquent\Model;

class AdvertiseCountry extends Model
{

    public function advertise()
    {
        $this->belongsTo(Advertise::class);
    }
}
