<?php

namespace App;

use App\Member;
use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    
    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
