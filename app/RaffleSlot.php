<?php

namespace App;

use App\Member;
use App\Raffle;
use App\RafflePrize;
use Illuminate\Database\Eloquent\Model;

class RaffleSlot extends Model
{
    public function member()
    {
        return $this->belongsTo(Member::class);
    }
    public function raffle()
    {
        return $this->belongsTo(Raffle::class);
    }

    public function prize()
    {
        return $this->belongsTo(RafflePrize::class, 'prize_id');
    }
}
