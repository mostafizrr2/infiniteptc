<?php

namespace App;

use App\Member;
use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
   public function members()
   {
       return $this->hasMany(Member::class);
   }
}
