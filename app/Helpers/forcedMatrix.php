<?php
namespace App\Helpers;

use App\Tree;
use App\Level;
use App\Member;
use App\AdminEarning;
use App\RefferalEarning;
use App\MemberEarningHistory;
use App\Matrix;
use Illuminate\Database\Eloquent\Model;

class forcedMatrix{

    private $count = 0;

    public function matrixFor($member)
    {
        $this->MatrixCalCulator($member);
    }


    public function MatrixCalCulator($member)
    {
        $allMatrices = Matrix::all();

        $newMatrix = $this->NewPositionsAdminAndSponsorEarnings($member);

        if(count($allMatrices))
        {
            foreach($allMatrices as $mtx)
            {
                if($mtx->left == null)
                {
                    $mtx->left_id = $newMatrix->id;
                    $mtx->save();
                
                    $newMatrix->parent_id = $mtx->id;
                    $newMatrix->save();

                    break;
                }
                elseif($mtx->middle == null)
                {
                    $mtx->middle_id = $newMatrix->id;
                    $mtx->save();
            
                    $newMatrix->parent_id = $mtx->id;
                    $newMatrix->save();
                    
                    break;
                }
                elseif($mtx->right == null)
                {
                    $mtx->right_id = $newMatrix->id;
                    $mtx->save();
             
                    $newMatrix->parent_id = $mtx->id;
                    
                    $newMatrix->save();

            //Payment for new posiotion in the matrix
                    $level = Level::where('level', 1)->first();

                    $user = $mtx->member;

                    if($level->payouts > 0)
                    {
                        if($user->level->level < $level->level)
                        {
                            $user->level_id = $level->id;
                        }

                        $user->total_earning = $user->total_earning + $level->payouts;
                        $user->available_withdrawal = $user->available_withdrawal + $level->payouts;
                        $user->save();
        
                        $earning = new MemberEarningHistory();
                        $earning->member_id = $user->id;
                        $earning->type = 'in';
                        $earning->title = 'Level up';
                        $earning->description = 'You earned a level up bonus';
                        $earning->amount = $level->payouts;
                        $earning->save();
                    }

                    break;
                }
                else 
                {
                    continue;
                }
                //End of if else under foreach loop

            }// end of foreach($allMatrices as $mtx)
          
            //step 1
            if($parentMatrix1 = $newMatrix->parent)
            {
                $this->count = 0;
                $member1 = $parentMatrix1->member;
                $this->increamentMemberLevel($member1);

                //step 2
                if($parentMatrix2 = $parentMatrix1->parent)
                {
                    $this->count = 0;
                    $member2 = $parentMatrix2->member;
                    $this->increamentMemberLevel($member2);

                    //step 3
                    if($parentMatrix3 = $parentMatrix2->parent)
                    {
                        $this->count = 0;
                        $member3 = $parentMatrix3->member;
                        $this->increamentMemberLevel($member3);

                        //step 4
                        if($parentMatrix4 = $parentMatrix3->parent)
                        {
                            $this->count = 0;
                            $member4 = $parentMatrix4->member;
                            $this->increamentMemberLevel($member4);

                            //step 5
                            if($parentMatrix5 = $parentMatrix4->parent)
                            {
                                $this->count = 0;
                                $member5 = $parentMatrix5->member;
                                $this->increamentMemberLevel($member5);

                                //step 6
                                if($parentMatrix6 = $parentMatrix5->parent)
                                {
                                    $this->count = 0;
                                    $member6 = $parentMatrix6->member;
                                    $this->increamentMemberLevel($member6);

                                    //step 7
                                    if($parentMatrix7 = $parentMatrix6->parent)
                                    {
                                        $this->count = 0;
                                        $member7 = $parentMatrix7->member;
                                        $this->increamentMemberLevel($member7);
                                    }//end of step 7
                                }//end of step 6
                            }//end of step 5
                        }//end of step 4
                    }//end of step 3
                }//end of step 2
            }//end of step 1
        } // End of if(count($allMatrices))
    }


    public function increamentMemberLevel($member)
    {
        $memberMatrix = Matrix::where('member_id', $member->id)->first();

        $this->userMatrixCounter($memberMatrix);

        $counter = ($this->count - 1);

        $levelCount = 0;

        if( $counter == 3 )
        {
             $levelCount = 1;
        }
        elseif($counter == 12)
        {
             $levelCount = 2;
        }
        elseif( $counter == 39 )
        {
             $levelCount = 3;
        }
        elseif($counter == 120)
        {
             $levelCount = 4;
        }
        elseif($counter == 363)
        {
             $levelCount = 5;
        }
        elseif($counter == 1092)
        {
             $levelCount = 6;
        }
        elseif($counter == 3279 )
        {
             $levelCount = 7;
        }


        $level = Level::where('level',  $levelCount)->first();

        if( $level &&  $level->level > 1 )
        {
            if( $level->level > $member->level->level )
            {
                $member->level_id = $level->id;
                $member->save();
            }

            if($level->payouts > 0)
            {
                $member->total_earning = $member->total_earning + $level->payouts;
                $member->available_withdrawal = $member->available_withdrawal + $level->payouts;
                $member->save();

                $earning = new MemberEarningHistory();
                $earning->member_id = $member->id;
                $earning->type = 'in';
                $earning->title = 'Level up';
                $earning->description = 'You earned a level up bonus';
                $earning->amount = $level->payouts;
                $earning->save();
            }

            if($level->refferal_bonus > 0)
            {
                $sponsor = $member->sponsor;

                if( $sponsor )
                {
                    $sponsor->refferal_earning =  $sponsor->refferal_earning + $level->refferal_bonus; 
                    $sponsor->total_earning =  $sponsor->total_earning + $level->refferal_bonus; 
                    $sponsor->available_withdrawal =  $sponsor->available_withdrawal + $level->refferal_bonus; 
                    $sponsor->save();
                    
                    $ref = new RefferalEarning();
                    $ref->member_id = $member->id;
                    $ref->sponsor_id = $sponsor->id;
                    $ref->amount = $level->refferal_bonus;
                    $ref->description = "You earned from your refferal member's level up";
                    $ref->save();
                }
            }
        }
    }

    public function userMatrixCounter($matrix)
    {
        $matrices = [];
        
        if($matrix)
        {
            if( $matrix->left != null)
            {
                array_push( $matrices, $matrix->left);
            }
    
            if( $matrix->middle != null)
            {
                array_push( $matrices, $matrix->middle);
            }

            if( $matrix->right != null)
            {
                array_push( $matrices, $matrix->right);
            }

            foreach($matrices as $key => $mtx)
            {
                $this->userMatrixCounter($mtx);

                if ($key == 3)
                { 
                    break;
                }
            }
            $this->count++;
        }
    }


    public function NewPositionsAdminAndSponsorEarnings($member)
    {
        $newMatrix = new Matrix();
        $newMatrix->member_id = $member->id;
        $newMatrix->username = $member->username;
        $newMatrix->parent_id = null;
        $newMatrix->left_id = null;
        $newMatrix->middle_id = null;
        $newMatrix->right_id = null;
        $newMatrix->save();

        $memberSponsor = $member->sponsor;

        if( $memberSponsor )
        {
            $memberSponsor->refferal_earning =  $memberSponsor->refferal_earning + 1; 
            $memberSponsor->total_earning =  $memberSponsor->total_earning + 1; 
            $memberSponsor->available_withdrawal =  $memberSponsor->available_withdrawal + 1; 
            $memberSponsor->save();
            
            $ref = new RefferalEarning();
            $ref->member_id = $member->id;
            $ref->sponsor_id = $memberSponsor->id;
            $ref->amount = 1;
            $ref->description = "Referred member got a new position";
            $ref->save();
        }
        $admin = new AdminEarning();
        $admin->member_id = $member->id;
        $admin->amount = 1.25;
        $admin->description = "Admin fees from member's new position";
        $admin->save();

        return $newMatrix;
    }
}