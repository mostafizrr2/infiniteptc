<?php 

use App\Tree;
//get Downline of a Member, this function is needed so that you can simply call left or right of the memberid you are looking for
function GetDownline($member_id, $direction) 
{ 
    $getdownlinesql = Tree::where('sponsor_id', $member_id)->where('position', $direction)->first();

    // $getdownlinesql = @mysql_fetch_assoc(@mysql_query('select memid,placementid,position from `yourtablehere` where placementid="'.$member_id.'" and  position="'.$direction.'"'));
    $getdownline = $getdownlinesql->member_id;
    
    return $getdownline; 
}
//get the child of the member, this section will look for left or right of a member, once found, it will call GetNextDownlines() function to assign new memberid variables for left or right
function GetChildDownline($member_id) 
{ 
    $getchilddownlinesql =  Tree::where('sponsor_id', $member_id)->get();
    // $getchilddownlinesql = @mysql_query('select memid,placementid,position from `yourtablehere` where placementid="'.$member_id.'" ORDER BY position');

    foreach($getchilddownlinesql as $downline){
        
            $childdownlinecode = $downline->member_id;
            $direction = $downline->position;
            
            if($direction=='left'){
                if($childdownlinecode){
                    //this is where you play with your html layout
                    echo $childdownlinecode.'<br>';
                    GetNextDownlines($childdownlinecode,'left');
                }
            }
            
            if($direction=='middle'){
                    if($childdownlinecode){
                        //this is where you play with your html layout
                        echo $childdownlinecode.'<br>';
                        GetNextDownlines($childdownlinecode,'middle');
                }
            }
            
            if($direction=='right'){
                    if($childdownlinecode){
                        //this is where you play with your html layout
                        echo $childdownlinecode.'<br>';
                        GetNextDownlines($childdownlinecode,'right');
                }
            }
        }
    // while($childdownline = mysql_fetch_array($getchilddownlinesql)){

    // 		$childdownlinecode = $childdownline['memid'];
    //         $direction = $childdownline['position'];
            
    // 		if($direction=='left'){
    // 			if($childdownlinecode){
    // 				//this is where you play with your html layout
    // 				echo $childdownlinecode.'<br>';
    // 				GetNextDownlines($childdownlinecode,'left');
    // 			}
    // 		}
            
    // 		if($direction=='right'){
    // 				if($childdownlinecode){
    // 					//this is where you play with your html layout
    // 					echo $childdownlinecode.'<br>';
    // 					GetNextDownlines($childdownlinecode,'right');
    // 			}
    // 		}
    //  }
}
//recursive function to call the functions and start all over again, this is where you can get the newly assigned memberid, call the GetChildDownline() that gets the left or right, then recycle all codes
function GetNextDownlines($member_id,$direction) 
{
    if($direction=='left'){

        $topleft = GetDownline($member_id,'left');
        if($topleft){
            //this is where you play with your html layout
            echo $topleft.'<br>';
        }

        $getleftdownlinesql = Tree::where('sponsor_id', $topleft)->get();

        // $getleftdownlinesql = @mysql_query('select memid,placementid,position from `yourtablehere` where placementid="'.$topleft.'" ORDER BY position');

        foreach($getleftdownlinesql as $downline){
            $leftdownline = $downline->member_id;
            $leftdirection = $downline->position;
            
            if($leftdirection=='left'){
                if($leftdownline){
                    //this is where you play with your html layout
                    echo $leftdownline.'<br>';
                    GetChildDownline($leftdownline);
                }
            }

            if($leftdirection=='middle'){
                if($leftdownline){
                    //this is where you play with your html layout
                    echo $leftdownline.'<br>';
                    GetChildDownline($leftdownline);
                }
            }
            
            if($leftdirection=='right'){
                if($leftdownline){
                    //this is where you play with your html layout
                    echo $leftdownline.'<br>';
                    GetChildDownline($leftdownline);
                }
            }
        }
        // while($getleftdownline = mysql_fetch_array($getleftdownlinesql)){
        //     $leftdownline = $getleftdownline['memid'];
        //     $leftdirection = $getleftdownline['position'];
            
        //     if($leftdirection=='left'){
        //             if($leftdownline){
        //         //this is where you play with your html layout
        //                 echo $leftdownline.'<br>';
        //                 GetChildDownline($leftdownline);
        //             }
        //     }
            
        //     if($leftdirection=='right'){
        //             if($leftdownline){
        //         //this is where you play with your html layout
        //             echo $leftdownline.'<br>';
        //                 GetChildDownline($leftdownline);
        //                 }
        //     }
        // }
    }


    if($direction=='middle'){

        $topMiddle = GetDownline($member_id,'middle');
        if($topMiddle){
            //this is where you play with your html layout
            echo $topMiddle.'<br>';
        }

        $getMiddledownlinesql = Tree::where('sponsor_id', $topMiddle)->get();

        // $getMiddledownlinesql = @mysql_query('select memid,placementid,position from `yourtablehere` where placementid="'.$topMiddle.'" ORDER BY position');

        foreach($getMiddledownlinesql as $downline){
            $leftdownline = $downline->member_id;
            $leftdirection = $downline->position;
            
            if($leftdirection=='left'){
                if($leftdownline){
                    //this is where you play with your html layout
                    echo $leftdownline.'<br>';
                    GetChildDownline($leftdownline);
                }
            }

            if($leftdirection=='middle'){
                if($leftdownline){
                    //this is where you play with your html layout
                    echo $leftdownline.'<br>';
                    GetChildDownline($leftdownline);
                }
            }
            
            if($leftdirection=='right'){
                if($leftdownline){
                    //this is where you play with your html layout
                    echo $leftdownline.'<br>';
                    GetChildDownline($leftdownline);
                }
            }
        }

    }


    if($direction=='right'){
        $topright = GetDownline($member_id,'right');
        if($topright){
        echo $topright.'<br>';
        }

        $getrightdownlinesql = Tree::where('sponsor_id', $topright)->get();
        // $getrightdownlinesql = @mysql_query('select memid,placementid,position from `yourtablehere` where placementid="'.$topright.'" ORDER BY position');

        foreach($getrightdownlinesql as $downline){
            $rightdownline = $downline->member_id;
            $rightdirection = $downline->position;
            
            if($rightdirection=='left'){
                if($rightdownline){
                //this is where you play with your html layout
                echo $rightdownline.'<br>';
                GetChildDownline($rightdownline);
                }
            }

            if($rightdirection=='middle'){
                if($rightdownline){
                //this is where you play with your html layout
                echo $rightdownline.'<br>';
                GetChildDownline($rightdownline);
                }
            }
            
            if($rightdirection=='right'){
                if($rightdownline){
                    //this is where you play with your html layout
                    echo $rightdownline.'<br>';
                    GetChildDownline($rightdownline);
                }
            }
    
        }
    }
}

