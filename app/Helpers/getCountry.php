<?php

function userCountryName()
{
    $ip = request()->ip();
    $details = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip={$ip}"));
    $country = ($details->geoplugin_countryName != null) ? $details->geoplugin_countryName : "Bangladesh";
    return $country;
}

function userCountryAll()
{
    $ip = request()->ip();
    return json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip={$ip}"));
}