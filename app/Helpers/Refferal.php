<?php 

function getRefferalLink($id = null)
{
    if($id != null)
    {
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') 
        { 
            $protocol = "https://";
        }
        else 
        {
            $protocol = "http://";
        }
    
        return $protocol . $_SERVER['HTTP_HOST'] . '/user/sign-up?ref_id=' . $id;
    }

}