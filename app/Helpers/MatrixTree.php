<?php
namespace App\Helpers;

use App\Matrix;

class MatrixTree{

     public $count = 0;
     public $matrixiId = null;
     public $matrix = null;
     public $mtx_ids = [];

     public function drawTree($sponsor)
     {
          $memberMatrix = Matrix::where('member_id', $sponsor->id)->first();
          $output = "";
          $output .= $this->myTree($memberMatrix);
          return $output;
     }


     public function myTree($matrix)
     {
          $matrices = [];
          $memberMatrix = $matrix;
          if($memberMatrix)
          {

               if( $memberMatrix->left != null)
               {
                    array_push( $matrices, $memberMatrix->left);
               }
       
               if( $memberMatrix->middle != null)
               {
                    array_push( $matrices, $memberMatrix->middle);
               }

               if( $memberMatrix->right != null)
               {
                    array_push( $matrices, $memberMatrix->right);
               }

               $menu = "";

               if($this->count <= 3279)
               {
                    foreach($matrices as $key => $mtx)
                    {
                    
                         $member = $mtx->member;

                         $caret = 'caret';
                         $nested = 'nested';

                         // $caret = '';
                         // $nested = '';


                         $menu .= '<li class="parent-li">';
                         
                         $menu .= '<div class="parent '.$caret.'"><img src="'. avatar($member->avatar) .'">';
                         $menu  .= $member->username.'</div>';
               
                         $menu .= '<ol class="childs '.$nested.'">'. $this->myTree($mtx) .'</ol>';
               
                         $menu .= '</li>';

                         if ($key == 3)
                         { 
                              break;
                         }
                    }
               }

               $this->count++;
               
               return $menu;
          }
     }
}

function avatar($avatar){
     if($avatar != null)
     {
        return url('storage/member/'. $avatar);
     } 
     else 
     {
        return url('defaults/user.png');
     }
 }
