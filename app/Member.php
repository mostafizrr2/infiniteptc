<?php

namespace App;

use App\Room;
use App\Level;
use App\Matrix;
use App\Wallet;
use App\Deposit;
use App\Withdraw;
use App\Advertise;
use App\Membership;
use App\AdpackPurchase;
use App\MemberEarningHistory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    use Notifiable;

    protected $fillable = ['member_id', 'name', 'username', 'email', 'address', 'state', 'country', 'zip', 'verify_token', 'status', 'password'];

    
    public function sponsor()
    {
        return $this->belongsTo(Member::class);
    }

    public function sponsors()
    {
        return $this->hasMany(Member::class, 'sponsor_id');
    }


    public function members()
    {
        return $this->hasMany(Member::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    public function purchases()
    {
        return $this->hasMany(AdpackPurchase::class);
    }

    public function advetises()
    {
        return $this->hasMany(Advertise::class);
    }

    public function histories()
    {
        return $this->hasMany(MemberEarningHistory::class)->orderBy('id', 'DESC');
    }

    public function withdraws()
    {
        return $this->hasMany(Withdraw::class)->orderBy('id', 'DESC');
    }

    public function membership()
    {
        return $this->belongsTo(Membership::class);
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class)->orderBy('id', 'DESC');
    }

    public function wallets()
    {
        return $this->hasMany(Wallet::class)->orderBy('id', 'DESC');
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class)->where('status', 1);
    }

    //Matrix releted relationships
    
    public function matrices()
    {
        return $this->hasMany(Matrix::class);
    }

    // public function matrix()
    // {
    //     return $this->hasOne(Matrix::class);
    // }

    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }
}
