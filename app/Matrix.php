<?php

namespace App;

// use App\Matrix;
use App\Member;
use Illuminate\Database\Eloquent\Model;

class Matrix extends Model
{

    public function member()
    {
        return $this->belongsTo(Member::class);
    }
    
    public function parent()
    {
        return $this->belongsTo(Matrix::class);
    }

    public function left()
    {
        return $this->belongsTo(Matrix::class);
    }

    public function middle()
    {
        return $this->belongsTo(Matrix::class);
    }

    public function right()
    {
        return $this->belongsTo(Matrix::class);
    }
}
