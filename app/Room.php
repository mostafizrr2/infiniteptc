<?php

namespace App;

use App\Member;
use App\Message;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function members()
    {
        return $this->belongsToMany(Member::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'user_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }


}
