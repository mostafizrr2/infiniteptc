<?php

namespace App;

use App\Member;
use App\AdvertiseCountry;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Admin\AdvertiseController;

class Advertise extends Model
{
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

}
