<?php

namespace App;

use App\RaffleSlot;
use Illuminate\Database\Eloquent\Model;

class RafflePrize extends Model
{
    public function slot()
    {
        return $this->hasOne(RaffleSlot::class, 'prize_id');
    }
}
