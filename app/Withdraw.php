<?php

namespace App;

use App\Member;
use App\Wallet;
use Illuminate\Database\Eloquent\Model;

class Withdraw extends Model
{
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }
}
