<?php

namespace App;

use App\RaffleSlot;
use App\RafflePrize;
use Illuminate\Database\Eloquent\Model;

class Raffle extends Model
{
    public function prizes()
    {
        return $this->hasMany(RafflePrize::class, 'raffle_id');
    }
    public function slots()
    {
        return $this->hasMany(RaffleSlot::class, 'raffle_id');
    }

    public function free_slots()
    {
        return $this->hasMany(RaffleSlot::class, 'raffle_id')->where('member_id', null);
    }
}
