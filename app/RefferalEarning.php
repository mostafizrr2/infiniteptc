<?php

namespace App;

use App\Member;
use Illuminate\Database\Eloquent\Model;

class RefferalEarning extends Model
{
    
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function sponsor()
    {
        return $this->belongsTo(Member::class);
    }
}
