<?php

namespace App;

use App\Member;
use Illuminate\Database\Eloquent\Model;

class AdminEarning extends Model
{
    
    public function member()
    {
        return $this->belongsTo(Member::class);
    }
}
