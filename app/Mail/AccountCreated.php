<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $user_name;
    private $user_email;
    private $password;

    public function __construct($user, $password)
    {
        $this->user_name = $user->name;
        $this->user_email = $user->email;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->user_name;
        $data['email'] = $this->user_email;
        $data['password'] = $this->password;
        return $this->markdown('emails.AccountCreated',$data);
    }
}
