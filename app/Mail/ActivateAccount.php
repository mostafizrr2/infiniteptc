<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ActivateAccount extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $user_name;
    private $user_email;
    private $user_verify_token;

    public function __construct($user)
    {
        $this->user_name = $user->name;
        $this->user_email = $user->email;
        $this->user_verify_token = $user->verify_token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->user_name;
        $data['email'] = $this->user_email;
        $data['token'] = $this->user_verify_token;
        return $this->markdown('emails.activateAccount',$data);
    }
}
