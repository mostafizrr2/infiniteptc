<?php

namespace App;

use App\Member;
use Illuminate\Foundation\Auth\User;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'member_id');
    }
}
