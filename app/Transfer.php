<?php

namespace App;

use App\Member;
use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    
    public function sender()
    {
        return $this->belongsTo(Member::class);
    }
    
    public function receiver()
    {
        return $this->belongsTo(Member::class);
    }
}
