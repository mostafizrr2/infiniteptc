<?php

namespace App\Http\Controllers\Member;

use App\Wallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->guard('member')->user();

        $data['wallets'] = $user->wallets;

        return view('member.wallet.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required|max:50',
            'address' => 'required|max:250',
            ]);
            
        $user = auth()->guard('member')->user();
        
        $wallets = $user->wallets;

        if($request->has('status') && $request->status == "on")
        {
            foreach($wallets as $wlt)
            {
                $wlt->status = false;
                $wlt->save();
            }
            
            $status = true;
        }
        else 
        {
            $status = false;
        }

        if(!count( $wallets ))
        {
            $status = true;
        }

        $wallet = new Wallet();
        $wallet->member_id = $user->id;
        $wallet->title = $request->title;
        $wallet->type = "BTC";
        $wallet->address = $request->address;
        $wallet->description = $request->description;
        $wallet->status = $status;
        $wallet->save();

        Toastr::success('Wallet created successfully.', 'Success');
        return redirect()->back();
    }

    public function makeDefault(Request $request)
    {
        $user = auth()->guard('member')->user();
        $wallets = $user->wallets;

        foreach($wallets as $wlt)
        {
            $wlt->status = false;
            $wlt->save();
        }
        
        $wallet = Wallet::whereId($request->id)->where('member_id', $user->id)->first();
        $wallet->status = true;
        $wallet->save();

        Toastr::success('Deafault wallet changed successfully.', 'Success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'title' => 'required|max:50',
            'address' => 'required',
        ]);

        $user = auth()->guard('member')->user();

        $wallet = Wallet::whereId($id)->where('member_id', $user->id)->first();
        $wallet->title = $request->title;
        $wallet->address = $request->address;
        $wallet->description = $request->description;
        $wallet->save();

        Toastr::success('Wallet updated successfully.', 'Success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth()->guard('member')->user();

        $wallet = Wallet::whereId($id)->where('member_id', $user->id)->first();

        if($wallet->status == 1)
        {
            Toastr::error('you annot delete deafault wallet.', 'Error!');
            return redirect()->back();
        }

        $wallet->delete();

        Toastr::success('Wallet deleted successfully.', 'Success');
        return redirect()->back();
    }
}
