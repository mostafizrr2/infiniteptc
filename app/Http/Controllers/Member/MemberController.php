<?php

namespace App\Http\Controllers\Member;

use App\Member;
use App\ClickEarn;
use Illuminate\Http\Request;
use App\MemberEarningHistory;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Member\ForcedMatrix;

class MemberController extends Controller
{
    public function index()
    {
        $user = auth()->guard('member')->user();

        $todaysClicks = ClickEarn::where('member_id', $user->id)
        ->whereDate('created_at', Carbon::today())->get();
        
        $todaysEarn = 0.00;
  
        foreach($todaysClicks as $clicks)
        {
             $todaysEarn = $todaysEarn + $clicks->earning;
        }
 
        $data['todaysEarn'] = $todaysEarn;
        
        $data['histories'] = MemberEarningHistory::where('member_id', $user->id)->latest()->get()->take(10);
        return view('member.index', $data);
    }

    public function myAds()
    {
        return view('member.myAds');
    }
}
