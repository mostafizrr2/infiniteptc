<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\RefferalEarning;
use Illuminate\Http\Request;

class RefferalEarningController extends Controller
{
    public function index()
    {
        $user = auth()->guard('member')->user();
        $data['earnings'] = RefferalEarning::where('sponsor_id', $user->id)->latest()->paginate(20); 
        return view('member.earning.refferal', $data);
    }
}
