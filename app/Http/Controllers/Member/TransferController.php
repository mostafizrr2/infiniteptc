<?php

namespace App\Http\Controllers\Member;

use App\Member;
use App\Transfer;
use Illuminate\Http\Request;
use App\MemberEarningHistory;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class TransferController extends Controller
{
    public function index()
    {
        $user = auth()->guard('member')->user();
        $data['transfers'] = Transfer::where('sender_id', $user->id)->latest()->paginate(20);
        return view('member.transfer.index', $data);
    }

    public function postTranfer(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric',
            'username' => 'required',
        ]);

        $user = auth()->guard('member')->user();



        if($request->username == $user->username)
        {
            Toastr::error('You cannot transfer funds from your account balance to your withdrawal balance.', 'Sorry!');
            return redirect()->back();
        }
        
        if($request->amount < 7.5)
        {
            Toastr::error('You cannot transfer less than 7.5$.', 'Sorry!');
            return redirect()->back();
        }

        if($request->amount > $user->balance)
        {
            Toastr::error('You do not have enough balance to transfer this amount.', 'Sorry!');
            return redirect()->back();
        }

        $receiver = Member::where('username', $request->username)->first();

        if(!$receiver)
        {
            Toastr::error('There is no user exists with this username.', 'Sorry!');
            return redirect()->back();
        }

        $user->balance = $user->balance - $request->amount;
        $user->save();

        $receiver->available_withdrawal = $receiver->available_withdrawal + $request->amount;
        $receiver->save();


        if($request->note != "")
        {
           $note = $request->note;
        }
        else
        {
            $note = "Funds sent to your desired member";
        }

        $trns = new Transfer();
        $trns->sender_id = $user->id;
        $trns->receiver_id = $receiver->id;
        $trns->amount = $request->amount;
        $trns->note = $note;
        $trns->save();


        if($request->note != "")
        {
           $note = $request->note;
        }
        else
        {
            $note = "_____";
        }


        $history = new MemberEarningHistory();
        $history->member_id = $receiver->id;
        $history->type = "in";
        $history->title = "Fund transfered from ". $user->username;
        $history->description = $note;
        $history->amount = $request->amount;
        $history->save();

        Toastr::success('Your funds are successfully transfered to '. $receiver->username .'.', 'Success!');
        return redirect()->back();

    }
}
