<?php

namespace App\Http\Controllers\Member;

use App\Advertise;
use App\AdvertiseCountry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class AdvertiseController extends Controller
{
  
    public function index()
    {
        $countries = countries();
        $countries = json_decode($countries, true);

        $countryByName = [];
        foreach($countries as $country)
        {
            $countryByName[] = $country['country'];
        }

        $user = auth()->guard('member')->user();
        $data['advertises'] = $user->advetises;
        $data['countries'] =  $countryByName;
        return view('member.advertise.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $request->validate([
            "title" => 'required|max:100|min:4',
            "link" => 'required|url|max:150',
            "description" => "required|max:300",
            "required_clicks" => "required|numeric",
            "country" => "required",
        ]);

        $user = auth()->guard('member')->user();

        if($request->required_clicks > $user->available_adclick)
        {
            Toastr::error('You do not have enough available clicks to make this advertisement. please buy an ad pack.', 'Error');
            return redirect()->back();
        }

        $advertise = new Advertise();
        $advertise->member_id = auth()->guard('member')->user()->id;
        $advertise->title = $request->title;
        $advertise->link = $request->link;
        $advertise->description = $request->description;
        $advertise->required_clicks = $request->required_clicks;
        $advertise->is_running = false;
        $advertise->status = false;
        $advertise->save();

        $user->available_adclick = $user->available_adclick - $advertise->required_clicks;
        $user->save();


        if($request->country == 'not')
        {
            $advertise->country_should = null;
        }
        elseif($request->country == 'exclude')
        {
            $advertise->country_should = 'exclude';
        }
        elseif($request->country == 'include')
        {
            $advertise->country_should = 'include';
        }

        $advertise->save();



        if($request->country != 'not')
        {
            if($request->has('countries'))
            {
                foreach($request->countries as $country)
                {
                    $ctr = new AdvertiseCountry();
                    $ctr->advertise_id = $advertise->id;
                    $ctr->country = $country;
                    $ctr->type = ""; 
                    $ctr->save(); 
                }
            }
        }


        Toastr::success('Your advertisement is successfully created and waiting for admin approval.', 'Success');
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $request->validate([
            "title" => 'required|max:100|min:4',
            "link" => 'required|url|max:150',
            "description" => "required|max:300",
        ]);

        $user = auth()->guard('member')->user();

        if($request->required_clicks > $user->available_adclick)
        {
            Toastr::error('You do not have enough available clicks to make this advertisement. please buy an ad pack.', 'Error');
            return redirect()->back();
        }

        $advertise = Advertise::whereId($id)->first();

        // dd($advertise );

        if( $user->id != $advertise->member_id )
        {
            die('You are unauthenticated to do this performance!');
        }

        // $advertise->member_id = $user->id;
        $advertise->title = $request->title;
        $advertise->link = $request->link;
        $advertise->description = $request->description;
        // $advertise->required_clicks = $request->required_clicks;
        $advertise->is_running = false;
        $advertise->status = false;
        $advertise->save();

        // $user->available_adclick = $user->available_adclick - $advertise->required_clicks;
        // $user->save();

        if($request->country == 'not')
        {
            $advertise->country_should = null;
        }
        elseif($request->country == 'exclude')
        {
            $advertise->country_should = 'exclude';
        }
        elseif($request->country == 'include')
        {
            $advertise->country_should = 'include';
        }

        $advertise->save();



   
            if($request->has('countries'))
            {
                $advertiseCountry = AdvertiseCountry::where('advertise_id', $id)->get();
    
                foreach($advertiseCountry as $adctr)
                {
                    $adctr->delete();
                }

                if($request->country != 'not')
                {

                    foreach($request->countries as $country)
                    {
                        $ctr = new AdvertiseCountry();
                        $ctr->advertise_id = $advertise->id;
                        $ctr->country = $country;
                        $ctr->type = ""; 
                        $ctr->save(); 
                    }
                }
            }
        




        
        Toastr::success('Your advertisement is successfully updated and waiting for admin approval.', 'Success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adsCtr = AdvertiseCountry::where('advertise_id', $id)->get();

        foreach($adsCtr as $ctr)
        {
           $ctr->delete(); 
        }

        $ad = Advertise::whereId($id)->first();
        $ad->delete();

        Toastr::success('Advertise deleted successfully.', 'Success');
        return redirect()->back();
    }
}
