<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\MemberEarningHistory;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    
    public function index()
    {
        $user = auth()->guard('member')->user();

        $data['histories'] = MemberEarningHistory::where('member_id', $user->id)->latest()->paginate(20);
        return view('member.history.index', $data);
    }
}
