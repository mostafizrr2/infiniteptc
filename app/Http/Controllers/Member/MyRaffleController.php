<?php

namespace App\Http\Controllers\Member;

use App\Raffle;
use App\RaffleSlot;
use Illuminate\Http\Request;
use App\MemberEarningHistory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class MyRaffleController extends Controller
{
    public function index($id)
    {
        $user = auth()->guard('member')->user();
        $raffle = Raffle::with('prizes')->with('slots')->with('free_slots')->whereId($id)->where('status', '!=' ,'init')->first();
        if(!$raffle)
        {
            abort(404);
        }  
        $data['raffle'] = $raffle;
        return view('member.raffle.raffle', $data);
    }

    public function rafflePurchase($id)
    {
         $user = auth()->guard('member')->user();
         $slot = RaffleSlot::whereId($id)->first();
         $raffle = $slot->raffle;

         if($raffle->slot_price > $user->balance)
         {
            Toastr::error('Your account balance is low.', 'Sorry!');
            return redirect()->back();
         }

         $slot->member_id = $user->id;
         $slot->save();

         
         $user->balance = $user->balance - $raffle->slot_price;
         $user->save();

         $history = new MemberEarningHistory();
         $history->member_id = $user->id;
         $history->type = "out";
         $history->title = "Raffle slot purchased";
         $history->description = "You have purchased a Raffle slot";
         $history->amount = $raffle->slot_price;
         $history->save();
 

         $slots = RaffleSlot::where('raffle_id', $raffle->id)->where('member_id', null)->get();

         if(count($slots) < 1)
         {
            $raffle->status = 'drawable';
            $raffle->save();
         }
         
         Toastr::success('Slot purchased successfully.', 'Success');
         return redirect()->back();
    }

    public function myRaffles()
    {
        $user = auth()->guard('member')->user();

        // $category_id = Post::orderBy('count', 'desc')->select(DB::raw('category_id,count(*) as count'))->groupBy('category_id')->get();

        $raffleIds = RaffleSlot::where('member_id', $user->id)
        ->orderBy('count', 'desc')
        ->select(DB::raw('raffle_id, count(*) as count'))->groupBy('raffle_id')
        ->get();
        // $raffles = RaffleSlot::with('prizes')->with('slots')->with('free_slots')->where('status', '!=' ,'init')->get();

        $MyActiveRaffles = [];
        $MyDrawableRaffles = [];
        $MyEndedRaffles = [];

        foreach($raffleIds as $id)
        {
            $raffle = Raffle::whereId($id->raffle_id)->first();

            if($raffle->status == 'running')
            {
                $MyActiveRaffles[] = $raffle;
            }
            elseif($raffle->status == 'drawable')
            {
                $MyDrawableRaffles[] = $raffle;
            }
            elseif($raffle->status == 'ended')
            {
                $MyEndedRaffles[] = $raffle;
            }
        }

        // dd($raffles);
 
        $data['MyActiveRaffles'] = $MyActiveRaffles;
        $data['MyDrawableRaffles'] = $MyDrawableRaffles;
        $data['MyEndedRaffles'] = $MyEndedRaffles;
        return view('member.raffle.my-raffles', $data);
    }
}
