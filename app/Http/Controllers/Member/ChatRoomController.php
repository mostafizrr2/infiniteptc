<?php

namespace App\Http\Controllers\Member;

use App\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ChatRoomController extends Controller
{
    public function index()
    {
        return view('member.chat.index');
    }

    public function myChatRoom($id)
    {

        $myId = auth('member')->user()->id;

        $room = Room::find($id);

        $myRoom = DB::table('member_room')
        ->where('room_id', $id)
        ->where('member_id', $myId)
        ->first();

        if(!$myRoom && $room->user_id != $myId)
        {
            return redirect()->route('member.chatrooms');
        }

        $data['roomId'] = $id;
        return view('member.chat.room',$data);
    }
}
