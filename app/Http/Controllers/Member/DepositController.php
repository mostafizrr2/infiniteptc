<?php

namespace App\Http\Controllers\Member;

use App\System;
use App\Deposit;
use App\Transaction;
use App\AdminEarning;
use Illuminate\Http\Request;
use App\Helpers\CoinPayments;
use App\MemberEarningHistory;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class DepositController extends Controller
{ 
    public function index()
    {
        $user = auth()->guard('member')->user();
        $data['depositWaiting'] = Deposit::where('member_id', $user->id)->where('status', 'waiting')->latest()->get();
        $data['depositHistory'] = Deposit::where('member_id', $user->id)->where('status', '!=', 'waiting')->latest()->paginate(50);
        return view('member.deposit.index',$data);
    }


    public function depositEarning (Request $request)
    {
        $request->validate([
            'amount' => 'required'
        ]);

        $user = auth()->guard('member')->user();
       
        if($request->amount < 2)
        {
            Toastr::error('Cannot deposite less than 2$.', 'Sorry!');
            return redirect()->back();
        }
        else if( $request->amount > $user->available_withdrawal )
        {
            Toastr::error('Your available earnings is to low then '. $request->amount."$", 'Opppsss!');
            return redirect()->back();
        }
        else
        {
            $user->available_withdrawal = $user->available_withdrawal - $request->amount;
            $user->balance = $user->balance + $request->amount;
            $user->save();

            $history = new MemberEarningHistory();
            $history->member_id = $user->id;
            $history->type = "out";
            $history->title = "Fund deposited";
            $history->description = "Fund deposited to your account balance.";
            $history->amount = $request->amount;
            $history->save();

            Toastr::success($request->amount.'$ are deposited to your account balance.', 'Success!');
            return redirect()->back();
        }

    }

    public function depositeFromOutside(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric'
        ]);

        if($request->amount < 7.5)
        {
            Toastr::error('Cannot deposite less than 7.5$ from external wallet.', 'Sorry!');
            return redirect()->back();
        }

        $user = auth()->guard('member')->user();

        $app = System::first();

        $cps = new CoinPayments();

        $cps->Setup($app->cp_private_key, $app->cp_public_key);

        $amount = $request->amount;

        $req = array(
            'amount' => $amount,
            'currency1' => 'USD',
            'currency2' => 'BTC',
            'buyer_email' => $user->email,
            'item_name' => 'Deposite Funds'
        );
        // See https://www.coinpayments.net/apidoc-create-transaction for all of the available fields
                
        $result = $cps->CreateTransaction($req);

        // dd($result);

        if ($result['error'] == 'ok') {

            // dd($result['result']);
            // return redirect()->to($result['result']['checkout_url']);

            $record = new Deposit();
            $record->member_id = $user->id;
            $record->trx_id = $result['result']['txn_id'];
            $record->merchant_wallet_Address = $result['result']['address'];
            $record->amount_from = $amount;
            $record->amount_to = $result['result']['amount'];
            $record->received_currency = 'BTC';
            $record->timeout = $result['result']['timeout'];
            $record->checkout_url = $result['result']['checkout_url'];
            $record->status_url = $result['result']['status_url'];
            $record->qrcode_url = $result['result']['qrcode_url'];
            $record->status_text = 'Waiting for funds';
            $record->status = 'waiting';
            $record->save();

            Toastr::success('The transaction not completed yet. We are waiting for receive the amount from you. please send the excact amount (  '. $record->amount_to .' BTC )  to the BTC address specified below. thanks.', 'Waiting for receive amount');
            return redirect()->route('member.transaction', $record->id );

            // $re
            // $le = php_sapi_name() == 'cli' ? "\n" : '<br />';
            // print 'Transaction created with ID: '.$result['result']['txn_id'].$le;
            // print 'Buyer should send '.sprintf('%.08f', $result['result']['amount']).' BTC'.$le;
            // print 'Status URL: '.$result['result']['status_url'].$le;
        } else {
            print 'Error: '.$result['error']."\n";
            die;
        }

    }

    public function transactionView($id)
    {
        $user = auth('member')->user();
        $deposit = Deposit::where('member_id', $user->id)->where('id', $id)->where('status', 'waiting')->first();
        if(!$deposit)
        {
            return abort(404);
        }
        $data['record'] = $deposit;

        return view('member.deposit.view', $data);
    }

    public function completeDeposited(Request $request)
    {
        $request->validate([
            'trx_id' => 'required',
        ]);

        $user = auth('member')->user();
        $deposit = Deposit::where('member_id', $user->id)->where('trx_id', $request->trx_id)->where('status', 'waiting')->first();

        if(!$deposit)
        {
           abort(404);
        }
        else 
        {
            $deposit->status_text = 'Deposit submitted and waiting for confimation';
            $deposit->status = 'submitted';
            $deposit->save();

            Toastr::success('Your funds will be deposit to your account balance automatically after confimation.', 'Success');
            return redirect()->route('member.deposit');
 
        }

    }
    
}


