<?php

namespace App\Http\Controllers\Member;

use App\Withdraw;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Wallet;
use Brian2694\Toastr\Facades\Toastr;

class WithdrawController extends Controller
{
    public function index()
    {
        $user = auth()->guard('member')->user();
        $data['withdraw'] =  Withdraw::where('member_id', $user->id)->latest()->paginate(20);
        return view('member.withdraw.index', $data);
    }

    public function actionWithdraw(Request $request)
    {
        $user = auth()->guard('member')->user();
        
        $request->validate([
            'amount' => 'required|numeric|max:'.$user->available_withdrawal.'|min:10',
        ]);

        if($request->amount > $user->available_withdrawal || $request->amount < 10)
        {
            die('You cannot withdraw at this moment.');
        }

       $withdraw = Withdraw::where('member_id', $user->id)->where('status', 0)->first();

       if($withdraw)
       {
            Toastr::error('You already have a pending request in the queue.', 'Sorry!');
            return redirect()->back();
       }

       $wallet = Wallet::where('member_id', $user->id)->where('status', true)->first();

       if(!$wallet)
       {
        Toastr::error('You do not have any deafult wallet to receive funds. pleas at first create an wallet and try again.', 'Error!');
        return redirect()->back();
       }
    
       $wtdr = new Withdraw();
       $wtdr->member_id = $user->id;
       $wtdr->available_balance = $user->available_withdrawal;
       $wtdr->request_amount = $request->amount;
       $wtdr->status = 0;
       $wtdr->save();

       Toastr::success('Your withdraw request is successfully created and waiting for admin clearance.', 'Success');
       return redirect()->back();

    }
}
