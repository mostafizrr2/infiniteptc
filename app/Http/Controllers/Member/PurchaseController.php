<?php

namespace App\Http\Controllers\Member;

use App\Adpack;
use App\Membership;
use App\AdpackPurchase;
use Illuminate\Http\Request;
use App\Helpers\forcedMatrix;
use App\MemberEarningHistory;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class PurchaseController extends Controller
{
    public function index()
    {
        $member = auth()->guard('member')->user();

        $data['adpacks'] = Adpack::latest()->get();
        $data['purchase_history'] = AdpackPurchase::where('member_id', $member->id)->latest()->paginate(20);

        return view('member.purchase.index', $data);
    }

    public function purchaseAdpack(Request $request)
    {
        $member = auth()->guard('member')->user();

        $adpack = Adpack::whereId($request->adpack_id)->first();

        if($adpack->price > $member->balance)
        {  
            Toastr::error('You do not have enough balance to purchase this ad pack. Please deposite atleast '. $adpack->price .'$ to your account balance first.', 'Sorry!');
            return redirect()->route('member.deposit');
        }

        $adpackPrchase = new AdpackPurchase();
        $adpackPrchase->member_id = $member->id;
        $adpackPrchase->clicks = $adpack->clicks;
        $adpackPrchase->price = $adpack->price;
        $adpackPrchase->description = "Adpack purchased";
        $adpackPrchase->save();

        if($member->membership->type == 'free')
        {
            $membership = Membership::where('type', 'paid')->first();
            $member->membership_id = $membership->id;
        }

        $member->available_adclick = $member->available_adclick + $adpack->clicks;
        $member->balance = $member->balance - $adpack->price;
        $member->save();

        $history = new MemberEarningHistory();
        $history->member_id = $member->id;
        $history->type = "out";
        $history->title = "Adpack purchased";
        $history->description = "You have purchased an Ad pack";
        $history->amount = $adpack->price;
        $history->save();

        $matrix = new forcedMatrix();
        $matrix->matrixFor($member);

        Toastr::success('Ad pack purchased successfully.', 'Success');
        return redirect()->back();
        
    }
}
