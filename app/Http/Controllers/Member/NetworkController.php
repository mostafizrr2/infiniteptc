<?php

namespace App\Http\Controllers\Member;

use App\Helpers\MatrixTree;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NetworkController extends Controller
{

    public function referred()
    {
        $user = auth()->guard('member')->user();

        $data['referred_users'] = $user->sponsors;

        return view('member.network.referred_members', $data);
    }

    public function geneology()
    {
        $user = auth()->guard('member')->user();

        $tree = new MatrixTree();

        $data['user'] = $user;
        $data['tree'] = $tree->drawTree($user);

        return view('member.network.geneology', $data);
    }
}
