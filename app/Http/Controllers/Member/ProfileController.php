<?php

namespace App\Http\Controllers\Member;

use App\Faq;
use App\User;
use App\Member;
use Illuminate\Http\Request;
use App\Mail\UserPAsswordChanged;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{ 
    public function index()
    {
  
        return view('member.user.profile');
    }

    public function profileEdit()
    {
        $data['member'] = auth('member')->user();
        // dd($data['member']);
        return view('member.user.edit', $data);
    }

    public function changePassword()
    {

        $data['member'] = auth('member')->user();
        return view('member.user.password',$data);
    }

    public function updateEdit(Request $request)
    {

        $member = $member = auth('member')->user();
        // dd($member);
        
        if($request->hasFile('avatar'))
        {
            if(!Storage::disk('public')->exists('member'))
            {
               Storage::disk('public')->makeDirectory('member');
            }

            // dd(Storage::disk('public')->exists('member'));

            $file = $request->file('avatar');
            $ext = $file->getClientOriginalExtension();
            $fileName = "member-".time().".".$ext;
            $path = "storage/member/".$fileName;

            // dd($member->avatar);

            if( $member->avatar != "")
            {
                Storage::disk('public')->delete('member/'. $member->avatar);
            }

            Image::make($file)->fit(200,200)->save($path);
        }
        else 
        {
            $fileName = $member->avatar;
        }

        $member->name = $request->name;
        $member->email = $request->email;
        $member->address = $request->address;
        $member->state = $request->state;
        $member->zip = $request->zip;
        $member->avatar = $fileName;
        $member->save();

        Toastr::success('profile updated successfully.', 'Success');
 
        return redirect()->route('user.profile');
    }

    public function updatePassword(Request $request)
    {
     
        $request->validate([
            'current_password' => 'required',
            'password' => 'required|min:8|max:100|confirmed',
            'password_confirmation' => 'required'
        ]);
    //    dd($request->all());
        $member = auth('member')->user();
        // dd( Hash::check($request->current_password, $member->password) );

        if(!Hash::check($request->current_password, $member->password))
        {
            Toastr::error('Current password in incorrect.', 'Error');
 
            return redirect()->back();
        }

        $password = $request->password;

        $member->password = bcrypt($password);
        $member->save();

        Mail::to($member->email)->send(new UserPAsswordChanged($member, $password));

        Toastr::success('Password updated successfully.', 'Success');
        return redirect()->route('user.profile');

    }

    public function logout()
    {
        auth()->guard('member')->logout();
        return redirect()->back();
    }

    public function faq()
    {
        $data['faqs'] = Faq::latest()->get();
        return view('member.faq', $data);
    }
}
