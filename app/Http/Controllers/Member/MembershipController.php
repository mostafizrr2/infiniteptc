<?php

namespace App\Http\Controllers\Member;

use App\Membership;
use Illuminate\Http\Request;
use App\Helpers\forcedMatrix;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class MembershipController extends Controller
{
    
    public function index()
    {
        $user = auth()->guard('member')->user();

        $data['memberships'] = Membership::where('type', '!=', 'free')->get();

        return view('member.membership.index', $data);
    }

    public function membershipPurchase(Request $request)
    {
        $request->validate([
            'membership_id' => 'required|numeric'
        ]);

        $user = auth()->guard('member')->user();
        
        $membership = Membership::whereId($request->membership_id)->first();

        if($membership)
        {
           if($membership->price > $user->balance)
           {
            Toastr::error('Your balace is too low to purchase the membership plan. please deposite atleast '. $membership->price .'$ to your account balance to be able to buy the membership plan.', 'Low balance!');
            return redirect()->route('member.deposit');
           }

           $user->membership_id = $request->membership_id;
           $user->balance = $user->balance - $membership->price;
           $user->save();

           $matrix = new forcedMatrix();
           $matrix->matrixFor($user);

           Toastr::success('Your membership plan is upgraded to '. $membership->title.'.', 'Congratulations!');
           return redirect()->back();
        }
        else
        {
            die("Oopss! something going wrong.");
        }

    }
}
