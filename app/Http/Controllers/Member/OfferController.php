<?php

namespace App\Http\Controllers\Member;

use App\Advertise;
use App\ClickEarn;
use App\AdminEarning;
use App\AdvertiseCountry;
use App\RefferalEarning;
use Illuminate\Http\Request;
use App\MemberEarningHistory;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class OfferController extends Controller
{
   public function myOffers()
   {
       $user = auth()->guard('member')->user();

       $viewdAds = ClickEarn::where('member_id', $user->id)->whereDate('created_at', Carbon::today())->get();

       $adIds = [];

       foreach($viewdAds as $viewd)
       {
          $adIds[] = $viewd->advertise_id;
       }

       $todaysClicks = ClickEarn::where('member_id', $user->id)
       ->whereDate('created_at', Carbon::today())->get();

       $todaysEarn = 0.00;

       foreach($todaysClicks as $clicks)
       {
            $todaysEarn = $todaysEarn + $clicks->earning;
       }

       $todaysClicksCount = count($todaysClicks);



       $availableAds = Advertise::inRandomOrder()->where('status', 1)
                                        ->where('is_running', true)
                                        ->where('member_id', '!=', $user->id)
                                        ->whereNotIn('id', $adIds)
                                        ->take( $user->membership->can_click - $todaysClicksCount)
                                        ->get();

        $advertisements = [];
        $country_type = null;

        foreach($availableAds as $ads)
        {
            $countries = [];
            $advertiseCountry = AdvertiseCountry::where('advertise_id', $ads->id)->get();

            $country_type = $ads->country_should;

            foreach($advertiseCountry as $country)
            {
                $countries[] = $country->country;
            }

            if($country_type == 'exclude')
            {
                if(!in_array($user->country, $countries))
                {
                    $advertisements[] = $ads;
                }
            }
            elseif($country_type == 'include')
            {
                if(in_array($user->country, $countries))
                {
                    $advertisements[] = $ads;
                }  
            }
            elseif($country_type == null)
            {
                $advertisements[] = $ads;
            }

        }//Foreach

       $data['advertises'] = $advertisements;
       $data['todaysClicksCount'] = $todaysClicksCount;
       $data['todaysClicksEarn'] = $todaysEarn;

       return view('member.offer.index', $data);
   }

   public function offerView($id)
   {
        $user = auth()->guard('member')->user();

        $exists = ClickEarn::where('member_id', $user->id)
                    ->where('advertise_id', $id)
                    ->whereDate('created_at', Carbon::today())
                    ->first();

        if( $exists )
        {
            die("You are not allowed to view that ad.");
        }


        $todaysClicks = ClickEarn::where('member_id', $user->id)
        ->whereDate('created_at', Carbon::today())->get();

        $clickCount = count($todaysClicks);
        $clickPermission = $user->membership->can_click;

        if($clickCount >= $clickPermission)
        {
            die("Sorry! You have already clicked your today's all offer.");
        }


        $data['advertise'] = Advertise::where('id', $id)->first();

        return view('member.offer.view', $data);
   }

   public function adClicked($id)
   {
        $advertise = Advertise::whereId($id)->first();

        if(!$advertise)
        {
            die('No advertise found');
        }

        $user = auth()->guard('member')->user();

        if( $advertise->total_clicked < $advertise->required_clicks)
        {
            $advertise->total_clicked = $advertise->total_clicked + 1;

            $advertise->save();

            if($advertise->total_clicked >=  $advertise->required_clicks)
            {
                $advertise->status = 2;
                $advertise->is_running = 0;
                $advertise->save();
            }

            $advertise->save();

            $exists = ClickEarn::where('member_id', $user->id)
            ->where('advertise_id', $id)
            ->whereDate('created_at', Carbon::today())
            ->first();

            if($exists)
            {
                return "You have already clicked that ad";
            }

            $earning = $user->membership->cpc;
            $admin_fees = $user->membership->admin_fees;
            $sponsor_bonus = $user->membership->sponsor_bonus;

            $click = new ClickEarn();
            $click->member_id = $user->id;
            $click->advertise_id = $id;
            $click->earning = $earning;
            $click->save();

            $user->total_earning = $user->total_earning + $click->earning;
            $user->available_withdrawal = $user->available_withdrawal + $click->earning;
            $user->save();

            $history = new MemberEarningHistory();
            $history->member_id = $user->id;
            $history->type = "in";
            $history->title = "Cash Earning";
            $history->description = "You earned ". $click->earning . " by ad click";
            $history->amount = $click->earning;
            $history->save();

            $admin = new AdminEarning();
            $admin->member_id = $user->id;
            $admin->amount = $admin_fees;
            $admin->description = "Admin fees from member ad click";
            $admin->save();

            $sponsor = $user->sponsor;

            if($sponsor)
            {
                //Sponsor bonus 0.005$
                $sponsor->total_earning = $sponsor->total_earning + $sponsor_bonus;
                $sponsor->refferal_earning = $sponsor->refferal_earning + $sponsor_bonus;
                $sponsor->available_withdrawal = $sponsor->available_withdrawal + $sponsor_bonus;
                $sponsor->save();

                //Refferal Earning history
                $ref = new RefferalEarning();
                $ref->member_id = $user->id;
                $ref->sponsor_id = $sponsor->id;
                $ref->amount = $sponsor_bonus;
                $ref->description = 'Earning by ad click';
                $ref->save();

            }

            return "Congrates. You have earn ".  $click->earning  ." just now.";

        }
        else
        {

            if($advertise->total_clicked >=  $advertise->required_clicks)
            {
                $advertise->status = 2;
                $advertise->is_running = 0;
                $advertise->save();
            }
            
             return "This ad has expired";
        }
   }

}
