<?php

namespace App\Http\Controllers;

use App\Tree;
use App\Level;
use App\Member;
use App\Membership;
use App\Helpers\Matrix;
use App\Mail\ResetPassword;
use Illuminate\Http\Request;
use App\Mail\ActivateAccount;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
// use Illuminate\Contracts\Session\Session;

class PublicAuthController extends Controller
{
 
    function __construct()
    {
        $this->middleware('guest:member')->except('logout');
    }
    
    function guard()
    {
        return Auth::guard('member');
    }
    
    public function memberLogin()
    {
        return view('public.login');
    }

    public function memberRegister()
    {
        $ip = request()->ip();
        $details = json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip={$ip}"));
        $data['country'] = userCountryName();

        if(isset($_GET['ref_id']))
        {
            $ref_id = $_GET['ref_id'];
            Session::put('ref_id', $ref_id);
            return redirect('https://infinite-funds-ptc.com');
        }

        $data['ref_id'] = Session::get('ref_id');

        return view('public.register', $data);
    }

    function authLogin(Request $request)
    {
        // dd($request->all());

        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $member = Member::where('email', $request->email)->first();

        if($member)
        {
            if($member->block_status == true)
            {
       
                Session::flash('error', 'Your account is blocked for some of your suspicious activities. Please contact with us to re-open your account');
                
                return redirect()->route('user.login');
            
            }

            if($member && $member->status == false)
            {
                Session::flash('error', 'You cannot login right now. Maybe your account is not verified yet. Check the email to verify the account. If you are already verified, please contact with our support team.');
                
                return redirect()->route('user.login');
            }
    
            if($this->guard()->attempt($request->only(['email', 'password'])))
            {
                $member->country = userCountryName();
                $member->save();
                return redirect()->route('member.index'); 
            }
    
            Session::flash('error', 'Incorrect Email or Password given. Please try again.');
    
            return redirect()->back();
        }
        else 
        {
            Session::flash('error', 'Incorrect Email or Password given. Please try again.');
    
            return redirect()->back();
        }

    }
    

    public function authRegister(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50',
            'username' => 'required|unique:members,username|max:50',
            'email' => 'required|email|unique:members,email|max:50',
            'address' => 'required|max:150',
            'state' => 'required|max:50',
            'zip' => 'required|max:10',
            'password' => 'required|confirmed',
            'referral_id' => 'nullable|numeric'
        ]);

        if($request->referral_id != null)
        {
            $rfr = Member::where('referral_id', $request->referral_id)->first();
            
            if($rfr)
            {
                $sponsor = $rfr->id;
            }
            else
            {
                $sponsor = null;
            }
        }
        else 
        {
            $sponsor = null; 
        }

        $member = new Member();
        $member->sponsor_id = $sponsor;
        $member->name = $request->name;
        $member->username = $request->username;
        $member->email = $request->email;
        $member->address = $request->address;
        $member->state = $request->state;
        $member->country = userCountryName();
        $member->zip = $request->zip;

        $ref_id = substr(time(), -6);
        $member->referral_id = $ref_id;

        $membership = Membership::where('type', 'free')->first();
        $member->membership_id = $membership->id;
        
        $level = Level::where('level', 0)->first();
        $level_id = ($level) ? $level->id : 1;
        $member->level_id = $level_id;

        $member->verify_token = sha1(time());
        $member->status = false;

        $member->password = bcrypt($request->password);
        $member->save();

        Mail::to($member->email)->send(new ActivateAccount($member));

        if(Session::has('ref_id'))
        {
            Session::forget('ref_id');
        }


        if($member)
        {
            Session::flash('success', 'Your account is created. Please check the email to verify.');
            return redirect()->route('user.login');
        }
           
    }


    public function verifyMember($token, $email)
    {
        if($token && $email == "")
        {
            abort(404);
        }

        $member = Member::where('email', $email)->first();

        if($member)
        {
          if( $member->verify_token != null && $member->status == false )
          {
              if($token == $member->verify_token)
              {
                  $member->verify_token = null;
                  $member->status = true;
                  $member->save();

                  Session::flash('success', 'Your account has been activated. Now you can login by your credentials.');
                  return redirect()->route('user.login');
              }
              else
              {
                  return abort(404);
              }
          }
          else 
          {
            return abort(404);
          }
        }
        else
        {
            return abort(404);
        }
    }


    public function userForgotPassword()
    {
        return view('public.forgot-password');
    }

    public function userPasswordResetMail(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        $member = Member::where('email', $request->email)->first();

        if( !$member )
        {
            Session::flash('error', 'We cannot find any account with that email.');
            return redirect()->route('user.pass.forgot');
        }
        else 
        {
            $member->verify_token = sha1(time());
            $member->save();

            Mail::to($member->email)->send(new ResetPassword($member));

            Session::flash('success', 'We have sent an email with the password reset link. please check your email.');
            return redirect()->route('user.pass.forgot');
            
        }
    }


    public function userResetPassword($token, $email)
    {
        if($token && $email == "")
        {
            abort(404);
        }

        $member = Member::where('email', $email)->first();

        if($member)
        {
          if( $member->verify_token != null  )
          {
              if($token == $member->verify_token)
              {
                $data['token'] = $token;
                $data['email'] = $email;
                return view('public.reset-password', $data);
              }
              else
              {
                  return abort(404);
              }
          }
          else 
          {
            return abort(404);
          }
        }
        else
        {
            return abort(404);
        }
    }

    public function userConfirmResetPassword(Request $request, $token, $email)
    {
        $request->validate([
            'password' => 'required|confirmed|min:8'
        ]);
        
        if($token && $email == "")
        {
            abort(404);
        }
        $member = Member::where('email', $email)->first();

        if($member)
        {
          if( $member->verify_token != null  )
          {
              if($token == $member->verify_token)
              {
                 //Sone action will work
                 $member->password = bcrypt($request->password);
                 $member->verify_token = null;
                 $member->save();

                 Session::flash('success', 'Your password has been changed successfully. try to login with the credentials');
                 return redirect()->route('user.login');
              }
              else
              {
                  return abort(404);
              }
          }
          else 
          {
            return abort(404);
          }
        }
        else
        {
            return abort(404);
        }
    }
}
