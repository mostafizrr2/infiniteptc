<?php

namespace App\Http\Controllers\Admin;

use App\Deposit;
use App\Transaction;
use App\AdminEarning;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class DepositsController extends Controller
{
    
    public function index()
    {
        $data['waiting_records'] = Deposit::whereStatus('waiting')->latest()->paginate(50);
        $data['submitted_records'] = Deposit::whereStatus('submitted')->latest()->paginate(50);
        $data['success_records'] = Deposit::whereStatus('success')->latest()->paginate(50);
        return view('admin.request.deposit', $data); 
    }

    public function depositeReceived(Request $request)
    {
       $record = Deposit::whereId($request->deposite_id)->first();
       if(!$record || $record->status == 'success')
       {
        Toastr::error('This deposite record already axcepted.', 'Ooppssss!');
        return redirect()->back();
       }

       $member = $record->member;
       if(!$member){
        Toastr::error('This member ha no existence  in the database..', 'Ooppssss!');
        return redirect()->back();
       };

       $member->balance = $member->balance + $record->amount_from;
       $member->save();

       $record->status_text = 'Deposit confirmed';
       $record->status = 'success';
       $record->save();

       $trx = new Transaction();
       $trx->member_id = $member->id;
       $trx->type = 'in';
       $trx->transaction_id = $record->trx_id;
       $trx->amount = $record->amount_from;
       $trx->message = "Fund deposited to the wallet.";
       $trx->status = true;
       $trx->save();

       $trx = new AdminEarning();
       $trx->member_id = $member->id;
       $trx->amount = $record->amount_from;
       $trx->description = "You recevied a deposit fund";
       $trx->save();

       Toastr::success('You have confirmed the deposit request.', 'Confirmed');
       return redirect()->back();
    }
}
