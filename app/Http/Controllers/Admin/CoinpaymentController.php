<?php

namespace App\Http\Controllers\Admin;

use App\System;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\IpnMessage;
use Brian2694\Toastr\Facades\Toastr;

class CoinpaymentController extends Controller
{

    function index()
    {
      $data['ipnLogs'] = IpnMessage::latest()->paginate(50);
      return view('admin.system.cp', $data);
    }

    function SaveCoinPayment(Request $request)
    {
      $request->validate([
        "cp_title" => 'max:50',
        "cp_merchant_email" => 'max:150',
        "cp_merchant_id" => 'max:150',
        "cp_ipn_secret" => 'max:150',
        "cp_public_key" => 'required|max:255',
        "cp_private_key" => 'required|max:255',
      ]);

      $system = System::first();
      $system->cp_title = $request->cp_title; 
      $system->cp_merchant_email = $request->cp_merchant_email; 
      $system->cp_merchant_id = $request->cp_merchant_id; 
      $system->cp_ipn_secret = $request->cp_ipn_secret; 
      $system->cp_public_key = $request->cp_public_key; 
      $system->cp_private_key = $request->cp_private_key; 
      $system->save();
      Toastr::success('Coinpayment info updated successfully.', 'Success');
      return redirect()->back();
    }

}
