<?php

namespace App\Http\Controllers\Admin;

use App\Raffle;
use App\RaffleSlot;
use App\RafflePrize;
use Illuminate\Http\Request;
use App\MemberEarningHistory;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class RaffleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['initialRaffles'] = Raffle::with('prizes')->with('slots')->where('status','init')->latest()->get();
        $data['runningRaffles'] = Raffle::with('prizes')->with('slots')->with('free_slots')->where('status','running')->latest()->get();
        $data['drawableRaffles'] = Raffle::with('prizes')->with('slots')->with('free_slots')->where('status','drawable')->latest()->get();
        $data['endedRaffles'] = Raffle::with('prizes')->with('slots')->with('free_slots')->where('status','ended')->latest()->paginate(30);
        return view('admin.raffle.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            "title" => "required|max:150",
            "description" => "required",
            "total_slots" => "required|numeric",
            "slot_price" => "required|numeric"
        ]);

        $raffle = new Raffle();
        $raffle->title = $request->title;
        $raffle->description = $request->description;
        $raffle->slot_price = $request->slot_price;
        $raffle->status = 'init';
        $raffle->save();

        if(is_numeric($request->total_slots))
        {
           for($x = 1; $x <= $request->total_slots; $x++)
           {
               $slot = new RaffleSlot();
               $slot->raffle_id = $raffle->id;
               $slot->member_id = null;
               $slot->prize_id = null;
               $slot->status = null;
               $slot->save();
           }
        }

        Toastr::success('Raffle created successfully.', 'Success');
        return redirect()->back();
    }

    public function storePrize(Request $request)
    {
    //    dd($request->all());
       $request->validate([
        "raffle_id" => "required",
        "position" => "required|numeric",
        "prize_amount" => "required|numeric"
       ]);

       $prize = new RafflePrize();
       $prize->raffle_id = $request->raffle_id;
       $prize->position = $request->position;
       $prize->prize_amount = $request->prize_amount;
       $prize->save();

       Toastr::success('Position prize created.', 'Success');
       return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $raffle = Raffle::with('prizes')->with('slots')->with('free_slots')->whereId($id)->where('status','!=', 'init')->first();
        if(!$raffle)
        {
           abort(404);
        }

        $data['raffle'] = $raffle;
        return view('admin.raffle.view', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $request->validate([
            "title" => "required|max:150",
            "description" => "required",
            "total_slots" => "required|numeric",
            "slot_price" => "required|numeric"
        ]);

        $raffle = Raffle::whereId($id)->first();
        $raffle->title = $request->title;
        $raffle->description = $request->description;
        $raffle->slot_price = $request->slot_price;
        $raffle->status = 'init';
        $raffle->save();

        $slots = RaffleSlot::where('raffle_id', $id)->get();

        if(is_numeric($request->total_slots))
        {

           if($request->total_slots != count($slots))
           {
               foreach($slots as $slot)
               {
                  $slot->delete();
               }

               for($x = 1; $x <= $request->total_slots; $x++)
               {
                   $slot = new RaffleSlot();
                   $slot->raffle_id = $raffle->id;
                   $slot->member_id = null;
                   $slot->prize_id = null;
                   $slot->status = null;
                   $slot->save();
               }

           }

        }

        Toastr::success('Raffle updated successfully.', 'Success');
        return redirect()->back();
    }

    public function rafflePublish($id)
    {
        $raffle = Raffle::whereId($id)->where('status','init')->first();

        if(!$raffle)
        {
            abort(404);
        }

        $raffle->status = 'running';
        $raffle->save();

        Toastr::success('Raffle published successfully.', 'Success');
        return redirect()->back();
    }

    public function updatePrize(Request $request, $id)
    {
       $request->validate([
           'prize_amount' => 'required|numeric'
       ]); 

       $prize = RafflePrize::where('raffle_id', $request->raffle_id)->where('id', $id)->first();

       if(!$prize)
       {
           abort(404);
       }

       $prize->prize_amount = $request->prize_amount;
       $prize->save();

       Toastr::success('Prize updated successfully.', 'Success');
       return redirect()->back();
    }

    public function deletePrize($raffleId, $prizeId)
    {
        $prize = RafflePrize::where('raffle_id', $raffleId)->where('id', $prizeId)->first(); 
        if(!$prize)
        {
            abort(404);
        }
        $prize->delete();

        Toastr::success('Prize deleted successfully.', 'Success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slots = RaffleSlot::where('raffle_id', $id)->get();
        foreach($slots as $slot)
        {
            $slot->delete();
        }

        $prizes = RafflePrize::where('raffle_id', $id)->get();
        foreach($prizes as $prize)
        {
            $prize->delete();
        }

        $raffle = Raffle::whereId($id)->first();
        $raffle->delete();

        Toastr::success('Raffle deleted successfully.', 'Success');
        return redirect()->back();
    }


    public function drawRaffle($id)
    {
       $raffle = Raffle::whereId($id)->where('status', 'drawable')->first();
       if(!$raffle)
       {
           abort(404);
       }

       $prizes = RafflePrize::where('raffle_id', $raffle->id)->get();

       $take = count($prizes);

       $slots = RaffleSlot::where('raffle_id', $raffle->id)
                            ->where('member_id', '!=', null)
                            ->inRandomOrder()
                            ->get()->take($take);

        // dd($slots[2]);

        for($x = 0; $x < $take; $x++)
        {
            $slots[$x]->prize_id = $prizes[$x]->id;
            $slots[$x]->save();

            $member = $slots[$x]->member;
            $member->available_withdrawal = $member->available_withdrawal + $prizes[$x]->prize_amount;
            $member->save();

            $history = new MemberEarningHistory();
            $history->member_id = $member->id;
            $history->type = "in";
            $history->title = "Raffle Prize";
            $history->description = "you have won ( ". $prizes[$x]->position ." ) position on the raffle for slot #".$slots[$x]->id;
            $history->amount = $prizes[$x]->prize_amount;
            $history->save();
    
        }     
        
        $raffle->status = 'ended';

        $raffle->save();

        Toastr::success('Raffle drawn successfully.', 'Success');
        return redirect()->back();
       
    }
}
