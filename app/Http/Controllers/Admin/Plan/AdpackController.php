<?php

namespace App\Http\Controllers\Admin\Plan;

use App\Adpack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Level;
use Brian2694\Toastr\Facades\Toastr;

class AdpackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['adpacks'] = Adpack::all();
        $data['levels'] = Level::all();
        return view('admin.plan.adpack', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->all());

        $request->validate([
            'title' => 'required|unique:adpacks,title',
            'clicks' => 'required|numeric',
            'price' => 'required',
        ]);

        $adpack = new Adpack();
        $adpack->title = $request->title;
        $adpack->clicks = $request->clicks;
        $adpack->price = $request->price;
        $adpack->description = $request->description;
        $adpack->save();

        Toastr::success('Adpack created successfully.', 'Success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|unique:adpacks,title,'.$id,
            'clicks' => 'required|numeric',
            'price' => 'required',
        ]);

        $adpack = Adpack::whereId($id)->first();
        $adpack->title = $request->title;
        $adpack->clicks = $request->clicks;
        $adpack->price = $request->price;
        $adpack->description = $request->description;
        $adpack->save();

        Toastr::success('Adpack updated successfully.', 'Success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adpack = Adpack::whereId($id)->first();
        $adpack->delete();

        Toastr::success('Adpack deleted successfully.', 'Success');
        return redirect()->back();
    }
}
