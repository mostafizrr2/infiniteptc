<?php

namespace App\Http\Controllers\Admin\Plan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Membership;
use Brian2694\Toastr\Facades\Toastr;

class MembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['memberships'] = Membership::all();
        return view('admin.plan.membership', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // abort(404);
        // dd($request->all());
        $request->validate([
            'title' => 'required|unique:memberships,title',
            'can_click' => 'required|numeric',
            'cpc' => 'required',
            'price' => 'required',
        ]);

        $membership = new Membership();
        $membership->title = $request->title;
        $membership->type = 'custom';
        $membership->can_click = $request->can_click;
        $membership->cpc = $request->cpc;
        $membership->sponsor_bonus = $request->sponsor_bonus;
        $membership->admin_fees = $request->admin_fees;
        $membership->price = $request->price;
        $membership->description = $request->description;
        $membership->save();

        Toastr::success('Membership created successfully.', 'Success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|unique:memberships,title,'.$id,
            'can_click' => 'required|numeric',
            'cpc' => 'required',
            'price' => 'required',
        ]);

        $membership = Membership::whereId($id)->first();
        $membership->title = $request->title;
        $membership->can_click = $request->can_click;
        $membership->cpc = $request->cpc;
        $membership->sponsor_bonus = $request->sponsor_bonus;
        $membership->admin_fees = $request->admin_fees;
        $membership->price = $request->price;
        $membership->description = $request->description;
        $membership->save();

        Toastr::success('Membership updated successfully.', 'Success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // abort(404);

        $membership = Membership::whereId($id)->first();
       
        if(count($membership->members))
        {
            Toastr::error('Some members are associated with this membership plan. you cannot delete it right now.', 'Error!');
            return redirect()->back();
        }

        $membership->delete();

        Toastr::success('Membership deleted successfully.', 'Success');
        return redirect()->back();
    }
}
