<?php

namespace App\Http\Controllers\Admin\Plan;

use App\Level;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['levels'] = Level::all();
        return view('admin.plan.level', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
        // dd($request->all());

        // $request->validate([
        //     'level' => 'required|numeric|unique:levels,level',
        //     'position' => 'required|numeric',
        //     'payouts' => 'required',
        //     'refferal_bonus' => 'required',
        // ]);

        // $level = new Level();
        // $level->level = $request->level;
        // $level->position = $request->position;
        // $level->payouts = $request->payouts;
        // $level->refferal_bonus = $request->refferal_bonus;
        // $level->description = $request->description;
        // $level->save();

        // Toastr::success('Level created successfully.', 'Success');
        // return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'level' => 'required|numeric|unique:levels,level,'.$id,
            'position' => 'required|numeric',
            'payouts' => 'required',
            'refferal_bonus' => 'required',
        ]);

        $level = Level::whereId($id)->first();
        $level->level = $request->level;
        $level->position = $request->position;
        $level->payouts = $request->payouts;
        $level->refferal_bonus = $request->refferal_bonus;
        $level->description = $request->description;
        $level->save();

        Toastr::success('Level updated successfully.', 'Success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
        // $level = Level::whereId($id)->first();
        // $level->delete();

        // Toastr::success('Level deleted successfully.', 'Success');
        // return redirect()->back();
    }
}
