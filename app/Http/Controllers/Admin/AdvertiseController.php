<?php

namespace App\Http\Controllers\Admin;

use App\Advertise;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdvertiseController extends Controller
{

    public function pendingAdvertise()
    {
        $data['type'] = "pending";
        $data['title'] = "Pending Advertisements";
        $data['advertises'] = Advertise::where('status', 0)->paginate(50);
        return view('admin.advertise.index', $data);
    }

    public function approvedAdvertise()
    {
        $data['type'] = "approved";
        $data['title'] = "Approved Advertisements";
        $data['advertises'] = Advertise::where('status', 1)->latest()->paginate(50);
        return view('admin.advertise.index', $data);
    }

    public function declinedAdvertise()
    {
        $data['type'] = "declined";
        $data['title'] = "Declined Advertisements";
        $data['advertises'] = Advertise::where('status', 3)->latest()->paginate(50);
        return view('admin.advertise.index', $data);
    }

    public function alldAdvertise()
    {
        $data['type'] = "all";
        $data['title'] = "Declined Advertisements";
        $data['advertises'] = Advertise::latest()->paginate(50);
        return view('admin.advertise.index', $data);
    }

    public function pendingAdvertiseApprove(Request $request)
    {
        //  dd($request->advertise_id);
        $advertise = Advertise::where('id', $request->advertise_id)->first();
        $advertise->is_running = true;
        $advertise->status = 1;
        $advertise->save();

        return redirect()->back();
    }

    public function declineAdvertise(Request $request)
    {
        //  dd($request->advertise_id);
        $advertise = Advertise::where('id', $request->advertise_id)->first();
        $advertise->is_running = false;
        $advertise->status = 3;
        $advertise->save();

        $member = $advertise->member;
        $member->available_adclick =  $advertise->member->available_adclick +  $advertise->required_clicks;
        $member->save();

        return redirect()->back();
    }
}
