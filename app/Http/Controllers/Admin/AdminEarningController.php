<?php

namespace App\Http\Controllers\Admin;

use App\AdminEarning;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminEarningController extends Controller
{
    public function index()
    {
        $data['earnings'] = AdminEarning::latest()->paginate(50);
        return view( 'admin.earning.index', $data );
    }
}
