<?php

namespace App\Http\Controllers\Admin;

use App\Raffle;
use App\Withdraw;
use Carbon\Carbon;
use App\AdminEarning;
use App\Charts\EarningChart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index()
    {

        $casEarning = AdminEarning::orderBy('created_at', 'ASC')->get(['amount', 'created_at'])->groupBy( function( $date ) {
            return Carbon::parse($date->created_at)->format('F');
         })->take(12)->toArray();

        //  $casEarning = AdminEarning::whereMonth('created_at', '=', Carbon::now()->subMonth()->month)->get(['amount','created_at']);

        
        // $months = [];
        $totalData = [];

        foreach($casEarning as $key => $data)
        {
            $earn = [];
            foreach($data as $res)
            {
               $earn[] = $res['amount'];
            }
           $totalData[$key] = array_sum($earn);
        }

        $earningMonths = [];
        $earnings = [];
        foreach($totalData as $month => $earning)
        { 
          $earningMonths[] = $month;   
          $earnings[] = round($earning, 2);   
        }
 
        $colors = ['#00909E', '#708160', '#27496D', '#024249', '#142850', '#06623B', '#512B58', '#937D14'];
        $clr = array_rand($colors,1);
        $randColor = $colors[$clr];

        $earningChart = new EarningChart;
        $earningChart->title('Admin Earnings', 20, "#17A2B8", true, 'Helvetica Neue');
        // $earningChart->barwidth(0.8);
        // $earningChart->displaylegend(false);
        $earningChart->labels($earningMonths);
        $earningChart->dataset('Earnings (USD)', 'bar', $earnings)
        // ->color("rgb(255, 99, 132)")
        ->backgroundcolor($colors)
        ->fill(false)
        ->linetension(0.1);
        // ->dashed([5]);


        $casWithDraw = Withdraw::where('status', 1)->orderBy('created_at', 'ASC')->get(['request_amount', 'created_at'])->groupBy( function( $date ) {
            return Carbon::parse($date->created_at)->format('F');
         })->take(12)->toArray();

         $totalData = [];

         foreach($casWithDraw as $key => $data)
         {
             $cost = [];
             foreach($data as $res)
             {
                $cost[] = $res['request_amount'];
             }
            $totalData[$key] = array_sum($cost);
         }
 
         $costMonths = [];
         $costs = [];
         foreach($totalData as $month => $cost)
         { 
           $costMonths[] = $month;   
           $costs[] = round($cost, 2);   
         }
    
         $withdrawChart = new EarningChart;
         $withdrawChart->title('Cash Outflow', 20, $randColor , true, 'Helvetica Neue');
         $withdrawChart->barwidth(0.5);
         $withdrawChart->displaylegend(true);
         $withdrawChart->labels($costMonths);
         $withdrawChart->dataset('Costs (USD)', 'line',  $costs)
         ->color($randColor)
         ->backgroundcolor($randColor)
         ->fill(false)
         ->linetension(0.1);


        $data['earningChart'] = $earningChart;
        $data['withdrawChart'] = $withdrawChart;
        $data['drawableRaffles'] = Raffle::with('prizes')->with('slots')->with('free_slots')->where('status','drawable')->latest()->get();
        return view('admin.index', $data);
    }
}
