<?php

namespace App\Http\Controllers\Admin;

use App\System;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class SystemController extends Controller
{
    function systemInfo()
    {
      return view('admin.system.info');
    }

    function SaveSystemInfo(Request $request)
    {
      $request->validate([
        "title" => 'required|max:50',
        "slogan" => 'required|max:100',
        "contact_phone" => 'max:15',
        "contact_email" => 'max:50',
        "street_address" => 'max:191',
        "city" => 'max:50',
        "country" => 'max:50',
        "latitude" => 'max:15',
        "longitude" => 'max:15',
      ]);

      $system = System::first();
      $system->title = $request->title; 
      $system->slogan = $request->slogan; 
      $system->description = $request->description; 
      $system->contact_phone = $request->contact_phone; 
      $system->contact_email = $request->contact_email; 
      $system->street_address = $request->street_address; 
      $system->city = $request->city; 
      $system->country = $request->country; 
      $system->latitude = $request->latitude; 
      $system->longitude = $request->longitude;
      $system->save();
      Toastr::success('profile updated successfully.', 'Success');
      return redirect()->back();
    }

    function seoInfo()
    {
        return view('admin.system.seo');
    }

    function SaveSeoInfo(Request $request)
    {
      $request->validate([
        "meta_title" => 'required|max:80',
        "meta_keywords" => 'max:255',
      ]);

      $system = System::first();
      $system->meta_title = $request->meta_title; 
      $system->meta_description = $request->meta_description;
      $system->meta_keywords = $request->meta_keywords; 
      $system->save();

      Toastr::success('Meta updated successfully.', 'Success');
      return redirect()->back();
    }


    function systemUploads()
    {
      return view('admin.system.uploads');
    }

    function SaveSystemUploads(Request $request)
    { 
        // dd($request->all());
      // $request->validate([
      //   "main_logo" => 'image|mimes:jpeg,png,jpg|max:2048',
      //   "mobile_logo" => 'image|mimes:jpeg,png,jpg|max:2048',
      //   "favicon" => 'image|mimes:jpeg,png,jpg|max:1024',
      //   "web_banner" => 'image|mimes:jpeg,png,jpg|max:3072'
      // ]);

      if(!Storage::disk('public')->exists('system'))
      {
          Storage::disk('public')->makeDirectory('system');
      } 

      $system = System::first();

      //Main logo
      if($request->hasFile('main_logo'))
      {

        $request->validate([
          "main_logo" => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);


          if($system->main_logo != null)
          {
              if(Storage::disk('public')->exists('system/'.$system->main_logo))
              {
                  Storage::disk('public')->delete('system/'.$system->main_logo);
              }
          }

          $file = $request->file('main_logo');
          $file_ext = $file->getClientOriginalExtension();
          $main_logo = 'main_logo_'.time().'.'.$file_ext;

          $save = 'storage/system/'. $main_logo;
 
          $image = Image::make($file)
                          ->resize(150,40)
                          ->save($save);
      }
      else
      {
          $main_logo = $system->main_logo;
      }


      //Mobile logo
      if($request->hasFile('mobile_logo'))
      {
        $request->validate([
          "mobile_logo" => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

          if($system->mobile_logo != null)
          {
              if(Storage::disk('public')->exists('system/'.$system->mobile_logo))
              {
                  Storage::disk('public')->delete('system/'.$system->mobile_logo);
              }
          }
          $file = $request->file('mobile_logo');
          $file_ext = $file->getClientOriginalExtension();
          $mobile_logo = 'mobile_logo_'.time().'.'.$file_ext;

          $save = 'storage/system/'. $mobile_logo;

          $image = Image::make($file)
                          ->resize(30,30)
                          ->save($save);
      }
      else
      {
          $mobile_logo = $system->mobile_logo;
      }

      //App Favicon
      if($request->hasFile('favicon'))
      {

        $request->validate([
          "favicon" => 'image|mimes:jpeg,png,jpg|max:1024'
        ]);

          if($system->favicon != null)
          {
              if(Storage::disk('public')->exists('system/'.$system->favicon))
              {
                  Storage::disk('public')->delete('system/'.$system->favicon);
              }
          }
          $file = $request->file('favicon');
          $file_ext = $file->getClientOriginalExtension();
          $favicon = 'favicon_'.time().'.'.$file_ext;

          $save = 'storage/system/'. $favicon;

          $image = Image::make($file)
                          ->resize(16,16)
                          ->save($save);
      }
      else
      {
          $favicon = $system->favicon;
      }

      //App Banner
      if($request->hasFile('web_banner'))
      {

        $request->validate([
          "web_banner" => 'image|mimes:jpeg,png,jpg|max:6072'
        ]);
        
          if($system->web_banner != null)
          {
              if(Storage::disk('public')->exists('system/'.$system->web_banner))
              {
                  Storage::disk('public')->delete('system/'.$system->web_banner);
              }
          }
          $file = $request->file('web_banner');
          $file_ext = $file->getClientOriginalExtension();
          $web_banner = 'web_banner_'.time().'.'.$file_ext;

          $save = 'storage/system/'. $web_banner;

          $image = Image::make($file)
                          ->fit(500,300)
                          ->save($save);
      }
      else
      {
          $web_banner = $system->web_banner;
      }

      $system->main_logo = $main_logo; 
      $system->mobile_logo = $mobile_logo; 
      $system->favicon = $favicon; 
      $system->web_banner = $web_banner; 
      
      $system->save();

      Toastr::success('Files uploaded successfully', 'Success');
      return redirect()->back();
    }

    function apiIntegration()
    {
      return view('admin.system.api');
    }

    function SaveApiIntegration(Request $request)
    {
      $request->validate([
        "google_app_key" => 'max:191',
        "google_app_secret" => 'max:191',
        "facebook_app_key" => 'max:191',
        "facebook_app_secret" => 'max:191',
        "mapbox_access_token" => 'max:191',
        "gmap_api_key" => 'max:191',
        "pusher_app_id" => 'max:191',
        "pusher_app_secret" => 'max:191'
      ]);

      $system = System::first();
      $system->google_app_key = $request->google_app_key;
      $system->google_app_secret = $request->google_app_secret;
      $system->facebook_app_key = $request->facebook_app_key;
      $system->facebook_app_secret = $request->facebook_app_secret;
      $system->mapbox_access_token = $request->mapbox_access_token;
      $system->gmap_api_key = $request->gmap_api_key;
      $system->pusher_app_id = $request->pusher_app_id;
      $system->pusher_app_secret = $request->pusher_app_secret;
      $system->save();

      Toastr::success('Api info updated successfully.', 'Success');
      return redirect()->back();

    }



    function metaScripts()
    {
      return view('admin.system.meta-scripts');
    }

    function SaveMetaScripts(Request $request)
    {
      $system = System::first();
      $system->header_scripts = $request->header_scripts;
      $system->footer_scripts = $request->footer_scripts;
      $system->save();

      Toastr::success('Meta scripts updated successfully.', 'Success');
      return redirect()->back();
    }


    function uploadsDelete($type)
    {
      $system = System::first();

      if($type == 'main_logo')
      {
        $file = $system->main_logo;
        $system->main_logo = null;
      }
      if($type == 'mobile_logo')
      {
        $file = $system->mobile_logo;
        $system->mobile_logo = null;
      }
      if($type == 'app_favicon')
      {
        $file = $system->app_favicon;
        $system->app_favicon = null;
      }
      if($type == 'app_banner')
      {
        $file = $system->app_banner;
        $system->app_banner = null;
      }

      $system->save();


      if($file != null)
      {
          if( Storage::disk('public')->exists( 'system/' . $file ) )
          {
              Storage::disk('public')->delete( 'system/' . $file );
          }
      }

      Toastr::success('File removed successfully.', 'Success');
      return redirect()->back();

    }

}
