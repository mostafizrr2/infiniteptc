<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChatRoomController extends Controller
{
    public function index()
    {
        return view('admin.chat.index');
    }

    public function myChatRoom($id)
    {
        $data['roomId'] = $id;
        return view('admin.chat.room',$data);
    }
}
