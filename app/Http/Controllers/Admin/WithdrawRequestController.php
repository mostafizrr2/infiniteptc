<?php

namespace App\Http\Controllers\Admin;

use App\System;
use App\Withdraw;
use App\Transaction;
use Illuminate\Http\Request;
use App\Helpers\CoinPayments;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class WithdrawRequestController extends Controller
{
    public function index()
    {
        $data['withdraw'] = Withdraw::where('status', 0)->paginate(50);
        return view('admin.request.withdraw', $data);
    }

    public function withdrawApprove(Request $request)
    {
        $wdr = Withdraw::whereId($request->id)->first();
        //    dd($wdr->member->available_withdrawal);
        $member =  $wdr->member;
 
        if($wdr->request_amount < 10 || $wdr->request_amount > $wdr->member->available_withdrawal)
        {
           die('Something going wrong');  
        }

        $app = System::first();

        $cps = new CoinPayments();

        $cps->Setup($app->cp_private_key, $app->cp_public_key);

        // dd($cps->GetBalances());

        $reqAmount = ($wdr->request_amount - ($wdr->request_amount / 100) * 5);

        $rates = $cps->GetRates();
        $usdToBtcRate = $rates['result']['USD']['rate_btc'];

        $amount = $usdToBtcRate * $reqAmount;

        $result = $cps->CreateWithdrawal( 
            $amount, 
            $member->wallet->type, 
            $member->wallet->address,
            true
        );

        $trans = new Transaction();
        $trans->member_id =  $member->id;
        $trans->type =  'cashout';
        $trans->amount =  $wdr->request_amount;
        $trans->wallet_address =  $member->wallet->address;

        if ($result['error'] == 'ok') {

            // print 'Withdrawal created with ID: '.$result['result']['id'];
            $trans->transaction_id = $result['result']['id'];
            $trans->message =  "Fund sent successfully";
            $trans->status = 1;

            $wdr->status = 1;
            $wdr->save();
    
            $member->available_withdrawal =  $member->available_withdrawal - $wdr->request_amount;
            $member->save();
    
            Toastr::success('Withdraw request approved successfully.', 'Success');


        } else {
            // print 'Error: '.$result['error']."\n";
            $trans->transaction_id = "____";
            $trans->message =  $result['error'];
            $trans->status = 0;

            Toastr::error($result['error'], 'Ooopsss...');

        }

        $trans->save();

        return redirect()->back();

    }

    public function withdrawDecline(Request $request)
    {
        $wdr = Withdraw::whereId($request->id)->first();
        $wdr->status = 2;
        $wdr->save();

        Toastr::success('Withdraw request approved successfully.', 'Success');
        return redirect()->back();
    }

    public function withdrawHistory()
    {
        $data['withdraw'] = Withdraw::where('status', 2)->orWhere('status', 1)->latest()->paginate(50);
        return view('admin.request.history', $data);
    }
}
