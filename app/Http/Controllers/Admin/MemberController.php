<?php

namespace App\Http\Controllers\Admin;

use App\Level;
use App\Member;
use App\Membership;
use App\Mail\AccountCreated;
use Illuminate\Http\Request;
use App\Helpers\forcedMatrix;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Mail;


class MemberController extends Controller
{

    public function memberView($username)
    {
        if(!$member = Member::where('username', $username)->first())
        {
            abort(404);
        }

        $data['member'] = $member;
        return view('admin.member.view',$data);
    }

    public function memberBlock($username)
    {
        if(!$member = Member::where('username', $username)->first())
        {
            abort(404);
        }

        $member->block_status = true;
        $member->save();

        $username = $member->username;

        Toastr::success($username.' blocked successfully.', 'Success');

        return redirect()->back();
    }

    public function memberUnBlock($username)
    {
        if(!$member = Member::where('username', $username)->first())
        {
            abort(404);
        }

        $member->block_status = false;
        $member->save();

        $username = $member->username;

        Toastr::success($username.' unblocked successfully.', 'Success');

        return redirect()->back();
    }

    public function allMembers(Request $request)
    {
        
        if($request->has('field') && $request->has('search') && !empty($request->search))
        {
            if($request->field == 'username')
            {
               $members = Member::where('status', true)->where('username', 'LIKE', '%'.$request->search.'%')->latest()->paginate(50);
            }
            elseif($request->field == 'email')
            {
                $members = Member::where('status', true)->where('email', 'LIKE', '%'.$request->search.'%')->latest()->paginate(50);
            }
            else
            {
                $members = Member::where('status', true)->where('name', 'LIKE', '%'.$request->search.'%')->latest()->paginate(50);
            }

            $data['title'] = "Searched by: ". ucfirst($request->field) ." ( ".$request->search." )";
        }
        else
        {
            $data['title'] = "All Members";
            $members = Member::where('status', true)->latest()->paginate(50);
        }

        $data['members'] = $members;
        $data['memberships'] = Membership::all();
        return view('admin.member.index',$data);
    }

    public function paidMembers()
    {
        $data['title'] = "Paid Members";
        $memberships = Membership::where('type', 'paid')->orWhere('type', 'custom')->get();

        $membership_id = [];
        foreach($memberships as $mem)
        {
            $membership_id[] = $mem->id;
        }

        // dd($membership_id);
        $data['memberships'] = Membership::all();
        $data['members'] = Member::where('status', true)->whereIn('membership_id', $membership_id)->latest()->paginate(50);
        return view('admin.member.index',$data);
    }

    public function freeMembers()
    {
        $data['title'] = "Free Members";
        $membership = Membership::where('type', 'free')->first();
        $data['members'] = Member::where('status', true)->where('membership_id', $membership->id)->latest()->paginate(50);
        $data['memberships'] = Membership::all();
        return view('admin.member.index',$data);
    }

    public function blockedMembers()
    {
        $data['title'] = "Blocked Members";
        $data['members'] = Member::where('status', true)->where('block_status', true)->latest()->paginate(50);
        $data['memberships'] = Membership::all();
        return view('admin.member.index',$data);
    }


    public function changeMembership(Request $request)
    {
        $request->validate([
            'member_id' => 'required',
            'membership_id' => 'required'
        ]);

        $member = Member::whereId($request->member_id)->first();
        $member->membership_id = $request->membership_id;
        $member->save();

        $matrix = new forcedMatrix();
        $matrix->matrixFor($member);

        Toastr::success('Membership changed successfully.', 'Success');
        return redirect()->back();
    }

    public function addMember()
    {
        $countries = countries();
        $countries = json_decode($countries, true);

        $countryByName = [];
        foreach($countries as $country)
        {
            $countryByName[] = $country['country'];
        }

        $data['countries'] =  $countryByName;
        return view('admin.member.add-member', $data);
    }

    public function addMemberAction(Request $request)
    {
        $request->validate([
          'name' => 'required',
          'username' => 'required|unique:members,username',
          'email' => 'required|unique:members,email',
          'country' => 'required',
        ]);

        if($request->referral_id)
        {
            $sponsor = Member::where('referral_id', $request->referral_id)->first();
            if($sponsor)
            {
                $sponsor_id = $sponsor->id;
            }
            else
            {
                $sponsor_id = null;
            }
        }
        else
        {
            $sponsor_id = null;
        }

        $member = new Member();
        // $member->sponsor_id = $sponsor;
        $member->sponsor_id = $sponsor_id;
        $member->name = $request->name;
        $member->username = $request->username;
        $member->email = $request->email;
        // $member->address = $request->address;
        // $member->state = $request->state;
        $member->country = $request->country;
        // $member->zip = $request->zip;

        $ref_id = substr(time(), -6);
        $member->referral_id = $ref_id;

        $membership = Membership::where('type', 'free')->first();
        $member->membership_id = $membership->id;

        $level = Level::where('level', 0)->first();
        $level_id = ($level) ? $level->id : 1;
        $member->level_id = $level_id;

        $member->verify_token = null;
        $member->status = true;

        $member->password = bcrypt($request->password);
        $member->save();

        Mail::to($member->email)->send(new AccountCreated($member, $request->password));

        Toastr::success('Member created successfully.', 'Success');
        return redirect()->back();
    }

    public function memberEdit($id)
    {
        // dd($id);
        $data['member'] = Member::where('id', $id)->first();
        // dd($data['member']);
        return view('admin.member.edit',$data);
    }

    public function memberUpdate(Request $request, $id)
    {
        $request->validate([
            'username' => 'required|unique:members,username,'.$id,
            'email' => 'required|unique:members,email,'.$id,
            'name' => 'required',
        ]);
        $member =  Member::where('id', $id)->first();

        // dd($request->all());

        $member->username = $request->username;
        $member->name = $request->name;
        $member->email = $request->email;
        $member->address = $request->address;
        $member->state = $request->state;
        $member->zip = $request->zip;

        if($request->password != null)
        {
            $member->password = bcrypt($request->password);
        }

        $member->save();

        Toastr::success('Members profile updated successfully.', 'Success');

        return redirect()->route('admin.member', $member->username);
    }

}
