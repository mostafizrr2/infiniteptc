<?php

namespace App\Http\Controllers\Admin;

use App\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['faqs'] = Faq::latest()->get();
        return view('admin.faq.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'question' => 'required|max:150|unique:faqs,question',
            'answer' => 'required',
            'video_link' => 'max:255',
        ]);

        $faq = new Faq();
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->video_link = $request->video_link;
        $faq->save();

        Toastr::success('Faq created successfully!', 'Success');
        return redirect()->route('faq.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $faq = Faq::whereId($id)->first();

        if(!$faq)
        {
            abort(404);
        }

        $data['faq'] = $faq;
        return view('admin.faq.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'question' => 'required|max:150|unique:faqs,question,'. $id,
            'answer' => 'required',
            'video_link' => 'max:255',
        ]);

        $faq = Faq::whereId($id)->first();
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->video_link = $request->video_link;
        $faq->save();

        Toastr::success('Faq updated successfully!', 'Success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faq::whereId($id)->first();
        $faq->delete();

        Toastr::success('Faq deleted successfully!', 'Success');
        return redirect()->back();
    }
}
