<?php

namespace App\Http\Livewire\Chat;

use App\Room;
use App\Member;
use Livewire\Component;

class RoomMembers extends Component
{

    public $room;

    public $roomId;
    public $userId;
    public $userClass;

    public $allUsers = [];

    protected $listeners = [
        'ChatMembersUpdated' => '$refresh'
    ];

    public function mount($roomId)
    {


        if(auth('member')->check())
        {
            $this->userClass = "member";
            $this->userId = auth('member')->user()->id;
        }
        elseif(auth()->check())
        {
            $this->userClass = "admin";
            $this->userId = auth()->user()->id;
        }
        else
        {
            // $this->userClass = null;
            $this->userId = null;
        }  


        $room = Room::find($roomId);

        $this->room = $room;

        $this->roomId = $roomId;

        // $this->roomMembers = $room->members;
    }

    public function render()
    {
        $data['members'] = Member::all();

        $room = Room::find($this->roomId);

        $data['roomMembers'] = $room->members;
        
        return view('livewire.chat.room-members',$data);
    }
}
