<?php

namespace App\Http\Livewire\Chat;

use App\Room;
use Livewire\Component;
use App\Http\Livewire\Chat\Rooms;
use App\Member;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RoomForm extends Component
{
    public $title;
    public $description;

    public $userClass;
    public $userId;

    public $users = [];

    public function mount()
    {
    
        if(auth('member')->check())
        {
            $this->userClass = "member";
            $this->userId = auth('member')->user()->id;
        }
        elseif(auth()->check())
        {
            $this->userClass = "admin";
            $this->userId = auth()->user()->id;
        }
        else
        {
            $this->userClass = null;
            $this->userId = null;
        }  

    }
    
    public function updated()
    {
        $this->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
  
    }

    public function submit()
    {

        $this->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
  
        $room = new Room();
        $room->title = $this->title;
        $room->description = $this->description;
        $room->user_id = $this->userId;
        $room->user_type = $this->userClass;
        $room->save();

        if(count($this->users))
        {
            $room->members()->attach($this->users);
        }

        $this->title = "";
        $this->description = "";

        Session::flash('success', 'Room created Succesfully.');
        
        $this->emit('roomsRefresh');
    }

    public function render()
    {
        $data['members'] = Member::all();
        return view('livewire.chat.room-form',$data);
    }
}
