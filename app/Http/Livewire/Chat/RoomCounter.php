<?php

namespace App\Http\Livewire\Chat;

use App\Room;
use Livewire\Component;

class RoomCounter extends Component
{

    protected $listeners = [
        'roomsRefresh' => '$refresh'
    ];


    public function render()
    {
        $data['roomCounter'] = Room::all()->count();
        return view('livewire.chat.room-counter', $data);
    }
}
