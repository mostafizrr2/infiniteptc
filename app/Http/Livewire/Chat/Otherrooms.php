<?php

namespace App\Http\Livewire\Chat;

use App\Room;
use App\Member;
use Livewire\Component;

class Otherrooms extends Component
{

    protected $user;

    public $perPage = 5;

    public $total;

    public $showLoadButton = false;

    public $userClass;
    public $userId;

    public $route;

    public function mount()
    {
        if(auth('member')->check())
        {
            $this->user = auth('member')->user();
            $this->userId = auth('member')->user()->id;
            $this->userClass = 'member';
            $this->route = 'member.room';
        }
        elseif(auth()->check())
        {
            $this->user = auth()->user();
            $this->userId = auth()->user()->id;
            $this->userClass = 'admin';
            $this->route = 'admin.room';
        }


        if ($this->userClass == 'member') {


            $this->total = count($this->user->rooms);

        }
        elseif($this->userClass == 'admin')
        {
            $this->total = Room::where('user_type', 'member')
            ->latest()
            ->get()->count();
        }


        if($this->perPage < $this->total)
        {
            $this->showLoadButton = true;
        }

    }

    public function loadMore()
    {
        $this->perPage = $this->perPage + 5;

        // $this->total = Room::all()->count();

        if($this->perPage >= $this->total)
        {
            $this->showLoadButton = false;
        }
    }

    public function render()
    {
      
  
        if ($this->userClass == 'member') 
        {

            $data['rooms'] = Room::whereHas('members', function($q){
               return $q->where('member_id', $this->userId);
            })->paginate($this->perPage);
        }
        elseif($this->userClass == 'admin')
        {
            $data['rooms'] = Room::where('user_type', 'member')
            ->latest()
            ->paginate($this->perPage);
        }
    
        return view('livewire.chat.other-rooms',$data);
    }
}
