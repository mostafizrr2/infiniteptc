<?php

namespace App\Http\Livewire\Chat;

use App\Room;
use Livewire\Component;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Session;

class Myrooms extends Component
{

    protected $listeners = [
        'roomsRefresh' => '$refresh',
        // 'deleteRefresh' => '$refresh'
    ];

    public $perPage = 5;

    public $total;

    public $showLoadButton = false;

    public $userClass;
    public $userId;
    public $route;

    public function mount()
    {
        if(auth('member')->check())
        {
            $this->userClass = "member";
            $this->userId = auth('member')->user()->id;
            $this->route = 'member.room';
        }
        elseif(auth()->check())
        {
            $this->userClass = "admin";
            $this->userId = auth()->user()->id;
            $this->route = 'admin.room';
        }
        else
        {
            $this->userClass = null;
            $this->userId = null;
        }  


        $this->total = Room::where('user_type', $this->userClass)
                        ->where('user_id', $this->userId)
                        ->get()
                        ->count();

        if($this->perPage < $this->total)
        {
            $this->showLoadButton = true;
        }
    }

    public function loadMore()
    {
        $this->perPage = $this->perPage + 5;

        // $this->total = Room::all()->count();

        if($this->perPage >= $this->total)
        {
            $this->showLoadButton = false;
        }
    }



    public function remove($id)
    {
        Room::destroy($id);
        Session::flash('warning', 'Room deleted Succesfully.');
        return redirect('/dashboard/chatrooms');
      
    }
     

    public function render()
    {
        
        $data['rooms'] = Room::where('user_type', $this->userClass)
                                ->where('user_id', $this->userId) 
                                ->latest()
                                ->paginate($this->perPage);
                                
        return view('livewire.chat.my-rooms',$data);
    }
}
