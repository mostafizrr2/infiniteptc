<?php

namespace App\Http\Livewire\Chat;

use App\Room;
use App\Member;
use Livewire\Component;
use Illuminate\Support\Facades\Session;

class ChatUpdate extends Component
{
    public $roomId;
    public $roomMembers;

    public $userClass;
    public $userId;
    public $room;

    // public $messageText;

    public $selectedUsers = [];

    protected $listeners = [
        'refreshChat' => '$refresh'
    ];


    public function mount($roomId)
    {

        if(auth('member')->check())
        {
            $this->userClass = "member";
            $this->userId = auth('member')->user()->id;
        }
        elseif(auth()->check())
        {
            $this->userClass = "admin";
            $this->userId = auth()->user()->id;
        }
        else
        {
            $this->userClass = null;
            $this->userId = null;
        }

        // $room = Room::find($roomId);

        $this->roomId = $roomId;

        $room = Room::find($roomId);

        $this->room = $room;

        // dd($this->roomMembers->toArray());
        // dd($room->members);
        foreach($room->members as $key => $member){
            
            $this->selectedUsers[$key] = $member->id;
        }

       
        // $this->roomMembers = $room->members;
    }

    public function saveChanges()
    {
        // dd($this->selectedUsers);
        $room = Room::find($this->roomId);
        $room->members()->sync($this->selectedUsers);

        $this->emit('ChatMembersUpdated');

        Session::flash('success', 'Save changed succesfully.');
    }

    public function render()
    {
        $data['members'] = Member::all();
        return view('livewire.chat.chat-update', $data);
    }
}
