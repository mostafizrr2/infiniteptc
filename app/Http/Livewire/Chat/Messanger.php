<?php

namespace App\Http\Livewire\Chat;

use App\Room;
use App\Message;
use Livewire\Component;
use App\MessageNotification;

class Messanger extends Component
{

    public $roomId;
    public $room;

    public $userClass;
    public $userId;

    public $messageText;

    protected $listeners = [
        'refreshChat' => '$refresh'
    ];


    public function mount($roomId)
    {

        if(auth('member')->check())
        {
            $this->userClass = "member";
            $this->userId = auth('member')->user()->id;
        }
        elseif(auth()->check())
        {
            $this->userClass = "admin";
            $this->userId = auth()->user()->id;
        }
        else
        {
            $this->userClass = null;
            $this->userId = null;
        }

        $this->room = Room::find($roomId);

        $this->roomId = $roomId;

        // dd($room->members);

        // $this->roomMembers = $room->members;
    }

    public function submitMessage()
    {
        $this->validate([
            'messageText' => 'required'
        ]);

        $message = new Message();
        $message->member_id = $this->userId;
        $message->sender_type = $this->userClass;
        $message->room_id = $this->roomId;
        $message->message = $this->messageText;
        $message->file_url = null;
        $message->save();

        $this->messageText = null;

        
   
        foreach($this->room->members as $member)
        {

            $checkUnseend = MessageNotification::where('room_id', $this->roomId)
            ->where('member_id', $member->id)
            ->where('status', false)
            ->first();  

            if(!$checkUnseend)
            {
                $mn = new MessageNotification();
                $mn->member_id = $member->id;
                $mn->room_id = $this->roomId;
                $mn->notification = "You have a new message..";
                $mn->save();
            }

        }
        

        if( $this->userClass == 'admin' || $this->room->user_id != $this->userId )
        {
            $checkUnseend = MessageNotification::where('room_id', $this->roomId)
            ->where('member_id', $this->room->user_id)
            ->where('status', false)
            ->first();  

            if(!$checkUnseend)
            {
                $mn = new MessageNotification();
                $mn->member_id = $this->room->user_id;
                $mn->room_id = $this->roomId;
                $mn->notification = "You have a new message..";
                $mn->save();
            }
        }


        $this->emit('messageSent');


    }

    public function render()
    {
        $data['messages'] = Message::where('room_id', $this->roomId)->get();
        return view('livewire.chat.messanger', $data);
    }
}
