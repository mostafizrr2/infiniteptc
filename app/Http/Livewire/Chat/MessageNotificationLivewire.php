<?php

namespace App\Http\Livewire\Chat;

use Livewire\Component;
use App\MessageNotification;

class MessageNotificationLivewire extends Component
{

    // public $notifications = ['tgwertg','swetwet','wetewt'];
    public $userId;

    protected $listeners = [
        'messageSent' => '$refresh'
    ];

    public function mount()
    {
         $this->userId = auth('member')->user()->id;
         $this->user = auth('member')->user();
    }

    public function afterUpdate()
    {
        
        $this->listeners;
    }

    public function enterMessageRoom($id)
    {
        $mn = MessageNotification::find($id);
        // dd($mn);
        $mn->status = true;
        $mn->save();

        return redirect()->route('member.room', $mn->room_id);
    }

    public function render()
    {
         $data['notifications'] = MessageNotification::where('member_id', $this->userId)
                                    ->where('status', false)
                                    ->latest()
                                    ->get();

                                    // dd( $data['notifications']);

        return view('livewire.chat.message-notification-livewire', $data);
    }
}
