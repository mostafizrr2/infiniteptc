<?php

namespace App\Providers;

use App\Level;
use App\Member;
use App\Raffle;
use App\System;
use App\Withdraw;
use App\Advertise;
use App\Membership;
use App\AdminEarning;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('path.public', function() {
            return base_path().'/../public_html/account';
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {


        // $example = __DIR__."/../../.env.example";
        // $env = __DIR__."/../../.env";

        // if(!file_exists($env)){
        //     copy($example, $env);
        //     Artisan::call("key:generate");
        //     $host = $_SERVER['HTTP_HOST'];
        //     header('location:'. 'http://'.$host);
        //     exit;
        // }


        Schema::defaultStringLength(191);

        try
        {
            DB::connection()->getPdo();
            $connection = true;
        }
        catch(\Exception $e)
        {
            $connection = false;
        }


        if(Schema::hasTable('systems'))
        {
           $system = System::first();
           if(!$system)
           {
               $system = new System();
               $system->title = "PTC Application";
               $system->slogan = "Unlimited earning with forced matrix system";
               $system->save();
           }    
        }


        if($connection)
        {
            if (Schema::hasTable('systems')) {
                $system =  System::first();
                View::share ('system', $system);

                // if($system->app_environment == true)
                // {
                //     if($system->pub_path != null)
                //     {
                //         $this->app->bind('path.public', function() {
                //             $system =  System::first();
                //             return realpath(base_path().'/'.$system->pub_path);
                //         });
                //     }
                // }
            }
        }

        view()->composer('member.*', function ($view) 
        {
            if (Auth::guard('member')->check()) 
            { 
                $member =  Auth::guard('member')->user();
                $view->with('user', $member);
            }
            else 
            {
                $view->with('user', null);
            }
        }); 



        if(Schema::hasTable('withdraws'))
        {
            view()->composer('admin.*', function ($view) 
            {

                $withdraw = Withdraw::where('status', 0)->get();
                $view->with('withdrawCount', count($withdraw));
                
                $advertise = Advertise::where('status', 0)->get();
                $view->with('advertiseCount', count($advertise));

                $member = Member::where('status', 1)->get();
                $view->with('memberCount', count($member));

                $adminEarnings = AdminEarning::get();
                
                $earnings = [];
                foreach($adminEarnings as $earning)
                {
                    $earnings[] = $earning->amount;
                }

                $view->with('totalEarningCount', round( array_sum($earnings), 2 ) );
           }); 
        }


        if(Schema::hasTable('levels'))
        {

           $level = Level::where('level', 0)->first();
           if(!$level) 
           {
            $new = new Level();
            $new->level = 0;
            $new->position = 0;
            $new->payouts = 0;
            $new->refferal_bonus = 0;
            $new->description = "Level 0";
            $new->save();
           }

           $level = Level::where('level', 1)->first();
           if(!$level) 
           {
            $new = new Level();
            $new->level = 1;
            $new->position = 3;
            $new->payouts = 0.75;
            $new->refferal_bonus = 0;
            $new->description = "Level 1";
            $new->save();
           }

           $level = Level::where('level', 2)->first();
           if(!$level) 
           {
            $new = new Level();
            $new->level = 2;
            $new->position = 9;
            $new->payouts = 5;
            $new->refferal_bonus = 0;
            $new->description = "Level 2";
            $new->save();
           }

           $level = Level::where('level', 3)->first();
           if(!$level) 
           {
            $new = new Level();
            $new->level = 3;
            $new->position = 27;
            $new->payouts = 5;
            $new->refferal_bonus = 0;
            $new->description = "Level 3";
            $new->save();
           }

           $level = Level::where('level', 4)->first();
           if(!$level) 
           {
            $new = new Level();
            $new->level = 4;
            $new->position = 81;
            $new->payouts = 35;
            $new->refferal_bonus = 5;
            $new->description = "Level 4";
            $new->save();
           }

           $level = Level::where('level', 5)->first();
           if(!$level) 
           {
            $new = new Level();
            $new->level = 5;
            $new->position = 243;
            $new->payouts = 80;
            $new->refferal_bonus = 20;
            $new->description = "Level 5";
            $new->save();
           }

           $level = Level::where('level', 6)->first();
           if(!$level) 
           {
            $new = new Level();
            $new->level = 6;
            $new->position = 729;
            $new->payouts = 500;
            $new->refferal_bonus = 100;
            $new->description = "Level 6";
            $new->save();
           }

           $level = Level::where('level', 7)->first();
           if(!$level) 
           {
            $new = new Level();
            $new->level = 7;
            $new->position = 2187;
            $new->payouts = 2600;
            $new->refferal_bonus = 400;
            $new->description = "Level 7";
            $new->save();
           }

        }

        if(Schema::hasTable('users'))
        {
            $user = User::where('email', 'testadmin@gmail.com')->first();
            if(!$user)
            {
               $user = new User();
               $user->name = "Test Admin";  
               $user->email = "testadmin@gmail.com";  
               $user->password = bcrypt("@test@admin"); 
               $user->save(); 
            }
        }

        if(Schema::hasTable('raffles'))
        {
            view()->composer('member.*', function ($view) 
            {
              $runningRaffles = Raffle::with('prizes')->with('slots')->with('free_slots')->where('status','running')->latest()->get();  
              $view->with('activeRaffles', $runningRaffles);
            });
        }

        if(Schema::hasTable('memberships'))
        {
            $membership = Membership::where('type', 'free')->first();
            if(!$membership)
            {
              $new = new Membership();
              $new->title = "Free membership";
              $new->type = "free";
              $new->can_click = 400;
              $new->cpc = 0.02;
              $new->sponsor_bonus = 0.005;
              $new->admin_fees = 0.005;
              $new->description = "Free membership plan";
              $new->save();
            }

            $membership = Membership::where('type', 'paid')->first();
            if(!$membership)
            {
              $new = new Membership();
              $new->title = "Paid membership";
              $new->type = "paid";
              $new->can_click = 400;
              $new->cpc = 0.02;
              $new->sponsor_bonus = 0.005;
              $new->admin_fees = 0.005;
              $new->description = "Paids membership plan";
              $new->save();
            }
        }
    }
}
