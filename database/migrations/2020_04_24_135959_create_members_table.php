<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {

            $table->id();

            $table->unsignedBigInteger('sponsor_id')->nullable();
            $table->foreign('sponsor_id')->references('id')->on('members');
            
            $table->string('name', 50);
            $table->string('username', 50);
            $table->string('email', 50)->unique();
            $table->string('address', 150)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('country', 50)->nullable();
            $table->string('zip', 10)->nullable();
            $table->string('avatar', 50)->nullable();
            $table->string('verify_token', 255)->nullable();
            $table->boolean('status')->default(false);
            $table->boolean('block_status')->default(false);
            //Balance
            $table->double('balance')->default(0.00)->nullable();
            //Eearnings
            $table->double('total_earning')->default(0.00)->nullable();
            $table->double('refferal_earning')->default(0.00)->nullable();
            $table->double('available_withdrawal')->default(0.00)->nullable();
            //Purchased adpack clicks
            $table->integer('available_adclick')->default(0);
            $table->string('referral_id', 50)->nullable();
            $table->unsignedBigInteger('membership_id')->nullable();
            $table->foreign('membership_id')->references('id')->on('memberships');
            $table->integer('ad_click_count')->default(0);
            $table->unsignedBigInteger('level_id')->nullable();
            $table->foreign('level_id')->references('id')->on('levels');
            $table->string('password');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
