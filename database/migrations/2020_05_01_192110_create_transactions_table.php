<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('member_id');
            $table->foreign('member_id')->references('id')->on('members');

            $table->string('type')->nullable(); // cashin or cashout
            
            $table->string('transaction_id')->nullable(); // cashin or cashout

            $table->double('amount')->default(0.00);

            $table->string('wallet_address')->nullable();
            
            $table->text('message')->nullable(0);
            
            $table->boolean('status')->default(1); // 1 = no error, 0 = has error

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
