<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matrices', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('member_id')->nullable();
            $table->foreign('member_id')->references('id')->on('members');
            $table->string('username')->nullable();

            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('matrices');

            $table->unsignedBigInteger('left_id')->nullable();
            $table->foreign('left_id')->references('id')->on('matrices');

            $table->unsignedBigInteger('middle_id')->nullable();
            $table->foreign('middle_id')->references('id')->on('matrices');

            $table->unsignedBigInteger('right_id')->nullable();
            $table->foreign('right_id')->references('id')->on('matrices');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matrices');
    }
}
