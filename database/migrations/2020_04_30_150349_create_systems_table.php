<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function (Blueprint $table) {
            $table->id();
            //System Info
            $table->string('title', 50)->nullable()->default('PTC - Infinite funds');
            $table->string('slogan', 100)->nullable()->default('Best MLM & PTC Platform where you can earn');
            $table->text('description')->nullable();
            $table->string('contact_phone', 20)->nullable();
            $table->string('contact_email', 50)->nullable();
            $table->string('street_address', 190)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('country', 50)->nullable();
            $table->string('latitude', 15)->nullable();
            $table->string('longitude', 15)->nullable(); 
            
            //SEO Stuffs
            $table->string('meta_title', 80)->nullable(); 
            $table->text('meta_description')->nullable(); 
            $table->string('meta_keywords', 255)->nullable(); 

            // Files & Uploads
            $table->string('main_logo')->nullable();
            $table->string('mobile_logo')->nullable();
            $table->string('favicon')->nullable();
            $table->string('web_banner')->nullable();

            //API Integration
            $table->string('google_app_key')->nullable();
            $table->string('google_app_secret')->nullable();
            $table->string('facebook_app_key')->nullable();
            $table->string('facebook_app_secret')->nullable();
            $table->string('mapbox_access_token')->nullable();
            $table->string('gmap_api_key')->nullable();
            $table->string('pusher_app_id')->nullable();
            $table->string('pusher_app_secret')->nullable();

            //CoinPayment Wallet
            $table->string('cp_title', 50)->nullable();
            $table->string('cp_merchant_id', 150)->nullable();
            $table->string('cp_public_key', 255)->nullable();
            $table->string('cp_private_key', 255)->nullable();


            //Meta Scripts
            $table->text('header_scripts')->nullable();
            $table->text('footer_scripts')->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }
}
