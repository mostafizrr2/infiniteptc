<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            
            $table->id();

            $table->unsignedBigInteger('member_id');
            $table->foreign('member_id')->references('id')->on('members');

            $table->string('trx_id')->nullebale();
            $table->string('merchant_wallet_Address',255)->nullable();

            $table->double('amount_from')->nullable();
            $table->double('amount_to')->nullable();
            $table->string('received_currency')->nullable();

            $table->bigInteger('timeout')->nullable();

            $table->string('checkout_url', 255)->nullable();
            $table->string('status_url', 255)->nullable();
            $table->string('qrcode_url', 255)->nullable();

            $table->string('status',50)->default('waiting')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
